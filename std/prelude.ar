// Prelude is included in every file implicitly:

type Maybe<T> = none | some(T)
type Either<T, U> = left(T) | right(U)

//
// FILE API:
//

#[extern] fun fopen(name: char*, access: char*) -> void*
#[extern] fun fread(buffer: void*, elementSize: i64, elementCount: i64, f: void*) -> i64
#[extern] fun fwrite(ptr: int*, size: i64, count: i64, stream: void*)
#[extern] fun ftell(f: void*) -> int
#[extern] fun fseek(f: void*, offset: int, origin: int) -> int
#[extern] fun fclose(stream: void*)

//
// Libc:
//

#[extern] fun malloc(bytes: i64) -> void*
#[extern] fun free(val: void*)
#[extern] fun memcpy(dest: void*, src: void*, bytes: i64) -> void*

#[extern] fun qsort(base: void*, num: int, width: int, pred: \(void*, void*) -> int)

#[extern] fun strtof(start: char*, end: char**) -> float
#[extern] fun strtol(start: char*, end: char**, base: int) -> int
#[extern] fun puts(text: char*)

#[extern] fun exit(code: int)
#[extern] fun abort()

// From Argot standard library:
#[extern] fun printnum(a: int)
#[extern] fun printfloat(a: float)

fun swap<T>(a: T*, b: T*) {
    let temp = *a;
    *a = *b;
    *b = temp;
}

// this is basically C++'s operator new
fun new<T, Args: ConstructorArgs<T>>(args: Args) -> T* {
    let mem = malloc(sizeof(T));
    constructInto(mem, T(args))
}


// This might be horrible:

impl Norm for float {
    type ResultType = float
    fun norm() -> float {
        if self > 0 { self } else { -self }
    }
}

impl Norm for int {
    type ResultType = int
    fun norm() -> int {
        if self > 0 { self } else { -self }
    }
}

fun streq(a: char*, b: char*) -> bool {
    let i = 0;
    while a[i] != '\0' && b[i] != '\0' {
        if a[i] != b[i] {
            return false;
        }

        i += 1;
    }

    a[i] == b[i]
}
