//
// common.h
// Common (PCH) header:
//

#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include <algorithm>
#include <assert.h>
#include <cstdarg>
#include <cstdint>
#include <cstring>
#include <memory>
#include <optional>
#include <set>
#include <stack>
#include <string>
#include <unordered_map>
#include <vector>

#define ignore(x) (void)x

using Str = std::string;
using StrA = const Str&;
using Wstr = std::wstring;
using WstrA = const std::wstring&;

template <typename T> using Own = std::unique_ptr<T>;
template <typename T> using Ref = std::shared_ptr<T>;

template <typename T> using Stack = std::stack<T>;
template <typename T> using Vec = std::vector<T>;
template <typename T> using Set = std::set<T>;
template <typename K, typename V> using Map = std::unordered_map<K, V>;
template <typename X, typename Y> using Pair = std::pair<X, Y>;

template <typename T> using RefA = const Ref<T>&;
template <typename T> using VecA = const std::vector<T>&;

template <typename T> using Maybe = std::optional<T>;
constexpr auto none = std::nullopt;

struct Timer {
    Timer();
    double elapsedS() const;
    double elapsedMs() const;
    void reset();

  private:
    int64_t _start;
};

template <typename T, typename U> Ref<T> dyn_cast(const Ref<U>& u) {
    return std::dynamic_pointer_cast<T>(u);
}

template <typename T, typename U> bool is(const Ref<U>& u) {
    return dyn_cast<T>(u) != nullptr;
}

[[noreturn]] inline void unreachable(const char* msg) {
    printf("Internal compiler error: %s\n", msg);
    abort();
}

template <typename Key, typename Value, typename LookupKey>
Maybe<Value> lookup(const Map<Key, Value>& map, const LookupKey& key) {
    auto it = map.find(key);
    if (it == map.end())
        return none;
    return it->second;
}

template <typename Key> bool lookup(const Set<Key>& map, const Key& key) {
    return map.find(key) != map.end();
}

// http://www.reedbeta.com/blog/python-like-enumerate-in-cpp17/
template <typename T, typename TIter = decltype(std::begin(std::declval<T>())),
          typename = decltype(std::end(std::declval<T>()))>
constexpr auto enumerate(T&& iterable) {
    struct iterator {
        size_t i;
        TIter iter;
        bool operator!=(const iterator& other) const { return iter != other.iter; }
        void operator++() {
            ++i;
            ++iter;
        }
        auto operator*() const { return std::tie(i, *iter); }
    };
    struct iterable_wrapper {
        T iterable;
        auto begin() { return iterator{ 0, std::begin(iterable) }; }
        auto end() { return iterator{ 0, std::end(iterable) }; }
    };
    return iterable_wrapper{ std::forward<T>(iterable) };
}

// These exist so we can have static creators accessing private constructors.
// This is a problem because make_shared<> can't access, so gotta call new then wrap.

template <typename T> Ref<T> wrapShare(T* t) {
    return Ref<T>(t);
}

template <typename T> Ref<T> wrapUnique(T* t) {
    return Own<T>(t);
}

template <typename T, typename... Args> Ref<T> mkref(const Args&... args) {
    return std::shared_ptr<T>(new T(args...));
}

template <typename T, typename... Args> Own<T> mkown(const Args&... args) {
    return std::unique_ptr<T>(new T(args...));
}

// Variable flags:
enum class VFlags {
    none,
    // reference type:
    ref,
};

extern Map<Str, Ref<struct LexedFile>> lexedFiles;
void debugBreak();

// Support helpers for dealing with file system stuff:
static const char _pathSeparator = '\\';

bool isPathSeparator(char c);
Str joinPath(StrA lhs, StrA rhs);
bool fileExists(StrA path);
Maybe<Str> readFile(const Str& name);
Str workingDirectory();
Wstr workingDirectoryW();
Pair<Str, Str> separateFilePath(StrA inputPath);
bool isRelativePath(StrA path);
char* heapBackString(const Str& str);

#ifdef __clang__
#define FORMAT_ATTRIBUTE(x, y)    __attribute__((format (printf, x, y)))
#else
#define FORMAT_ATTRIBUTE(x, y)
#endif

inline Str format(const char* fmt, va_list va) {
    auto cnt = _vscprintf(fmt, va) + 1;

    Vec<char> buffer(cnt);
    _vsnprintf_s(&buffer[0], cnt, cnt - 1, fmt, va);
    va_end(va);

    return Str(buffer.begin(), buffer.end());
}

FORMAT_ATTRIBUTE(1, 2) inline Str format(const char* fmt, ...) {
    va_list va;
    va_start(va, fmt);
    return format(fmt, va);
}
