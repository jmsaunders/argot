#include "common.h"

#include "ast.h"

#include <sstream>

// This is an unfortunate hack so we have access to the internal method call symbol!
// We need to populate the internal symbol table of instantiated generic record types. This requires
// knowledge of some internal typechecker stuff. This should maybe be moved into the type checker
// then?
#include "typecheck/typecheck_private.h"
#include "typecheck/scope.h"

using namespace std;

bool Type::copyable() {
    auto self = ref();

    // Not a meaningful question. We should never get here.
    assert(!is<VoidType>(self));

    // Fundamental types are always copyable:
    return is<IntType>(self) || is<FloatType>(self) || isBool();
}

//
// Built-in types:
//

Str prettyIntNameForWidth(int width) {
    if (width == 32)
        return "int";
    if (width == 16)
        return "i16";
    if (width == 8)
        return "char";

    return "i" + std::to_string(width);
}

void addArithmeticTrait(Ref<Type> type, RefA<InterfaceGeneric> gen) {
    auto inst = gen->instantiate({ type });

    SubstitutionMap subs = { { inst->types["ResultType"], type } };

    auto impl = mkref<TraitInstanceInfo>(inst, subs, inst->methods);
    type->traits.push_back(impl);
}

Ref<InterfaceGeneric> sumInterface();
Ref<InterfaceGeneric> prodInterface();

Ref<IntType> IntType::make(int width) {
    auto ret = wrapShare(new IntType(width));

    addArithmeticTrait(ret, sumInterface());
    addArithmeticTrait(ret, prodInterface());

    return ret;
}

Ref<FloatType> FloatType::make(int width) {
    auto ret = wrapShare(new FloatType(width));

    addArithmeticTrait(ret, sumInterface());
    addArithmeticTrait(ret, prodInterface());

    return ret;
}

static Str floatName(int width) {
    if (width == 32)
        return "float";
    return "double";
}

FloatType::FloatType(int width) : Type(floatName(width)), width(width) {
}

IntType::IntType(int width) : Type(prettyIntNameForWidth(width)), width(width) {
}

Ref<Type> Type::getI8() {
    static Ref<Type> ret = IntType::make(8);
    return ret;
}

Ref<Type> Type::getI16() {
    static Ref<Type> ret = IntType::make(16);
    return ret;
}

Ref<Type> Type::getI32() {
    static Ref<Type> ret = IntType::make(32);
    return ret;
}

Ref<Type> Type::getI64() {
    static Ref<Type> ret = IntType::make(64);
    return ret;
}

Ref<Type> Type::getF32() {
    static Ref<Type> ret = FloatType::make(32);
    return ret;
}

Ref<Type> Type::getBool() {
    static Ref<Type> ret = make_shared<BoolType>();
    return ret;
}

Ref<Type> Type::getVoid() {
    static Ref<Type> ret = make_shared<VoidType>();
    return ret;
}

Ref<Type> Type::resolve() {
    auto ret = ref();

    while (auto tv = ret->typeVariable()) {
        if (tv->bound)
            ret = tv->bound;
        else
            break;
    }

    return ret;
}

bool Type::equals(RefA<Type> rhs) const {
    //
    // This is not only ugly and bad, it is incorrect.
    // This degenerates when comparing templated types types like:
    // List<T> where T can probably be a T coming from two different templated scopes.
    //
    // This needs to be a canonicalized pointer-equivalence test I think. Or something
    // more complicated at least with recursive testing for exact equality.
    //

    return name == rhs->name;
}

TraitSelfType::TraitSelfType(RefA<Interface> trait) : Type(trait->name), trait(trait) {
}

Str generateTemplateListString(const Vec<Ref<TemplateArgType>>& templates,
                               const SubstitutionMap& instances) {
    stringstream ss;

    ss << "<";
    bool started = false;
    for (const auto& t : templates) {
        if (started)
            ss << ", ";

        auto inst = instances.find(t);
        if (inst != instances.end())
            ss << inst->second->resolve()->name;
        else
            ss << t->resolve()->name;

        started = true;
    }
    ss << ">";

    return ss.str();
}

Str generateTemplateListString(const Vec<Ref<Type>>& templates) {
    stringstream ss;

    ss << "<";
    bool started = false;
    for (const auto& t : templates) {
        if (started)
            ss << ", ";

        ss << t->resolve()->name;

        started = true;
    }
    ss << ">";

    return ss.str();
}

static Str generateTemplatedName(StrA name, const Vec<Ref<TemplateArgType>>& templates,
                                 const SubstitutionMap& instances) {
    return name + generateTemplateListString(templates, instances);
}

//
// Generics:
//

Vec<Ref<Type>> replaceTypesFromMap(Vec<Ref<Type>> from, const SubstitutionMap& map) {
    Vec<Ref<Type>> ret;
    for (const auto& f : from)
        ret.push_back(replaceTypesFromMap(f, map));
    return ret;
}

Ref<Interface> replaceTypesFromMap(RefA<Interface> from, const SubstitutionMap& map) {
    if (auto gi = dyn_cast<InterfaceGenericInstance>(from)) {
        auto newMap = gi->args;
        for (auto& [from, to] : newMap) {
            if (auto rep = dyn_cast<ReplacableType>(to)) {
                if (auto result = lookup(map, rep))
                    to = *result;
            }
        }
        return gi->parent->instantiate(newMap);
    }

    return from;
}

Ref<Type> replaceTypesFromMap(RefA<Type> from, const SubstitutionMap& map) {
    // @TODO:
    // This code just blindly replaces without making sure it has to. It should check before
    // re-instantiating things...

    if (auto tv = dyn_cast<ReplacableType>(from)) {
        if (auto result = lookup(map, tv))
            return (*result)->resolve();
    }

    // Re-instantiate any templated types if needed:
    if (auto ki = dyn_cast<GenericInstance>(from)) {
        SubstitutionMap replacement = ki->arguments;

        for (auto& r : replacement)
            r.second = replaceTypesFromMap(r.second, map);

        return ki->parent()->instantiate(replacement);
    }

    if (auto pv = dyn_cast<PointerType>(from)) {
        auto c = replaceTypesFromMap(pv->interior, map);
        if (c != pv)
            return PointerType::of(c);
    }

    if (auto fn = dyn_cast<FunctionType>(from)) {
        auto rt = replaceTypesFromMap(fn->returnType, map);
        auto args = replaceTypesFromMap(fn->argTypes, map);
        return mkref<FunctionType>(rt, args);
    }

    return from;
}

static ArgumentList replaceTypesFromMap(const ArgumentList& args, const SubstitutionMap& map) {
    ArgumentList ret = args;
    for (auto& parm : ret)
        parm.type = replaceTypesFromMap(parm.type, map);
    return ret;
}

Ref<FunctionSignature> replaceTypesFromMap(RefA<FunctionSignature> fs, const SubstitutionMap& map) {
    auto ret = mkref<FunctionSignature>(*fs);

    ret->returnType = replaceTypesFromMap(ret->returnType, map);
    ret->params = replaceTypesFromMap(fs->params, map);

    return ret;
}

Ref<Type> Generic::instantiate(const Vec<Ref<Type>>& inst) {
    if (inst.size() != templates.size())
        return nullptr;

    SubstitutionMap map;
    for (size_t i = 0; i < inst.size(); ++i)
        map[templates[i]] = inst[i];

    return instantiate(map);
}

bool Generic::validInstantiation(const SubstitutionMap& subs) {
    for (const auto& t : templates) {
        if (auto mappedTo = lookup(subs, t)) {
            if (!constraintSatisfiedByType(t->constraint, *mappedTo))
                return false;
        } else {
            return false;
        }
    }

    return true;
}

Ref<Type> Generic::trivialInstance() {
    SubstitutionMap trivial;
    for (auto t : templates)
        trivial[t] = t;
    return instantiate(trivial);
}

// Refind a Generic instance that still has unbound type variables in its arguments
// according to a substitution map.

Ref<Type> GenericInstance::refine(const SubstitutionMap& map) {
    auto newArguments = arguments;

    for (auto& arg : newArguments) {
        if (auto tv = dyn_cast<TemplateArgType>(arg.second)) {
            if (auto next = lookup(map, tv))
                arg.second = *next;
        }
    }

    return parent()->instantiate(newArguments);
}

bool RecordBaseType::defaultConstructible() {
    // This forces generic instances to be populated:
    ignore(recordType());

    if (defaultConstructorIndex)
        return true;

    // If we have constructors but no copy constructor then we're not
    // default constructable:
    if (!constructors.empty())
        return false;

    // At this point we have no constructors, so if all our fields are default
    // constructible, then we are:
    for (const auto& field : fields) {
        if (!field.type->defaultConstructible())
            return false;
    }

    return true;
}

bool RecordBaseType::copyable() {
    // This forces generic instances to be populated:
    ignore(recordType());

    // Anything with an explicit copy constructor is copyable:
    if (copyConstructorIndex)
        return true;

    // If we have a destructor and no copy constructor then we're not copyable:
    if (explicitDestructorDecl)
        return false;

    for (const auto& field : fields) {
        if (!field.type->copyable())
            return false;
    }

    return true;
}

bool RecordBaseType::nontrivialCopy() {
    // This forces generic instances to be populated:
    ignore(recordType());

    if (copyConstructorIndex)
        return true;

    for (const auto& field : fields) {
        if (field.type->nontrivialCopy())
            return true;
    }

    return false;
}

Ref<RecordBaseType> RecordInstanceType::recordType() {
    if (!_parent->isComplete())
        unreachable("invalid incomplete generic use");

    // @TODO: This code is horrible and doesn't populate the symbol table for the type!
    // This should be merged with the other code that does the things inside the typechecker!

    if (!_filled) {
        for (auto [index, from] : enumerate(_parent->fields)) {
            auto name = from.name;
            auto resultType = replaceTypesFromMap(from.type, arguments);
            fields.push_back({ name, resultType });
            symbols[name] = mkref<MemberVarSymbol>(name, index, resultType);
        }

        for (const auto& constructor : _parent->constructors) {
            ConstructorDecl newConstructor{
                constructor.selfToken,
                replaceTypesFromMap(constructor.params, arguments),
            };
            constructors.push_back(newConstructor);
        }

        for (const auto& method : _parent->methods) {
            auto newSig =
                mkref<MethodSignature>(shared_from_this(), *replaceTypesFromMap(method, arguments));

            auto methodSym = wrapShare(new MethodCallSymbol(shared_from_this(), newSig, {}));
            registerCallSymbol(symbols, methodSym);
            methods.push_back(newSig);
        }

        // This part sucks:
        defaultConstructorIndex = _parent->defaultConstructorIndex;
        copyConstructorIndex = _parent->copyConstructorIndex;
        explicitDestructorDecl = _parent->explicitDestructorDecl;

        _filled = true;
    }

    return shared_from_this();
}

Ref<Type> RecordGeneric::instantiate(const SubstitutionMap& map) {
    if (!validInstantiation(map))
        return nullptr;

    auto fullName = generateTemplatedName(name, templates, map);
    return mkref<RecordInstanceType>(fullName, nameToken, shared_from_this(), map);
}

bool VariantBaseType::copyable() {
    for (const auto& field : fields) {
        if (field.type && !field.type->copyable())
            return false;
    }

    return true;
}

bool VariantBaseType::nontrivialCopy() {
    for (const auto& field : fields) {
        if (field.type && field.type->nontrivialCopy())
            return true;
    }

    return false;
}

Ref<Type> VariantGeneric::instantiate(const SubstitutionMap& map) {
    if (!validInstantiation(map))
        return nullptr;

    auto fullName = generateTemplatedName(name, templates, map);
    return make_shared<VariantTypeInstance>(fullName, nameToken, shared_from_this(), map);
}

Ref<VariantBaseType> VariantTypeInstance::variantType() {
    if (!_filled) {
        for (const auto& f : _parent->fields) {
            VariantField field = { f.nameToken, replaceTypesFromMap(f.type, arguments) };
            fields.push_back(field);
        }

        _filled = true;
    }

    return shared_from_this();
}

Ref<Interface> InterfaceGeneric::instantiate(const Vec<Ref<Type>>& inst) {
    if (inst.size() != templates.size())
        return nullptr;

    SubstitutionMap map;
    for (size_t i = 0; i < inst.size(); ++i)
        map[templates[i]] = inst[i];

    return instantiate(map);
}

Ref<Interface> ConstructorArgTrait::make(RefA<Type> forType) {
    auto ret = wrapShare(new ConstructorArgTrait(forType));
    ret->traitSelf = mkref<TraitSelfType>(ret);
    return ret;
}

Ref<Interface> InterfaceGeneric::instantiate(const SubstitutionMap& inst) {
    auto instanceName = name + generateTemplateListString(templates, inst);
    auto ret = InterfaceGenericInstance::make(instanceName, shared_from_this(), inst);

    //
    // There's some funny business going on here.
    //
    // Since the method signatures can depend on the re-typed trait internal types, we add them to
    // an intermediary map and then do the type substitution to map to the new types.
    //
    // We recreate the TraitInternalTypes so we can keep track of what the explicit instance of our
    // generic is, so we can reconstruct associated types at code generation time.
    //

    SubstitutionMap sm = inst;
    for (const auto& [name, ty] : types) {
        auto nty = ret->types[name] = mkref<TraitInternalType>(name, ret);
        nty->type = ty->type;
        sm[ty] = nty;
    }

    for (const auto& m : methods)
        ret->methods.push_back(replaceTypesFromMap(m, sm));

    return ret;
}

Ref<Interface> Interface::make(StrA name) {
    auto ret = wrapShare(new Interface(name));
    ret->traitSelf = mkref<TraitSelfType>(ret);
    return ret;
}

Ref<Interface> InterfaceGenericInstance::make(StrA name, RefA<InterfaceGeneric> parent, const SubstitutionMap& args) {
    auto ret = wrapShare(new InterfaceGenericInstance(name, parent, args));
    ret->traitSelf = mkref<TraitSelfType>(ret);
    return ret;
}

Str ConstructorArgTraitIndex::prettyNameForType(RefA<Type> type, size_t constructorIndex) {
    return "ConstructorArgIndex<" + type->name + ", " + to_string(constructorIndex) + ">";
}

bool constraintSatisfiedByType(RefA<Interface> constraint, RefA<Type> type) {
    if (constraint) {
        if (auto t = type->templateVariable()) {
            if (t->constraint->equals(constraint))
                return true;
        }

        if (auto gi = type->genericInstance()) {
            for (const auto& supportedTraitInfo : gi->parent()->traits) {
                if (supportedTraitInfo->trait == constraint) {
                    Str errorMsg;
                    auto maybeSubmap = inferTraitImplTemplates(supportedTraitInfo, gi, &errorMsg);
                    return maybeSubmap != none;
                }
            }
        } else {
            for (const auto& supportedTraitInfo : type->traits) {
                if (supportedTraitInfo->trait->equals(constraint))
                    return true;
            }
        }

        return false;
    }

    return true;
}

//
// Type inference:
//

// Returns none on success and an error string on failure:
Maybe<Str> canUnifyTypes(Ref<Type> lhs, Ref<Type> rhs) {
    lhs = lhs->resolve();
    rhs = rhs->resolve();

    if (lhs->equals(rhs))
        return none;

    // Try to unify type variables:
    {
        auto lhsTV = lhs->typeVariable();
        auto rhsTV = rhs->typeVariable();

        if (lhsTV) {
            lhsTV->bound = rhs;
            return none;
        }

        if (rhsTV) {
            rhsTV->bound = lhs;
            return none;
        }
    }

    if (lhs->pointerType() && rhs->pointerType()) {
        auto lp = lhs->pointerType();
        auto rp = rhs->pointerType();

        return canUnifyTypes(lp->interior, rp->interior);
    }

    // Type unification on  function types:
    if (is<FunctionType>(lhs) && is<FunctionType>(rhs)) {
        auto lf = dyn_cast<FunctionType>(lhs), rf = dyn_cast<FunctionType>(rhs);

        if (lf->argTypes.size() != rf->argTypes.size())
            return none;

        for (size_t i = 0; i < lf->argTypes.size(); ++i) {
            if (auto err = canUnifyTypes(lf->argTypes[i], rf->argTypes[i]))
                return format("parameter unification error: %s", err->c_str());
        }

        if (auto err = canUnifyTypes(lf->returnType, rf->returnType))
            return format("return type unification error: %s", err->c_str());

        return none;
    }

    auto lhsKi = lhs->genericInstance();
    auto rhsKi = rhs->genericInstance();

    if (lhsKi && rhsKi) {
        if (lhsKi->parent() != rhsKi->parent()) {
            return format("cannot coerce between different variant generics (%s and %s)",
                          lhsKi->parent()->name.c_str(), rhsKi->parent()->name.c_str());
        }

        for (const auto& [from, to] : lhsKi->arguments) {
            auto other = *lookup(rhsKi->arguments, from);
            if (auto result = canUnifyTypes(to, other))
                return result;
        }

        return none;
    }

    return format("cannot unify %s and %s", lhs->name.c_str(), rhs->name.c_str());
}

Maybe<SubstitutionMap> inferTraitImplTemplates(RefA<TraitInstanceInfo>& supportedTraitInfo,
                                               RefA<GenericInstance> gi, Str* errorMsg) {
    //
    // We need to validate that this trait instance works against our type.
    // A couple things can go wrong with this process:
    //
    //   1. We may not support the required constraints
    //         impl<T: Show> Show for List<T>
    //   2. Types that need to be the same may not be the same
    //         impl<T: Cmp> Thing for Map<T, T>
    //

    // This is the first layer of substitutions, for impl<T>, we introduce a new
    // type variable for each template on the impl to try to unify and find a
    // T->whatever.
    SubstitutionMap implSubs;
    for (const auto& t : supportedTraitInfo->templates)
        implSubs[t] = mkref<TypeVariable>();

    // This is the map from the instance our impl is defined for, replacing the
    // templated parts of it with the type variables created above:
    auto typeSubs = supportedTraitInfo->type->genericInstance()->arguments;
    for (auto& t : typeSubs)
        t.second = replaceTypesFromMap(t.second, implSubs);

    auto tvType = replaceTypesFromMap(supportedTraitInfo->type, typeSubs);
    auto tvGenericInstance = tvType->genericInstance();
    assert(tvGenericInstance);

    for (const auto& [from, to] : gi->arguments) {
        if (auto errorResult = canUnifyTypes(to, typeSubs[from])) {
            *errorMsg = *errorResult;
            return none;
        }
    }

    return implSubs;
}

static Str functionTypeName(RefA<Type> returnType, VecA<Ref<Type>> argTypes) {
    stringstream ss;
    ss << "\\(";

    bool started = false;
    for (const auto& arg : argTypes) {
        if (started)
            ss << ", ";
        ss << arg->name;
        started = true;
    }
    ss << ")";

    if (!returnType->isVoid()) {
        ss << " -> " << returnType->name;
    }

    return ss.str();
}

FunctionType::FunctionType(RefA<Type> returnType, VecA<Ref<Type>> argTypes)
    : Type(functionTypeName(returnType, argTypes)), returnType(returnType), argTypes(argTypes) {
}

static Str arrayTypeName(RefA<Type> baseType, size_t length) {
    stringstream ss;
    ss << baseType->name << "[" << length << "]";
    return ss.str();
}

ArrayType::ArrayType(RefA<Type> baseType, size_t length)
    : Type(arrayTypeName(baseType, length)), baseType(baseType), length(length) {
}

void Interface::fillout(Ref<Interface> other, RefA<Type> type) const {
    SubstitutionMap sm;
    for (const auto& [name, ty] : types) {
        auto nty = other->types[name] = mkref<TraitInternalType>(name, other);
        nty->type = type;
        sm[ty] = nty;
    }

    for (const auto& m : methods)
        other->methods.push_back(replaceTypesFromMap(m, sm));
}

Ref<Interface> Interface::specialize(RefA<Type> type) {
    auto ret = make(name);
    fillout(ret, type);
    return ret;
}

Ref<Interface> InterfaceGenericInstance::specialize(RefA<Type> type) {
    auto ret = wrapShare(new InterfaceGenericInstance(name, parent, args));
    fillout(ret, type);
    return ret;
}
