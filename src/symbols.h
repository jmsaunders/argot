#pragma once

#include "token.h"

struct Type;
struct TemplateArgType;
namespace Ast {
struct LetStmt;
}

//
// Bare symbols
// These aid lookups on naked identifiers to disambiguate:
//

struct Symbol {
    virtual ~Symbol() {}
};

// Base class for variables:
struct VariableSymbol : Symbol {
    Ref<Type> type;

  protected:
    VariableSymbol(RefA<Type> type) : type(type) {}
};

// Local let binding:
struct LetVariableSymbol : VariableSymbol {
    Ref<Ast::LetStmt> origin;
    LetVariableSymbol(RefA<Ast::LetStmt> orig);
};

// Came from the function's argument:
struct FunArgSymbol : VariableSymbol {
    Str name;
    size_t argumentNumber;

    FunArgSymbol(StrA name, size_t offset, RefA<Type> type)
        : VariableSymbol(type), name(name), argumentNumber(offset) {}
};

// From parent struct:
struct MemberVarSymbol : VariableSymbol {
    Str name;
    size_t offset;

    // This is the path through the inherited structures
    Vec<size_t> path;

    MemberVarSymbol(StrA name, size_t offset, RefA<Type> type, Vec<size_t> path = {})
        : VariableSymbol(type), name(name), offset(offset), path(path) {
    }
};

// An unwrapped variable in a match expression:
struct UnwrappedSymbol : VariableSymbol {
    Str name;
    UnwrappedSymbol(StrA name, RefA<Type> type) : VariableSymbol(type), name(name) {}
};
