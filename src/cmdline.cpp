//
// A simple command-line flag parser:
//

#include "common.h"

#include "cmdline.h"

// Returns ("flag", some("foo")) when passed -flag=foo
static Pair<Str, Maybe<Str>> splitEqualsFlag(StrA flag) {
    auto it = flag.find('=');
    if (it == Str::npos)
        return make_pair(flag.substr(1), none);

    return make_pair(flag.substr(1, it - 1), flag.substr(it + 1));
}

namespace cl {

Vec<cl::PositionalOpt*>& positionalOptions() {
    static Vec<cl::PositionalOpt*> ret;
    return ret;
}

Map<Str, OptBase*>& optionsMap() {
    static Map<Str, OptBase*> ret;
    return ret;
}

PositionalOpt::PositionalOpt(Maybe<Str> def) : value(def) {
    positionalOptions().push_back(this);
}

OptBase::OptBase(StrA name) : name(name) {
    optionsMap()[name] = this;
}

} // namespace cl

void parseCommandLine(int argc, const char** argv) {
    auto& positionalOptions = cl::positionalOptions();
    auto& optionsMap = cl::optionsMap();

    for (int i = 1; i < argc; ++i) {
        Str current = argv[i];

        // Are we starting a flag or a positional argument?
        if (current[0] == '-') {
            Str flag;
            Maybe<Str> val;
            tie(flag, val) = splitEqualsFlag(current);

            if (auto arg = lookup(optionsMap, flag)) {
                Maybe<Str> nextArgument;
                if (i + 1 != argc)
                    nextArgument = argv[i + 1];

                // Returning true from handleParam indicates we consumed the next argument:
                if ((*arg)->handleParam(val, nextArgument))
                    ++i;
            } else {
                printf("unrecognized flag %s\n", argv[i]);
                exit(-1);
            }
        } else {
            if (positionalOptions.empty()) {
                printf("unrecognized positional option %s\n", argv[i]);
                exit(-1);
            }

            positionalOptions[0]->value = argv[i];
            positionalOptions.erase(positionalOptions.begin());
        }
    }
}
