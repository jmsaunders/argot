#pragma once

#include "typecheck_private.h"

struct SelfScope;
struct FunctionScope;

// A type checker scope:
struct TcScope {
    TcScope* parent = nullptr;

    bool hasReturned = false;

    Map<Str, Ref<Symbol>> symbols;

    TcScope();
    TcScope(TcScope* parent) : parent(parent) {}
    TcScope(RefA<TcScope> parent) : parent(parent.get()) {}
    virtual ~TcScope() {}

    void importFrom(RefA<TcScope> dep);

    virtual const SelfScope* findSelf() const { return parent ? parent->findSelf() : nullptr; }
    virtual const FunctionScope* findFunction() const {
        return parent ? parent->findFunction() : nullptr;
    }

    void registerGeneric(RefA<Generic> gen);
    void registerInterface(RefA<Interface> iface);
    void registerType(RefA<Type> ty);
    void registerCallSymbol(RefA<CallSymbol> sym);

    Ref<Symbol> lookupSymbol(StrA name) const;
    Ref<Type> lookupType(StrA name) const;
    Ref<Generic> lookupGeneric(StrA name) const;
    Ref<Interface> lookupTrait(StrA name) const;
    Ref<InterfaceGeneric> lookupTraitGeneric(StrA name) const;
};

// Provides a Self and template argument types in the scope:
enum SelfScopeFlags { SSF_Default, SSF_NoTemplates };

struct SelfScope : TcScope {
    Ref<Type> selfType;
    SelfScope(RefA<TcScope> parent, RefA<Type> selfType, SelfScopeFlags flags = SSF_Default);

    const SelfScope* findSelf() const override { return this; }
};

// A Self scope which populates with the selfType fields:
struct SelfImplScope : SelfScope {
    SelfImplScope(RefA<TcScope> parent, RefA<Type> selfType, SelfScopeFlags flags = SSF_Default);
};

struct FunctionScope : TcScope {
    Ref<Type> returnType;

    FunctionScope(RefA<TcScope> parent, RefA<Type> returnType, const ArgumentList& params,
                  VecA<Ref<TemplateArgType>> templates, bool hasSelf, const Maybe<VariadicInfo>& vi = none);

    FunctionScope(RefA<TcScope> parent, RefA<FunctionSignature> sig, bool hasSelf)
        : FunctionScope(parent, sig->returnType, sig->params, sig->templates, hasSelf,
                        sig->variadicConstraint) {
    }

    const FunctionScope* findFunction() const override { return this; }

    void validate(Ast::Block& block) const;
};

// @TODO this is a bit of a hack that the symbol table is really more of a scope construct.
// If we let the record types have a scope then we could unify this code easier... The important
// part is the symbols.
void registerCallSymbol(Map<Str, Ref<Symbol>>& symbols, RefA<CallSymbol> sym);