#include "common.h"

#include "scope.h"
#include "typecheck_private.h"

//
// Equivalent to:
//
//   interface NormType {
//       type ResultType
//       fun norm() -> ResultType
//   }
//

static Ref<Interface> normInterface() {
    static Ref<Interface> normInterface;
    if (normInterface)
        return normInterface;

    normInterface = Interface::make("Norm");

    auto normType = mkref<TraitInternalType>("ResultType", normInterface);
    normInterface->types["ResultType"] = normType;

    auto nameToken = Token(TIdent, nullptr, "norm", 4);
    auto norm = wrapShare(new FunctionSignature(nameToken, {}, normType, {}));
    normInterface->methods.push_back(norm);

    return normInterface;
}

//
// Equivalent to:
//
//   interface [Name]<Rhs> {
//       type ResultType
//       fun [name](rhs: Rhs) -> ResultType
//   }
//

Ref<InterfaceGeneric> arithmeticInterface(StrA name) {
    auto ret = mkref<InterfaceGeneric>(name);
    auto templateArg = mkref<TemplateArgType>("RhsType");
    ret->templates.push_back(templateArg);

    auto resultType = mkref<TraitInternalType>("ResultType", nullptr);
    ret->types["ResultType"] = resultType;

    auto methodName = heapBackString(name);
    methodName[0] = tolower(name[0]);

    Argument rhs = { "rhs", templateArg, VFlags::none };
    auto sumToken = Token(TIdent, nullptr, methodName, name.size());
    auto sumFn = wrapShare(new FunctionSignature(sumToken, { rhs }, resultType, {}));

    ret->methods.push_back(sumFn);
    return ret;
}

Ref<InterfaceGeneric> sumInterface() {
    static Ref<InterfaceGeneric> sumInterface;
    if (!sumInterface)
        sumInterface = arithmeticInterface("Sum");
    return sumInterface;
}

Ref<InterfaceGeneric> prodInterface() {
    static Ref<InterfaceGeneric> prodInterface;
    if (!prodInterface)
        prodInterface = arithmeticInterface("Prod");
    return prodInterface;
}

Ref<InterfaceGeneric> diffInterface() {
    static Ref<InterfaceGeneric> diffInterface;
    if (!diffInterface)
        diffInterface = arithmeticInterface("Diff");
    return diffInterface;
}

// Setup internal traits:
static void setupTraits(TcScope* scope) {
    scope->symbols["Norm"] = mkref<InterfaceSymbol>(normInterface());

    scope->symbols["Sum"] = mkref<GenericInterfaceSymbol>(sumInterface());
    scope->symbols["Prod"] = mkref<GenericInterfaceSymbol>(prodInterface());
    scope->symbols["Diff"] = mkref<GenericInterfaceSymbol>(diffInterface());

    // ConstructArgs:
    {
        auto catg = mkref<ConstructorArgTraitGeneric>();
        scope->symbols["ConstructorArgs"] = mkref<GenericInterfaceSymbol>(catg);
    }
}

TcScope::TcScope() {
    // Set up built-in types:
    symbols["i16"] = mkref<TypeSymbol>(Type::getI16());
    symbols["int"] = symbols["i32"] = mkref<TypeSymbol>(Type::getI32());
    symbols["i64"] = mkref<TypeSymbol>(Type::getI64());

    symbols["char"] = symbols["i8"] = symbols["byte"] = mkref<TypeSymbol>(Type::getI8());

    symbols["float"] = symbols["f32"] = mkref<TypeSymbol>(Type::getF32());

    symbols["bool"] = mkref<TypeSymbol>(Type::getBool());
    symbols["void"] = mkref<TypeSymbol>(Type::getVoid());

    setupTraits(this);
}

template <typename... Args> void scopeImportError(const char* fmt, Args... args) {
    printf("Scope import error: ");

    if (fmt)
        printf(fmt, args...);

    printf("\n");

    exit(-1);
}

void TcScope::importFrom(RefA<TcScope> mod) {
    for (const auto& [symbolName, symbol] : mod->symbols) {
        if (lookup(symbols, symbolName))
            scopeImportError("symbol name collision on import %s", symbolName.c_str());

        symbols[symbolName] = symbol;
    }
}

void TcScope::registerType(RefA<Type> type) {
    symbols[type->name] = mkref<TypeSymbol>(type);
}

void TcScope::registerGeneric(RefA<Generic> g) {
    symbols[g->name] = mkref<GenericSymbol>(g);
}

void TcScope::registerInterface(RefA<Interface> iface) {
    symbols[iface->name] = mkref<InterfaceSymbol>(iface);
}

Ref<Symbol> TcScope::lookupSymbol(StrA name) const {
    if (auto sym = lookup(symbols, name))
        return *sym;

    if (parent)
        return parent->lookupSymbol(name);
    return nullptr;
}

Ref<Type> TcScope::lookupType(StrA name) const {
    if (auto sym = lookupSymbol(name)) {
        if (auto ts = dyn_cast<TypeSymbol>(sym))
            return ts->type;
    }

    return nullptr;
}

Ref<Generic> TcScope::lookupGeneric(StrA name) const {
    if (auto sym = lookupSymbol(name)) {
        if (auto gs = dyn_cast<GenericSymbol>(sym))
            return gs->generic;
    }

    return nullptr;
}

Ref<Interface> TcScope::lookupTrait(StrA name) const {
    if (auto sym = lookupSymbol(name)) {
        if (auto is = dyn_cast<InterfaceSymbol>(sym))
            return is->iface;
    }

    return nullptr;
}

Ref<InterfaceGeneric> TcScope::lookupTraitGeneric(StrA name) const {
    if (auto sym = lookupSymbol(name)) {
        if (auto is = dyn_cast<GenericInterfaceSymbol>(sym))
            return is->tg;
    }

    return nullptr;
}

bool argumentListIdentical(const ArgumentList& a, const ArgumentList& b) {
    if (a.size() != b.size())
        return false;

    for (size_t i = 0; i < a.size(); ++i) {
        if (a[i].flags != b[i].flags)
            return false;
        if (!a[i].type->equals(b[i].type))
            return false;
    }

    return true;
}

bool FunctionsOverloadSymbol::alreadyExists(const ArgumentList& args) const {
    for (const auto& f : functions) {
        if (argumentListIdentical(args, f->sig->params))
            return true;
    }

    return false;
}

void registerCallSymbol(Map<Str, Ref<Symbol>>& symbols, RefA<CallSymbol> sym) {
    auto& fsym = symbols[sym->sig->name()];
    if (!fsym)
        fsym = mkref<FunctionsOverloadSymbol>();

    if (auto fos = dyn_cast<FunctionsOverloadSymbol>(fsym)) {
        if (fos->alreadyExists(sym->sig->params))
            error(sym->sig->nameToken, "function already exists");

        fos->functions.push_back(sym);
    } else {
        error(sym->sig->nameToken, "symbol with this name already exists");
    }
}

void TcScope::registerCallSymbol(RefA<CallSymbol> sym) {
    ::registerCallSymbol(symbols, sym);
}

SelfScope::SelfScope(RefA<TcScope> parent, RefA<Type> selfType, SelfScopeFlags flags)
    : TcScope(parent), selfType(selfType) {
    if (flags != SSF_NoTemplates) {
        if (auto gi = dyn_cast<GenericInstance>(selfType)) {
            for (auto t : gi->parent()->templates)
                registerType(t);
        }
    }
}

SelfImplScope::SelfImplScope(RefA<TcScope> parent, RefA<Type> selfType, SelfScopeFlags flags)
    : SelfScope(parent, selfType, flags) {
    if (auto body = selfType->recordType()) {
        // Just inherit the symbol scope of the record type:
        for (const auto& [name, val] : body->symbols) {
            symbols[name] = val;
        }
    }
}

FunctionScope::FunctionScope(RefA<TcScope> parent, RefA<Type> returnType,
                             const ArgumentList& params, const Vec<Ref<TemplateArgType>>& templates,
                             bool hasSelf, const Maybe<VariadicInfo>& vi)
    : TcScope(parent), returnType(returnType) {
    for (size_t i = 0; i < params.size(); ++i) {
        const auto& param = params[i];

        size_t paramNumber = i + (hasSelf ? 1 : 0);
        symbols[param.name] = mkref<FunArgSymbol>(param.name, paramNumber, param.type);
    }

    if (vi) {
        size_t paramNumber = params.size() + (hasSelf ? 1 : 0);
        symbols[vi->name] = mkref<FunArgSymbol>(vi->name, paramNumber, vi->type);
    }

    for (const auto& t : templates)
        registerType(t);
}

void FunctionScope::validate(Ast::Block& block) const {
    if (block.expr) {
        if (returnType->isVoid())
            error(block.expr->loc, "cannot yield value in void function");

        auto coercedExpr = attemptCoerceExpression(block.expr, returnType);
        if (!coercedExpr)
            error(block.expr->loc, "type mismatch on yielded value (got '%s' expected '%s')",
                  block.expr->type()->name.c_str(), returnType->name.c_str());

        block.expr = performCopyExprIfNeeded(coercedExpr);

        if (block.stmts.size() && is<T::ReturnStmt>(block.stmts.back()))
            error(block.expr, "cannot yield value after return");
    } else {
        if (!returnType->isVoid() && !blockReturns(block))
            error(*block.openingBrace, "function does not return a value");
    }
}
