//
// typecheck_expr.cpp
// Typechecking for expressions.
//

#include "common.h"
#include "scope.h"
#include "typecheck_private.h"
#include <sstream>

using TBlock = Ast::Block;

// Do all branches of this expression return?
bool exprReturns(RefA<TExpr> e) {
    using namespace Ast;

    if (auto ifExpr = dyn_cast<IfExpr>(e))
        return ifExpr->elseBody && blockReturns(ifExpr->body) && blockReturns(*ifExpr->elseBody);

    if (auto me = dyn_cast<MatchExpr>(e)) {
        for (const auto& arm : me->arms) {
            if (!blockReturns(arm.block))
                return false;
        }

        if (me->fallthrough)
            return blockReturns(*me->fallthrough);
        return me->completeMatch;
    }

    return false;
}

// Do all paths through this block explicitly return?
bool blockReturns(const TBlock& b) {
    using namespace Ast;

    if (b.stmts.empty())
        return false;

    const auto& lastStmt = b.stmts.back();
    if (is<ReturnStmt>(lastStmt))
        return true;

    if (auto exprStmt = dyn_cast<ExprStmt>(lastStmt))
        return exprReturns(exprStmt->expr);

    return false;
}

bool isDeref(RefA<TExpr> e) {
    auto u = dyn_cast<T::UnaryExpr>(e);
    return u && u->op == TStar;
}

// Can this expression evaluate to an lvalue?
bool isLvalue(RefA<TExpr> e) {
    using namespace Ast;
    return is<SelfExpr>(e) || is<VarRefExpr>(e) || is<MemberExpr>(e) || is<ArrayIndexExpr>(e) ||
           isDeref(e);
}

void unifyTypes(SourceLoc errorLoc, Ref<Type> lhs, Ref<Type> rhs) {
    if (auto result = canUnifyTypes(lhs, rhs))
        error(errorLoc, "%s", result->c_str());
}

static Ref<TExpr> actOnIfExpr(SourceLoc loc, RefA<TExpr> condition, const TBlock& then,
                              const Maybe<TBlock>& elseBody) {
    // Do some basic checks to make sure this kind of makes sense:
    if (then.expr) {
        if (!elseBody)
            error(condition, "if expressions yielding values require else");

        if (!blockReturns(*elseBody) && !elseBody->expr)
            error(then.expr, "else must yield value when if yields value");
    } else if (elseBody && elseBody->expr) {
        if (!blockReturns(then))
            error(elseBody->expr, "else cannot yield value without if yielding");
    }

    Ref<Type> thenType = then.expr ? then.expr->type() : nullptr;
    Ref<Type> elseType = (elseBody && elseBody->expr) ? elseBody->expr->type() : nullptr;

    Ref<Type> type = voidTy;
    if (thenType && elseType) {
        type = mkref<TypeVariable>();
        unifyTypes(loc, type, then.expr->type());
        unifyTypes(loc, type, elseBody->expr->type());
    } else {
        assert(!thenType && !elseType);
    }

    return mkref<T::IfExpr>(type, condition, then, elseBody);
}

// Fill out any un-populated part of template instantiation with type variables
// that can be unified with later for the complete type.

static void populateTemplateSubstitutions(SubstitutionMap& map, RefA<Generic> generic) {
    for (auto t : generic->templates) {
        auto& mappedTo = map[t];
        if (!mappedTo)
            mappedTo = mkref<TypeVariable>();
    }
}

static Ref<TExpr> actOnVariantInit(RefA<VariantSymbol> vs, SourceLoc loc, Ref<TExpr> init) {
    auto fieldType = vs->field.type;
    if (!fieldType && init)
        error(loc, "this variant initializer does not take an argument");

    Ref<VariantBaseType> variantType = dyn_cast<VariantType>(vs->variant);
    if (fieldType) {
        if (!init)
            error(loc, "this variant initializer requires an argument");

        // @TODO support implicit conversions here
        // Non-templated variant inits require exact type matches for now.
        auto initType = init->type();
        if (variantType && !fieldType->equals(initType)) {
            error(loc, "type mismatch: got %s (expected %s)", initType->name.c_str(),
                  fieldType->name.c_str());
        }

        init = performCopyExprIfNeeded(init);
    }

    if (!variantType) {
        auto gen = dyn_cast<VariantGeneric>(vs->variant);

        SubstitutionMap subs;
        populateTemplateSubstitutions(subs, gen);
        variantType = gen->instantiate(subs)->variantType();

        if (!variantType)
            error(loc, "invalid template instance");

        // Now attempt to unify:
        if (init) {
            auto instantiatedFieldType = variantType->fields[vs->fieldIndex].type;
            unifyTypes(init->loc, instantiatedFieldType, init->type());
        }
    }

    return mkref<T::VariantInitExpr>(variantType, vs->field, loc, vs->fieldIndex, init);
}

static Str argumentTypeList(const Vec<Ref<TExpr>>& args) {
    std::stringstream argumentTypes;
    bool first = true;

    for (const auto& arg : args) {
        if (!first)
            argumentTypes << ", ";
        argumentTypes << arg->type()->name;
        first = false;
    }

    return argumentTypes.str();
}

void validateTraitConstraintSatisfied(SourceLoc errorLoc, RefA<Interface> i, RefA<Type> type) {
    if (!constraintSatisfiedByType(i, type))
        error(errorLoc, "type %s does not support trait %s", type->name.c_str(), i->name.c_str());
}

// checkArgLength exists because variadic functions aren't going to match:
static bool validExactCall(const Vec<Ref<TExpr>>& callArguments,
                           const ArgumentList& functionArguments, bool checkArgLength = true) {
    if (checkArgLength && callArguments.size() != functionArguments.size())
        return false;

    // TODO augment with tryCoerce code to allow overloading with conversions:
    for (size_t i = 0; i < functionArguments.size(); ++i) {
        const auto& callArg = callArguments[i];
        const auto& callArgType = callArg->type();
        const auto& funArg = functionArguments[i];

        if (auto t = funArg.type->templateVariable()) {
            validateTraitConstraintSatisfied(callArg->loc, t->constraint, callArgType);
            continue;
        }

        if (!callArgType->equals(funArg.type))
            return false;
    }
    return true;
}

static bool validCall(Vec<Ref<TExpr>>& callArguments, const ArgumentList& functionArguments,
                      bool checkArgLength = true) {
    if (checkArgLength && callArguments.size() != functionArguments.size())
        return false;

    // TODO augment with tryCoerce code to allow overloading with conversions:
    for (size_t i = 0; i < functionArguments.size(); ++i) {
        auto& callArg = callArguments[i];
        const auto& callArgType = callArg->type();
        const auto& funArg = functionArguments[i];

        if (auto t = funArg.type->templateVariable()) {
            validateTraitConstraintSatisfied(callArg->loc, t->constraint, callArgType);
            continue;
        }

        if (!callArgType->equals(funArg.type)) {
            if (auto coerced = attemptCoerceExpression(callArg, funArg.type))
                callArg = coerced;
            else
                return false;
        }
    }
    return true;
}

Maybe<size_t> findConstructorIndexForCall(RefA<RecordBaseType> rec, VecA<Ref<TExpr>> args) {
    for (size_t i = 0; i < rec->constructors.size(); ++i) {
        const auto& c = rec->constructors[i];

        if (validExactCall(args, c.params))
            return i;
    }

    return none;
}

Maybe<size_t> findConstructorIndexForCall(RefA<Type> ty, VecA<Ref<TExpr>> args) {
    if (auto rec = ty->recordType())
        return findConstructorIndexForCall(rec, args);
    return none;
}

Maybe<Pair<Ref<FunctionSignature>, Vec<Ref<Type>>>>
inferTemplateCall(SourceLoc loc, RefA<FunctionSignature> sig, const Vec<Ref<TExpr>>& args,
                  const Vec<Ref<Type>>& providedTemplates) {
    const auto& variadic = sig->variadicConstraint;

    if (variadic) {
        // For variadic functions we need to have at least up to the variadic amount:
        if (args.size() < sig->params.size())
            return none;
    } else {
        // Argument counts must be exact for non-variadic functions:
        if (args.size() != sig->params.size())
            return none;
    }

    // Passing too many template arguments is never semantically valid:
    if (providedTemplates.size() > sig->templates.size())
        return none;

    //
    // Generate a template substitution map where everything maps to a unique type variable.
    //
    // The algorithm here is to then unify the passed-in argument types with the types of
    // the function parameters. If we succeed, then we'll have filled out the substitution
    // map with valid types depending on the arguments.
    //

    SubstitutionMap subs;
    for (size_t i = 0; i < sig->templates.size(); ++i) {
        if (i < providedTemplates.size())
            subs[sig->templates[i]] = providedTemplates[i];
        else
            subs[sig->templates[i]] = mkref<TypeVariable>();
    }

    auto newSig = replaceTypesFromMap(sig, subs);

    // Do regular type checking for the non-variadic portion:
    for (size_t i = 0; i < sig->params.size(); ++i)
        unifyTypes(loc, args[i]->type(), newSig->params[i].type);

    // Type-check the variadic portion:
    if (variadic) {
        auto variadicTemplateArg = subs[sig->templates.back()]->resolve();
        if (!variadicTemplateArg->typeVariable())
            error(loc, "cannot explicitly specify variadic template type");

        Vec<Ref<TExpr>> vargs;
        for (size_t i = sig->params.size(); i < args.size(); ++i)
            vargs.push_back(args[i]);

        // @TODO: Where do we want to do type-checking on the variadic traits?

        if (auto cat = dyn_cast<ConstructorArgTrait>(variadic->trait)) {
            // Make sure to do the substitution because we may be trying to construct something in
            // our template argument list:
            auto constructingType = replaceTypesFromMap(cat->forType, subs);
            if (auto index = findConstructorIndexForCall(constructingType, vargs)) {
                variadicTemplateArg->typeVariable()->bound =
                    mkref<ConstructorArgTraitIndex>(cat, constructingType, *index);
            } else {
                error(loc, "variadic constraints not met");
            }
        }
    }

    for (auto& [from, to] : subs) {
        to = to->resolve();

        // @TODO:
        // This can happen when we're calling our function with an unresolved type variable.
        // We need concrete unification to make the overload so maybe we should let this proceed
        // and error out later if we don't eventually resolve a type for it.
        //
        // I foresee this happening by keeping a list of all type variables introduced in a function
        // and validating at the end that they're all unified. That's probably the most natural way
        // of handling this.

        if (to->typeVariable())
            error(loc, "successful type unification did not yield value for %s",
                  from->name.c_str());
    }

    // Regenerate the template list, as it's what we're looking for:
    Vec<Ref<Type>> templateList;
    for (const auto& t : sig->templates)
        templateList.push_back(*lookup(subs, t));

    // Make sure the trait instantiations are correct:
    for (size_t i = 0; i < templateList.size(); ++i) {
        // Make sure to regenerate the constraints that may be parameterized on the types we have
        // inferred:
        auto constraint = replaceTypesFromMap(sig->templates[i]->constraint, subs); 

        validateTraitConstraintSatisfied(loc, constraint, templateList[i]);
    }

    auto resolvedSig = replaceTypesFromMap(sig, subs);

    // Make sure we can actually do this call:
    if (!validExactCall(args, resolvedSig->params, variadic == none))
        return none;

    return make_pair(resolvedSig, templateList);
}

Ref<TExpr> FunctionsOverloadSymbol::createCallExpr(RefA<TcScope> scope, Token nameToken,
                                                   Vec<Ref<TExpr>> args,
                                                   const Maybe<Vec<Ref<Type>>>& templates) {
    using namespace Ast;

    auto nameLoc = nameToken.loc();
    for (const auto& sym : functions) {
        if (auto mcs = dyn_cast<MethodCallSymbol>(sym)) {
            auto sig = mcs->sig;
            if (validCall(args, sig->params)) {
                auto self = mkref<ImplicitSelfExpr>(scope->findSelf()->selfType, nameLoc);

                return mkref<MemberCallExpr>(nameLoc, MemberAccessKind::Dot, self, sig, args,
                                             mcs->path);
            }
        }

        if (auto gcs = dyn_cast<GlobalCallSymbol>(sym)) {
            if (gcs->sig->isTemplated()) {
                Vec<Ref<Type>> providedTemplates;
                if (templates)
                    providedTemplates = *templates;

                if (auto result = inferTemplateCall(nameLoc, gcs->sig, args, providedTemplates)) {
                    auto [sig, inferredTemplates] = *result;

                    return mkref<TemplateCallExpr>(sig, nameLoc, inferredTemplates, args);
                }
            } else {
                if (!templates && validCall(args, gcs->sig->params)) {
                    return mkref<CallExpr>(gcs->sig, nameLoc, args);
                }
            }
        }
    }

    return nullptr;
}

Ref<TExpr> generateConstructorCall(Token nameToken, RefA<RecordBaseType> rec,
                                   VecA<Ref<TExpr>> args, bool nullOnFailure) {
    if (auto index = findConstructorIndexForCall(rec, args)) {
        const auto& c = rec->constructors[*index];
        return mkref<T::ConstructorExpr>(nameToken, *index, rec, args, c.params);
    }

    if (nullOnFailure)
        return nullptr;

    error(nameToken.loc(), "no constructor call matching type signature");
}

static Ref<TExpr> actOnCallExpr(RefA<TcScope> scope, Token nameToken,
                                const Maybe<Vec<Ref<Type>>>& templates, Vec<Ref<TExpr>> args) {
    auto name = nameToken.text();
    auto nameLoc = nameToken.loc();

    // Intrinsics:
    if (nameToken.text() == "constructInto" && !templates) {
        if (args.size() != 2)
            error(nameToken, "constructInto takes 2 arguments)");

        bool isVoidPtr = false;
        if (auto ptr = args[0]->type()->pointerType()) {
            if (ptr->interior->isVoid())
                isVoidPtr = true;
        }

        if (!isVoidPtr)
            error(args[0], "expected void*");

        return mkref<T::ConstructIntoExpr>(nameToken, args[0], args[1]);
    }

    if (auto sym = scope->lookupSymbol(name)) {
        if (is<VariableSymbol>(sym))
            error(nameLoc, "cannot call variables");

        if (auto vs = dyn_cast<VariantSymbol>(sym)) {
            if (templates)
                error(nameToken, "invalid templated call");

            if (args.size() != 1)
                error(nameToken, "expected single argument to variant constructor");

            return actOnVariantInit(vs, nameToken.loc(), args[0]);
        }

        // Vanilla function calls:
        if (auto fos = dyn_cast<FunctionsOverloadSymbol>(sym)) {
            if (auto call = fos->createCallExpr(scope, nameToken, args, templates))
                return call;
        }

        // Constructors:

        if (auto ts = dyn_cast<TypeSymbol>(sym)) {
            if (templates)
                error(nameToken, "template list provided for non-templated type");

            if (auto rec = ts->type->recordType()) {
                return generateConstructorCall(nameToken, rec, args);
            } else if (auto tv = ts->type->templateVariable()) {
                // Here is where we handle passing variadic template arguments into a constructor:
                if (args.size() == 1) {
                    if (auto tv = args[0]->type()->templateVariable()) {
                        if (auto cat = dyn_cast<ConstructorArgTrait>(tv->constraint)) {
                            if (cat->forType->equals(ts->type)) {
                                // If we get to this point, it means we have a template pack that we
                                // know can be an overload for the type we have. Create a special
                                // constructor expr:
                                return mkref<T::ConstructFromVariadicExpr>(nameToken.loc(),
                                                                           ts->type);
                            }
                        }
                    }
                }
            } else {
                error(nameLoc, "non-records do not have constructors");
            }
        }

        if (auto ks = dyn_cast<GenericSymbol>(sym)) {
            if (!templates)
                error(nameToken, "generic constructors require template arguments");

            if (auto rec = dyn_cast<RecordGeneric>(ks->generic)) {
                if (auto type = ks->generic->instantiate(*templates)->recordType())
                    return generateConstructorCall(nameToken, type, args);

                error(nameToken, "invalid template instance");
            } else {
                error(nameLoc, "non-records do not have constructors");
            }
        }
    }

    auto argumentTypes = argumentTypeList(args);

    Str templatesStr;
    if (templates)
        templatesStr = generateTemplateListString(*templates);

    error(nameToken, "could not resolve %s%s(%s)", name.c_str(), templatesStr.c_str(),
          argumentTypes.c_str());
}

// TODO:
//   - This can be optimized by not always re-creating the pointer types:
//   - There are other ways types can be subordinate like templates. These need replacing too.

Ref<Type> replaceTraitType(RefA<Type> input, RefA<Type> traitSelf) {
    if (auto ptr = dyn_cast<PointerType>(input))
        return PointerType::of(replaceTraitType(ptr->interior, traitSelf));

    if (auto self = dyn_cast<TraitSelfType>(input))
        return traitSelf;

    return input;
}

// typeVar is the type variable constrained on the trait we're trying to do the call on.
//   i.e. it's Self in trait scope

static bool validTraitCall(RefA<Type> typeVar, const Vec<Ref<TExpr>>& callArgs,
                           RefA<FunctionSignature> sig) {
    if (callArgs.size() != sig->params.size())
        return false;

    // TODO augment with tryCoerce code to allow overloading with conversions:
    for (size_t i = 0; i < callArgs.size(); ++i) {
        const auto& callArg = callArgs[i];
        const auto& funArg = sig->params[i];

        if (!callArg->type()->equals(funArg.type)) {
            auto replaced = replaceTraitType(funArg.type, typeVar);
            if (!replaced->equals(callArg->type()))
                return false;
        }

        if (funArg.flags == VFlags::ref) {
            if (!isLvalue(callArg))
                return false;
        }
    }
    return true;
}

// Semantic analysis on member calls
// Example: a.b<T>(123)
static Ref<TExpr> actOnMemberCall(bool deref, RefA<TExpr> lhs, Token nameToken,
                                  const Vec<Ref<TExpr>>& args) {
    auto name = nameToken.text();

    // TODO should never use bare type because it's possibly a typevar:
    auto objType = lhs->type();
    if (deref) {
        if (auto ptr = objType->pointerType())
            objType = ptr->interior;
        else
            error(nameToken, "Attempted to do arrow member call on non-pointer type");
    }

    // Check whether we're trying to dispatch on a record:
    if (auto record = objType->recordType()) {
        if (auto sym = lookup(record->symbols, name)) {

            // @TODO: Using FunctionOverloadSymbol for things inside the method symbol table is very
            // clumsy. This concept might be better just removed since I think overloading should
            // only happen through type classes...

            if (auto fos = dyn_cast<FunctionsOverloadSymbol>(*sym)) {
                for (auto m : fos->functions) {
                    auto method = dyn_cast<MethodCallSymbol>(m);
                    if (validExactCall(args, method->sig->params)) {
                        if (method->msig->isVirtual)
                            unreachable("calling virtual method");

                        auto ty = deref ? T::MemberAccessKind::Arrow : T::MemberAccessKind::Dot;
                        return mkref<T::MemberCallExpr>(lhs->loc, ty, lhs, method->msig, args, method->path);
                    }
                }
            }
        }

        auto argumentTypes = argumentTypeList(args);
        error(nameToken, "could not resolve method %s(%s) on record %s", name.c_str(),
              argumentTypes.c_str(), objType->name.c_str());
    }

    // Trait dispatch:
    if (auto tv = objType->templateVariable()) {
        if (auto trait = tv->constraint) {
            for (auto [i, method] : enumerate(trait->methods)) {
                if (method->name() == name && validTraitCall(objType, args, method)) {
                    auto ty = deref ? T::MemberAccessKind::Arrow : T::MemberAccessKind::Dot;
                    return mkref<T::InterfaceDispatchExpr>(ty, lhs, trait, i, args, method->returnType);
                }
            }
        }
    }

    auto argumentTypes = argumentTypeList(args);
    error(nameToken, "could not resolve method %s(%s) on %s", name.c_str(), argumentTypes.c_str(),
          objType->name.c_str());
}

// A brace-initialized record like: Vec3.{ 1.f, 0.f, 0.f }
static Ref<TExpr> actOnRecordInit(SourceLoc loc, RefA<Type> type, Vec<Ref<TExpr>> inits) {
    auto record = type->recordType();
    if (!record)
        error(loc, "cannot use struct literal of non-struct type");

    if (record->constructors.size() > 0)
        error(loc, "cannot brace-initialize record with constructors");

    unsigned offset = 0;
    for (auto& init : inits) {
        auto ourField = record->fields[offset++];

        auto coerced = attemptCoerceExpression(init, ourField.type);
        if (!coerced) {
            error(init, "cannot coerce field %s from type %s", ourField.name.c_str(),
                  init->type()->name.c_str());
        }

        init = performCopyExprIfNeeded(coerced);
    }

    for (; offset < record->fields.size(); ++offset) {
        const auto& field = record->fields[offset];
        if (!field.type->defaultConstructible())
            error(loc, "must specify values for non-default constructible fields");
    }

    return mkref<T::RecordInitExpr>(loc, type, inits);
}

static Pair<Ref<TcScope>, Ref<T::Pattern>> mkpattern(RefA<TcScope> scope, RefA<VariantBaseType> vt,
                                                     Token label, Maybe<Token> maybeUnwrap) {
    // Find which field we're trying to unwrap:
    Maybe<VariantField> field;
    unsigned fieldId = 0;

    for (const auto& f : vt->fields) {
        if (f.name() == label.text()) {
            // Found it:
            field = f;
            break;
        }
        ++fieldId;
    }

    if (!field)
        error(label, "could not find field in match");

    if (maybeUnwrap) {
        auto unwrap = *maybeUnwrap;
        if (!field->type)
            error(unwrap, "cannot unwrap match something with no contained value");

        auto unwrapName = unwrap.text();

        auto sema = mkref<TcScope>(scope);
        auto pat = mkref<T::UnwrapPattern>(field->type, unwrapName, label, fieldId);
        scope->symbols[unwrapName] = mkref<UnwrappedSymbol>(unwrapName, field->type);
        return make_pair(move(sema), pat);
    } else {
        return make_pair(mkref<TcScope>(scope), mkref<T::VariantPattern>(label, fieldId));
    }
}

static Ref<TExpr> actOnMatchExpr(Parsetree::MatchExpr* e, RefA<TcScope> scope) {
    auto match = checkExpr(e->match, scope);
    auto vt = match->type()->variantType();

    if (!vt)
        error(match, "can only match against variant types right now");

    const auto& cases = e->arms;
    if (cases.empty())
        error(match, "match cannot have no arms");

    Ref<Type> matchType;
    auto typecheckArm = [&](Ref<TExpr>& expr) { unifyTypes(e->loc, expr->type(), matchType); };

    // This loop does a lot of things:
    //  1. Finds the arms we're matching against
    //  2. Finds and removes the wildcard pattern
    //  3. Unifies the yielded types

    Set<Str> presentArms;
    Maybe<T::Block> fallthrough;
    Vec<T::MatchArm> arms;
    for (const auto& arm : e->arms) {
        auto armName = arm.pat->name;
        auto armText = armName.text();

        TBlock b;
        if (armText == "_") {
            if (fallthrough)
                error(arm.pat->name, "match block cannot have more than one wildcard arm");

            b = checkBlock(arm.block, scope);

            fallthrough = b;
        } else {
            if (lookup(presentArms, armText))
                error(armName, "duplicate match arm");
            presentArms.insert(armText);

            auto [newScope, pat] = mkpattern(scope, vt, armName, arm.pat->unwrapped);
            b = checkBlock(arm.block, newScope);
            arms.push_back({ pat, b });
        }

        if (b.expr) {
            if (!matchType)
                matchType = mkref<TypeVariable>();

            typecheckArm(b.expr);
        }
    }

    // See if we're missing any arms:
    if (!fallthrough && matchType) {
        Vec<Str> missingFields;
        for (const auto& field : vt->fields) {
            if (!lookup(presentArms, field.name()))
                missingFields.push_back(field.name());
        }

        if (!missingFields.empty()) {
            std::stringstream ss;
            bool first = true;
            for (const auto& m : missingFields) {
                if (!first)
                    ss << ", ";
                ss << m;
                first = false;
            }

            warnAtLocation(match->loc, "incomplete match, missing: %s", ss.str().c_str());
        }
    }

    // Make sure we don't have a fallthrough with a complete match:
    bool completeMatch = presentArms.size() == vt->fields.size();
    if (completeMatch && fallthrough)
        error(match, "matching all variant fields will never use fallthrough");

    // Make sure if we have a yielded type, that anything not yielding
    // is returning:
    if (matchType) {
        for (auto& c : arms) {
            if (!c.block.expr && !blockReturns(c.block))
                error(c.pat->loc, "match arm neither yields nor returns");
        }
    } else {
        // No branches yield, we're of void type:
        matchType = Type::getVoid();
    }

    return mkref<T::MatchExpr>(matchType, e->loc, match, arms, fallthrough, false);
}

struct TypecheckExpr : P::ExprVisitor {
    Ref<T::Expr> result;
    Ref<TcScope> scope;

    TypecheckExpr(RefA<TcScope> scope) : scope(scope) {}

    void accept(Parsetree::IfExpr* e) {
        auto condition = checkExpr(e->condition, scope);
        auto then = checkBlock(e->body, scope);

        Maybe<T::Block> elseBody;
        if (e->elseBody)
            elseBody = checkBlock(*e->elseBody, scope);

        result = actOnIfExpr(e->loc, condition, then, elseBody);
    }

    void accept(Parsetree::CallExpr* e) {
        Vec<Ref<TExpr>> args;
        for (const auto& arg : e->args)
            args.push_back(checkExpr(arg, scope));

        Maybe<Vec<Ref<Type>>> templates;
        if (e->templates) {
            templates = Vec<Ref<Type>>();
            for (const auto& t : *e->templates)
                templates->push_back(resolve(scope, t));
        }

        result = actOnCallExpr(scope, e->id, templates, args);
    }

    void accept(Parsetree::IdExpr* e) {
        auto nameToken = e->id;
        if (auto sym = scope->lookupSymbol(e->id.text())) {
            if (auto variant = dyn_cast<VariantSymbol>(sym)) {
                result = actOnVariantInit(variant, nameToken.loc(), nullptr);
            } else if (auto variable = dyn_cast<VariableSymbol>(sym)) {
                result = mkref<T::VarRefExpr>(variable, nameToken.loc());
            } else if (auto fos = dyn_cast<FunctionsOverloadSymbol>(sym)) {
                if (fos->functions.size() != 1)
                    error(nameToken, "ambiguous function reference");

                auto f = fos->functions.front();
                result = mkref<T::FunctionRefExpr>(f->sig, nameToken.loc());
            } else {
                error(nameToken, "invalid use of symbol");
            }
        }

        if (!result)
            error(nameToken, "unrecognized identifier");
    }

    void accept(Parsetree::SelfExpr* e) {
        if (auto s = scope->findSelf())
            result = mkref<Ast::SelfExpr>(s->selfType, e->loc);
        else
            error(e, "cannot use 'self' in this context");
    }

    void accept(Parsetree::InfixExpr* e) {
        result = actOnInfixExpr(checkExpr(e->lhs, scope), e->op, checkExpr(e->rhs, scope));
    }

    void accept(Parsetree::UnaryExpr* e) {
        result = actOnUnaryOp(e->op, checkExpr(e->e, scope));
    }

    void accept(Parsetree::ArrayIndexExpr* e) {
        auto arr = checkExpr(e->lhs, scope);
        auto idx = checkExpr(e->index, scope);

        if (!is<IntType>(idx->type()))
            error(idx, "array index must be integral type");

        auto at = arr->type();
        if (auto ty = at->pointerType()) {
            result = mkref<T::ArrayIndexExpr>(ty->interior, arr, idx);
        } else if (auto array = dyn_cast<ArrayType>(at)) {
            result = mkref<T::ArrayIndexExpr>(array->baseType, arr, idx);
        } else {
            error(arr, "cannot use array operator on non-pointer type %s", at->name.c_str());
        }
    }

    void accept(Parsetree::DotExpr* e) {
        auto lhs = checkExpr(e->lhs, scope);

        auto type = lhs->type();
        auto field = e->field.text();

        if (e->arrow) {
            if (auto ptr = type->pointerType())
                type = ptr->interior;
            else
                error(lhs, "arrow operator used on non-pointer type");
        }
        auto ty = e->arrow ? T::MemberAccessKind::Arrow : T::MemberAccessKind::Dot;

        if (auto record = type->recordType()) {
            if (auto sym = lookup(record->symbols, field)) {
                if (auto mvs = dyn_cast<MemberVarSymbol>(*sym))
                    result = mkref<T::MemberExpr>(ty, lhs, mvs);
                else
                    error(e->field, "invalid reference"); // @TODO better error message here
            } else {
                error(e->field, "record %s has no field %s", record->name.c_str(), field.c_str());
            }
        } else {
            error(lhs, "attempted to access field on non-record type %s", type->name.c_str());
        }
    }

    void accept(Parsetree::DotCallExpr* e) {
        auto lhs = checkExpr(e->lhs, scope);
        Vec<Ref<TExpr>> args;
        for (const auto& arg : e->args)
            args.push_back(checkExpr(arg, scope));

        result = actOnMemberCall(e->arrow, lhs, e->field, args);
    }

    void accept(Parsetree::CastExpr* cast) {
        auto ty = resolve(scope, cast->type);
        auto e = checkExpr(cast->lhs, scope);

        // TODO validate..
        result = mkref<T::CastExpr>(ty, e);
    }

    void accept(Parsetree::MatchExpr* e) { result = actOnMatchExpr(e, scope); }

    void accept(Parsetree::RecordInitExpr* e) {
        auto type = resolve(scope, e->type);
        Vec<Ref<TExpr>> inits;
        for (const auto& arg : e->inits)
            inits.push_back(checkExpr(arg, scope));

        result = actOnRecordInit(e->loc, type, inits);
    }

    void accept(Parsetree::SizeofExpr* e) {
        auto type = resolve(scope, e->type);
        result = mkref<T::SizeofExpr>(type, e->loc);
    }

    void accept(Parsetree::NormExpr* e) {
        auto of = checkExpr(e->of, scope);

        Vec<Ref<TraitInstanceInfo>>* traitsList;
        if (auto gi = of->type()->genericInstance())
            traitsList = &gi->parent()->traits;
        else
            traitsList = &of->type()->traits;

        if (auto arr = dyn_cast<ArrayType>(of->type())) {
            result = mkref<T::IntLiteralExpr>((unsigned) arr->length, Type::getI32(), e->loc);
            return;
        }

        for (const auto& tinst : *traitsList) {
            if (tinst->trait->name == "Norm") {
                auto ty = T::MemberAccessKind::Dot;
                Vec<Ref<TExpr>> args;
                result = mkref<T::InterfaceDispatchExpr>(ty, of, tinst->trait, 0, args,
                                                     tinst->methods[0]->returnType);
            }
        }

        if (!result)
            error(of, "type %s is not Normal", of->type()->name.c_str());
    }

    void accept(Parsetree::ArrayInitExpr* e) {
        unreachable("implement regular array initialization");
    }

    // Given an expression, attempt to extract a constant integer out of it:
    Maybe<int> extractConstInt(RefA<T::Expr> e) {
        if (auto lit = dyn_cast<T::IntLiteralExpr>(e))
            return lit->val;

        if (auto infix = dyn_cast<T::InfixExpr>(e)) {
            auto lhs = extractConstInt(infix->lhs), rhs = extractConstInt(infix->rhs);

            if (lhs && rhs) {
                switch (infix->op) {
                    case TPlus: return *lhs + *rhs;
                    case TMinus: return *lhs - *rhs;
                    case TStar: return *lhs * *rhs;
                    case TSlash: return *lhs / *rhs;
                }
            }
        }

        return none;
    }

    void accept(Parsetree::ArrayRepeatExpr* e) {
        auto init = checkExpr(e->init, scope);
        auto count = checkExpr(e->count, scope);

        if (auto countLiteral = extractConstInt(count))
            result = mkref<T::ArrayRepeatExpr>(e->loc, init, *countLiteral);
        else
            error(count, "expected a constant integer");
    }

    void accept(Parsetree::IntLiteralExpr* e) {
        switch (e->type) {
            case Parsetree::ITT_CHAR:
                result = mkref<T::IntLiteralExpr>(e->val, Type::getI8(), e->loc);
                return;
            case Parsetree::ITT_INT:
                result = mkref<T::IntLiteralExpr>(e->val, Type::getI32(), e->loc);
                return;
        }

        unreachable("bad int literal type");
    }

    //
    // Simple literals:
    //

    void accept(Parsetree::StringLiteralExpr* e) {
        result = mkref<T::StringLiteralExpr>(e->text, e->loc);
    }
    void accept(Parsetree::FloatLiteralExpr* e) {
        result = mkref<T::FloatLiteralExpr>(e->val, e->loc);
    }
    void accept(Parsetree::BoolLiteralExpr* e) {
        result = mkref<T::BoolLiteralExpr>(e->val, e->loc);
    }
};

Ref<TExpr> checkExpr(RefA<PExpr> e, RefA<TcScope> scope) {
    if (scope->hasReturned)
        error(e->loc, "returned scopes cannot yield a value");

    TypecheckExpr tc(scope);
    e->visit(&tc);
    assert(tc.result);
    return tc.result;
}
