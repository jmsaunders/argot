//
// typecheck_operator.cpp
// Typechecking for prefix and infix operators.
//

#include "common.h"
#include "typecheck_private.h"

bool isIntegralType(Ref<Type> type) {
    return bool(type->resolve()->intType());
}

bool arithmeticType(Ref<Type> type) {
    type = type->resolve();
    return is<IntType>(type) || is<FloatType>(type);
}

Ref<T::Expr> actOnUnaryOp(Token optok, RefA<T::Expr> expr) {
    using namespace Ast;

    auto op = optok.type;

    // Pointer dereference
    if (op == TStar) {
        if (auto ptr = expr->type()->pointerType())
            return mkref<UnaryExpr>(ptr->interior, optok, expr);
        error(expr, "cannot derefence non-pointer type %s", expr->type()->name.c_str());
    }

    // Logical not
    if (op == TBang) {
        if (!expr->type()->isBool())
            error(expr, "cannot use unary 'not' on non-boolean expression");
        return mkref<UnaryExpr>(boolTy, optok, expr);
    }

    // Address of
    if (op == TAmp) {
        if (!isLvalue(expr))
            error(expr, "cannot take address of non-lvalue");

        return mkref<UnaryExpr>(PointerType::of(expr->type()), optok, expr);
    }

    // Arithmetic negation
    if (op == TMinus) {
        if (!arithmeticType(expr->type()))
            error(expr, "Cannot negate type '%s'", expr->type()->name.c_str());

        return mkref<UnaryExpr>(expr->type(), optok, expr);
    }

    unreachable("unrecognized unary operator");
}

//
// Arithmetic helpers:
//

Ref<TExpr> attemptCoerceExpression(RefA<TExpr> e, RefA<Type> type) {
    using namespace Ast;

    auto lhsType = e->type();
    auto rhsType = type->resolve();

    if (lhsType->equals(rhsType))
        return e;

    if (auto ptr = rhsType->pointerType()) {
        // Handle accepting 0 as a pointer type:
        if (auto lit = dyn_cast<IntLiteralExpr>(e)) {
            if (lit->val == 0)
                return mkref<CastExpr>(type, e);
        }

        // Any pointer type can decay to a void*
        if (ptr->interior->isVoid()) {
            if (e->type()->pointerType())
                return mkref<CastExpr>(type, e);
        }
    }

    if (is<GenericInstance>(lhsType) && is<GenericInstance>(rhsType)) {
        unifyTypes(e->loc, lhsType, rhsType);
        return e;
    }

    // Try to do simple numeric coercion:
    Vec<Ref<Type>> promotionLadder = {
        Type::getI8(),
        Type::getI32(),
        Type::getI64(),
        Type::getF32(),
    };

    auto fromIt = find(begin(promotionLadder), end(promotionLadder), lhsType);
    auto toIt = find(begin(promotionLadder), end(promotionLadder), rhsType);

    if (fromIt != end(promotionLadder) && toIt != end(promotionLadder)) {
        // can't go up:
        if (fromIt > toIt)
            return nullptr;
        return mkref<CastExpr>(type, e);
    }

    return nullptr;
}

namespace {
using namespace Ast;

Pair<Ref<Expr>, Ref<Expr>> coerceArithmetic(Ref<Expr> lhs, Ref<Expr> rhs) {
    if (auto newLhs = attemptCoerceExpression(lhs, rhs->type()))
        return make_pair(newLhs, rhs);
    if (auto newRhs = attemptCoerceExpression(rhs, lhs->type()))
        return make_pair(lhs, newRhs);

    error(rhs, "arithmetic operator not defined for these types");
}

Ref<Expr> handleTraitOverload(RefA<Expr> lhs, TokenType op, Ref<Expr> rhs,
                              RefA<InterfaceGenericInstance> gi, const SubstitutionMap& sm) {
    Ref<Type> resolvedResultType;
    if (auto resultType = lookup(gi->types, "ResultType")) {
        if (auto mapped = lookup(sm, *resultType))
            resolvedResultType = *mapped;
        else
            resolvedResultType = *resultType;

        bool valid = false;
        auto arg = gi->args.begin()->second;
        if (op == TPlus) {
            if (gi->parent->name == "Sum" && gi->args.size() == 1 && arg->equals(rhs->type()))
                valid = true;
        } else if (op == TStar) {
            if (gi->parent->name == "Prod" && gi->args.size() == 1 && arg->equals(rhs->type()))
                valid = true;
        } else if (op == TMinus) {
            if (gi->parent->name == "Diff" && gi->args.size() == 1 && arg->equals(rhs->type()))
                valid = true;
        }

        if (valid) {
            Vec<Ref<Expr>> args = { rhs };
            return mkref<InterfaceDispatchExpr>(MemberAccessKind::Dot, lhs, gi, 0, args,
                                                resolvedResultType);
        }
    }

    return nullptr;
}

Ref<Expr> actOnArithmetic(Ref<Expr> lhs, TokenType op, Ref<Expr> rhs) {
    // Handle the operator overloading case first:
    if (op == TPlus || op == TStar || op == TMinus) {
        auto lt = lhs->type();
        for (const auto& traitInfo : lt->traits) {
            if (auto gi = dyn_cast<InterfaceGenericInstance>(traitInfo->trait)) {
                if (auto ret = handleTraitOverload(lhs, op, rhs, gi, traitInfo->types))
                    return ret;
            }
        }

        if (auto tv = lt->templateVariable()) {
            if (auto trait = tv->constraint) {
                if (auto gi = dyn_cast<InterfaceGenericInstance>(trait)) {
                    if (auto ret = handleTraitOverload(lhs, op, rhs, gi, {}))
                        return ret;
                }
            }
        }
    }

    auto opString = operatorToString(op);

    if (!arithmeticType(lhs->type()) || !arithmeticType(rhs->type()))
        error(lhs, "[%s] %s [%s] not supported", lhs->type()->name.c_str(), opString,
              rhs->type()->name.c_str());

    if (op == TAmp || op == TPipe || op == TCaret) {
        if (!isIntegralType(lhs->type()))
            error(lhs, "cannot do bitwise operations on non-integral types");
    }

    if (op == TMod && !isIntegralType(lhs->type()))
        error(lhs, "cannot do modulus on non-integral types");

    tie(lhs, rhs) = coerceArithmetic(lhs, rhs);
    return mkref<InfixExpr>(lhs->type(), op, lhs, rhs);
}

Ref<Expr> actOnComparison(Ref<Expr> lhs, Token op, Ref<Expr> rhs) {
    if (!arithmeticType(lhs->type()))
        error(lhs, "cannot do comparisons on non-arithmetic type %s", lhs->type()->name.c_str());
    if (!arithmeticType(rhs->type()))
        error(rhs, "cannot do comparisons on non-arithmetic type %s", rhs->type()->name.c_str());

    tie(lhs, rhs) = coerceArithmetic(lhs, rhs);
    return mkref<InfixExpr>(boolTy, op.type, lhs, rhs);
}

Ref<Expr> actOnAssignment(const Ref<Expr>& lhs, Token opToken, const Ref<Expr>& baseRhs) {
    if (!isLvalue(lhs))
        error(lhs, "left hand of assignment is not an lvalue");

    auto rhs = performCopyExprIfNeeded(baseRhs);

    if (auto rt = lhs->type()->recordType()) {
        if (rt->explicitDestructorDecl)
            error(lhs, "TODO: support destruction/reconstruction when assigning object types");
    }

    auto op = opToken.type;

    if (op != TEq) {
        auto opType = TokenType(op - (TStarEq - TStar));
        rhs = actOnArithmetic(lhs, opType, rhs);
    }

    if (auto newRhs = attemptCoerceExpression(rhs, lhs->type()))
        return mkref<AssignmentExpr>(lhs, opToken, newRhs);

    error(lhs, "cannot coerce rhs (%s) into lhs (%s) type", rhs->type()->name.c_str(),
          lhs->type()->name.c_str());
}

Ref<Expr> actOnLogicalOperator(const Ref<Expr>& lhs, Token opToken, const Ref<Expr>& rhs) {
    if (!lhs->type()->isBool())
        error(lhs, "expected boolean");
    if (!rhs->type()->isBool())
        error(rhs, "expected boolean");

    return mkref<LogicalOpExpr>(opToken.type, lhs, rhs);
}

Ref<Expr> actOnShiftOperator(const Ref<Expr>& lhs, Token opToken, const Ref<Expr>& rhs) {
    if (!lhs->type()->intType())
        error(lhs, "expected integer");
    if (!rhs->type()->intType())
        error(rhs, "expected integer");

    ShiftType ty;
    switch (opToken.type) {
        case TLAngleLAngle: ty = ShiftType::left; break;
        case TRAngleRAngle: ty = ShiftType::right; break;

        default: unreachable("expected shift operator");
    }

    return mkref<ShiftExpr>(ty, lhs, rhs);
}

} // namespace

Ref<Expr> actOnEqTest(const Ref<Expr>& lhs, Token opToken, const Ref<Expr>& rhs) {
    auto op = opToken.type;

    auto lhsType = lhs->type();
    auto rhsType = rhs->type();

    // For arithmetic types, find a convenient unifying type:
    if (arithmeticType(lhsType) && arithmeticType(rhsType)) {
        auto [newLhs, newRhs] = coerceArithmetic(lhs, rhs);
        return mkref<InfixExpr>(boolTy, op, lhs, rhs);
    }

    bool eq = op == TEqEq;

    if (lhsType->pointerType() || rhsType->pointerType()) {
        if (auto newLhs = attemptCoerceExpression(lhs, rhs->type()))
            return mkref<EqTestExpr>(newLhs, eq, rhs);
        if (auto newRhs = attemptCoerceExpression(rhs, lhs->type()))
            return mkref<EqTestExpr>(lhs, eq, newRhs);
    }

    if (lhsType->equals(rhsType)) {
        return mkref<EqTestExpr>(lhs, eq, rhs);
    } else if (lhsType->genericInstance() && rhsType->genericInstance()) {
        unifyTypes(lhs->loc, lhsType, rhsType);
        return mkref<EqTestExpr>(lhs, eq, rhs);
    }

    error(opToken, "cannot test equivalence on these types");
}

Ref<Expr> actOnInfixExpr(const Ref<Expr>& lhs, Token opToken, const Ref<Expr>& rhs) {
    auto op = opToken.type;
    switch (op) {
        // Arithmetic:
        case TPlus:
        case TMinus:
        case TStar:
        case TSlash:
        case TAmp:
        case TPipe:
        case TCaret:
        case TMod: return actOnArithmetic(lhs, opToken.type, rhs);

        // Comparison:
        case TLAngle:
        case TLEq:
        case TRAngle:
        case TREq: return actOnComparison(lhs, opToken, rhs);

        // Shifts:
        case TLAngleLAngle:
        case TRAngleRAngle: return actOnShiftOperator(lhs, opToken, rhs);

        // Equivalence:
        case TEqEq:
        case TBangEq: return actOnEqTest(lhs, opToken, rhs);

        // Logical:
        case TAmpAmp:
        case TPipePipe: return actOnLogicalOperator(lhs, opToken, rhs);

        // Assignment:
        case TEq:
        case TSlashEq:
        case TPlusEq:
        case TMinusEq:
        case TStarEq:
        case TAmpEq:
        case TPipeEq:
        case TCaretEq: return actOnAssignment(lhs, opToken, rhs);

        default: unreachable("Unrecognized binary op during type checking");
    }
}
