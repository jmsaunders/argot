#include "common.h"

#include "scope.h"
#include "typecheck_private.h"
#include <functional>

Ref<Type> boolTy = Type::getBool();
Ref<Type> intTy = Type::getI32();
Ref<Type> voidTy = Type::getVoid();

// Lifetime:

bool Ast::exprBeginsLifetime(Expr* e) {
    using namespace Ast;
    return dynamic_cast<ConstructorExpr*>(e) || dynamic_cast<ImplicitCopyExpr*>(e) ||
           dynamic_cast<RecordInitExpr*>(e) || dynamic_cast<VariantInitExpr*>(e) ||
           dynamic_cast<MemberCallExpr*>(e) || dynamic_cast<CallExpr*>(e) ||
           dynamic_cast<InterfaceDispatchExpr*>(e) || dynamic_cast<ArrayRepeatExpr*>(e);
}

bool exprBeginsLifetime(RefA<Ast::Expr> e) {
    return Ast::exprBeginsLifetime(e.get());
}

//
// If used as the rhs of an assignment or passed as an argument, we need
// to duplicate what our expression is sometimes.
//

Ref<TExpr> performCopyExprIfNeeded(RefA<TExpr> e) {
    if (!exprBeginsLifetime(e)) {
        auto ty = e->type();
        if (!ty->copyable())
            error(e->loc, "cannot copy object of type %s", e->type()->name.c_str());

        if (ty->nontrivialCopy())
            return mkref<T::ImplicitCopyExpr>(e);
    }

    return e;
}

Ref<TraitInstanceInfo> lookupTraitInstance(RefA<Type> type, StrA name) {
    Vec<Ref<TraitInstanceInfo>>* traits;
    if (auto gi = type->genericInstance()) {
        traits = &gi->parent()->traits;
    } else {
        traits = &type->traits;
    }

    for (const auto& traitDesc : *traits) {
        if (traitDesc->trait->name == name) {
            if (auto gi = type->genericInstance()) {
                Str msg;
                SubstitutionMap sm;
                if (auto maybeSm = inferTraitImplTemplates(traitDesc, gi, &msg)) {
                    sm = *maybeSm;
                } else {
                    // TODO: We can communicate information about why failed to infer here.
                    return nullptr;
                }

                // If we're a generic trait then we need to perform the instance's substitution map
                // on our trait information. Do this to a copy:
                auto ret = mkref<TraitInstanceInfo>(*traitDesc);
                for (auto& type : ret->types)
                    type.second = replaceTypesFromMap(type.second, sm);
                for (auto& m : ret->methods)
                    m = replaceTypesFromMap(m, sm);
                return ret;
            }

            return traitDesc;
        }
    }
    return nullptr;
}

Ref<T::Stmt> actOnForStmt(RefA<P::ForStmt> f, RefA<TcScope> scope) {
    auto from = checkExpr(f->from, scope);

    // This should return a mapped Trait type representing the template arguments processed
    // depending on the instantiated type in question

    auto tii = lookupTraitInstance(from->type(), "Iterable");
    if (!tii)
        error(from, "expr of type %s not iterable", from->type()->name.c_str());

    assert(tii->types.size() == 1);
    auto iter = tii->types.begin()->second;

    auto iterTii = lookupTraitInstance(iter, "Iterator");
    assert(iterTii && "iterator does not yield an iterable");

    assert(iterTii->types.size() == 1);
    auto yieldedType = iterTii->types.begin()->second;

    auto bodyScope = mkref<TcScope>(scope);
    auto name = f->bind.text();
    bodyScope->symbols[name] = mkref<UnwrappedSymbol>(name, yieldedType);

    auto block = checkBlock(f->body, bodyScope);

    if (block.expr)
        error(block.expr, "for loops cannot yield");

    return mkref<T::ForStmt>(f->bind, from, block);
}

// Statement type checker:
Ref<T::Stmt> checkStmt(RefA<P::Stmt> s, RefA<TcScope> scope) {
    using namespace Ast;

    if (scope->hasReturned)
        error(s->loc, "statement after returning");

    if (auto let = dyn_cast<P::LetStmt>(s)) {
        const auto& pinit = let->init;
        auto init = checkExpr(pinit, scope);

        if (init->type()->isVoid())
            error(pinit->loc, "variables cannot have void type");

        init = performCopyExprIfNeeded(init);

        // if we have an explicit type specified, unify against it:
        if (let->type) {
            auto type = resolve(scope, let->type);
            unifyTypes(init->loc, init->type(), type);
        }

        auto nameToken = let->nameToken;
        auto name = nameToken.text();
        auto ret = mkref<LetStmt>(mkref<VarDecl>(name, init), nameToken.loc());

        if (lookup(scope->symbols, name))
            error(nameToken, "name collision");
        else
            scope->symbols[name] = mkref<LetVariableSymbol>(ret);

        return ret;
    } else if (auto whileStmt = dyn_cast<P::WhileStmt>(s)) {
        auto condition = checkExpr(whileStmt->condition, scope);

        auto bodyScope = mkref<TcScope>(scope);
        auto block = checkBlock(whileStmt->body, bodyScope);

        auto coercedCond = attemptCoerceExpression(condition, Type::getBool());
        if (!coercedCond)
            error(condition, "while loop condition must be bool-coercible");

        if (block.expr)
            error(block.expr, "while loops cannot yield values");

        return mkref<WhileStmt>(s->loc, condition, block);
    } else if (auto exprStmt = dyn_cast<P::ExprStmt>(s)) {
        return mkref<T::ExprStmt>(checkExpr(exprStmt->expr, scope));
    } else if (auto ret = dyn_cast<P::ReturnStmt>(s)) {
        auto func = scope->findFunction();
        assert(func);

        Ref<TExpr> e;
        if (ret->val)
            e = checkExpr(ret->val, scope);

        scope->hasReturned = true;

        if (e) {
            auto coercedReturn = attemptCoerceExpression(e, func->returnType);
            if (!coercedReturn)
                error(e->loc, "type mismatch on return (got '%s' expected '%s')",
                      e->type()->name.c_str(), func->returnType->name.c_str());

            coercedReturn = performCopyExprIfNeeded(coercedReturn);

            return mkref<T::ReturnStmt>(coercedReturn, s->loc);
        } else {
            if (func->returnType != Type::getVoid())
                error(s->loc, "must return value from non-void function");

            return mkref<ReturnStmt>(nullptr, s->loc);
        }
    } else if (auto fors = dyn_cast<P::ForStmt>(s)) {
        return actOnForStmt(fors, scope);
    }

    unreachable("");
}

T::Block checkBlock(const P::Block& block, RefA<TcScope> scope) {
    auto blockScope = mkref<TcScope>(scope);

    Vec<Ref<T::Stmt>> stmts;
    for (const auto& stmt : block.stmts)
        stmts.push_back(checkStmt(stmt, blockScope));

    Ref<T::Expr> expr;
    if (block.expr)
        expr = checkExpr(block.expr, blockScope);

    return { stmts, expr, block.openingBrace, block.closingBrace };
}

Ref<Type> resolve(const TcScope* scope, RefA<P::TypeRef> tr) {
    if (auto simple = dyn_cast<P::SimpleTypeRef>(tr)) {
        if (simple->id.text() == "Self") {
            auto selfScope = scope->findSelf();
            if (!selfScope)
                error(tr->tok, "cannot use Self in this context");
            return selfScope->selfType;
        }

        if (auto ty = scope->lookupType(simple->id.text()))
            return ty;

        error(tr->tok, "does not name a type");
    } else if (auto ptr = dyn_cast<P::PointerTypeRef>(tr)) {
        // Pointer types are special. They don't need to be fully resolved:
        return PointerType::of(resolve(scope, ptr->interior));
    } else if (auto inst = dyn_cast<P::TemplateInstanceRef>(tr)) {
        auto generic = scope->lookupGeneric(inst->name.text());
        if (!generic)
            error(tr->tok, "does not name a generic");

        Vec<Ref<Type>> args;
        for (const auto& t : inst->args)
            args.push_back(resolve(scope, t));

        auto ret = generic->instantiate(args);
        if (ret)
            return ret;

        error(inst->name, "invalid template instance");
    } else if (auto f = dyn_cast<P::FunctionTypeRef>(tr)) {
        Ref<Type> returnType = Type::getVoid();
        Vec<Ref<Type>> argTypes;

        if (f->returnType)
            returnType = resolve(scope, f->returnType);
        for (const auto& a : f->argTypes)
            argTypes.push_back(resolve(scope, a));

        return mkref<FunctionType>(returnType, argTypes);
    } else if (auto a = dyn_cast<P::ArrayTypeRef>(tr)) {
        auto inner = resolve(scope, a->interior);
        return mkref<ArrayType>(inner, a->length);
    }

    error(tr->tok, "unknown type");
}

// TODO need to generalize this to work for variadic calls too:
ArgumentList mapArgumentList(const Ref<TcScope>& scope, const P::ArgumentList& args) {
    ArgumentList ret;
    for (const auto& arg : args)
        ret.push_back({ arg.name, resolve(scope, arg.type), arg.flags });
    return ret;
}

static Ref<Interface> resolveTraitInternal(RefA<TcScope> scope, RefA<P::TypeRef> ref) {
    Ref<Interface> ret;

    if (auto s = dyn_cast<P::SimpleTypeRef>(ref)) {
        return scope->lookupTrait(s->id.text());
    } else if (auto tg = dyn_cast<P::TemplateInstanceRef>(ref)) {
        if (auto gen = scope->lookupTraitGeneric(tg->name.text())) {
            Vec<Ref<Type>> args;
            for (const auto& arg : tg->args) {
                auto typed = resolve(scope, arg);
                if (!typed)
                    return nullptr;

                args.push_back(typed);
            }

            if (auto instance = gen->instantiate(args))
                return instance;

            error(ref->tok, "invalid interface instance");
        }
    } else if (auto p = dyn_cast<P::PointerTypeRef>(ref)) {
        error(ref->tok, "traits can't be pointers");
    }

    return nullptr;
}

Ref<Interface> resolveTrait(RefA<TcScope> scope, RefA<P::TypeRef> ref) {
    if (auto ret = resolveTraitInternal(scope, ref))
        return ret;
    error(ref->tok, "unknown trait");
}

// Note that this function populates the types it creates inside the scope so that we can
// use earlier template names in later template constraints!

Vec<Ref<TemplateArgType>> mapTemplateArgList(RefA<TcScope> scope,
                                             VecA<Ref<P::TemplateArg>> templates) {
    Vec<Ref<TemplateArgType>> ret;
    for (const auto& t : templates) {
        auto nt = mkref<TemplateArgType>(t->name);
        scope->registerType(nt);

        if (t->constraints.size() > 0) {
            auto parseConstraint = t->constraints.front();
            if (t->constraints.size() != 1)
                error(parseConstraint->tok,
                      "type checker can't handle more than one constraint right now");

            nt->constraint = resolveTrait(scope, parseConstraint)->specialize(nt);
        }

        ret.push_back(nt);
    }
    return ret;
}

bool isSpecialVariadicTrait(RefA<Interface> t) {
    return is<ConstructorArgTrait>(t);
}

Ref<FunctionSignature> mapFunctionSignature(const Ref<TcScope>& scope, RefA<P::FunctionSignature> sig) {
    // Create a template scope to type check the args and return types.
    // For the real type checking inside the function, the FunctionScope brings
    // these template arguments into scope.

    auto templateScope = mkref<TcScope>(scope.get());
    auto templates = mapTemplateArgList(templateScope, sig->templates);

    ArgumentList args;
    Maybe<VariadicInfo> variadicConstraint;
    for (const auto& [i, arg] : enumerate(sig->params)) {
        auto ty = resolve(templateScope, arg.type);

        if (auto tv = ty->templateVariable()) {
            if (isSpecialVariadicTrait(tv->constraint)) {
                VariadicInfo info = { arg.name, tv, tv->constraint };
                variadicConstraint = info;
            }
        }

        if (variadicConstraint) {
            if (i != sig->params.size() - 1)
                error(arg.type->tok, "variadic arguments must be last");
        } else {
            args.push_back({ arg.name, ty, arg.flags });
        }
    }

    auto retType = Type::getVoid();
    if (sig->returnType)
        retType = resolve(templateScope, sig->returnType);

    auto ret = mkref<FunctionSignature>(sig->nameToken, args, retType, templates);
    ret->variadicConstraint = variadicConstraint;
    return ret;
}

static bool traitTypesEqual(RefA<Type> self, RefA<Type> lhs, RefA<Type> rhs) {
    if (is<TraitSelfType>(lhs))
        return self->equals(rhs);
    if (is<TraitSelfType>(rhs))
        return self->equals(lhs);

    return lhs->equals(rhs);
}

static bool traitSignaturesMatch(RefA<Type> self, RefA<FunctionSignature> traitMethod,
                                 RefA<FunctionSignature> rhs) {
    assert(traitMethod->templates.empty());
    assert(rhs->templates.empty());

    if (traitMethod->name() != rhs->name())
        return false;
    if (!traitTypesEqual(self, traitMethod->returnType, rhs->returnType))
        return false;
    if (traitMethod->params.size() != rhs->params.size())
        return false;

    for (size_t i = 0; i < traitMethod->params.size(); ++i) {
        if (traitMethod->params[i].flags != rhs->params[i].flags)
            return false;

        if (!traitTypesEqual(self, traitMethod->params[i].type, rhs->params[i].type))
            return false;
    }

    return true;
}

struct Typechecker {
    Ref<P::ModuleImplementation> mi;
    Ref<TcScope> importScope = mkref<TcScope>();
    Ref<TcScope> scope = mkref<TcScope>(importScope);

    Ref<T::ModuleImplementation> impl;
    Vec<Ref<TcScope>> deps;
    Typechecker(RefA<P::ModuleImplementation> mi, VecA<Ref<TcScope>> deps)
        : mi(mi), impl(mkref<T::ModuleImplementation>(mi->path)), deps(deps) {
    }

    void fillVariantBody(RefA<TcScope> variantScope, RefA<P::VariantDefinition> def,
                         RefA<VariantBase> typed) {
        for (auto [i, field] : enumerate(def->fields)) {
            auto name = field.nameToken.text();

            auto fieldType = field.type ? resolve(variantScope, field.type) : nullptr;
            typed->fields.push_back({ field.nameToken, fieldType });

            // Do scope registration:
            if (scope->lookupSymbol(name))
                error(field.nameToken, "name collision");
            scope->symbols[name] = mkref<VariantSymbol>(typed, i);
        }

        // Make sure to mark our completion if we're a generic:
        if (auto g = dyn_cast<Generic>(typed))
            g->markComplete();
    }

    //
    // This method fills out the RecordBase for all record types except generic instances (which are
    // filled out in type.cpp) - that could should probably be merged with this in some way..
    // Possibly by having separate versions of the RecordBase struct - one for external-facing for
    // type-checking and the other for code generation?
    //

    void fillRecordBody(RefA<TcScope> parentScope, RefA<P::RecordDefinition> rec,
                        RefA<RecordBase> newType) {
        Ref<Type> selfType = dyn_cast<RecordType>(newType);
        if (!selfType) {
            // If we're a template type then our Self type is the generic
            // instantiated with the templates:
            auto gen = dyn_cast<RecordGeneric>(newType);
            selfType = gen->trivialInstance();
        }

        auto scope = mkref<SelfScope>(parentScope, selfType);

        for (const auto& base : rec->bases) {
            auto resolved = resolve(scope, base);
            if (auto rt = resolved->recordType())
                newType->bases.push_back(rt);
            else
                error(base->tok, "cannot inherit from non-record type");
        }

        for (auto [index, field] : enumerate(rec->fields)) {
            // Make sure we don't have two fields with the same name:
            for (const auto& f : newType->fields) {
                if (field.nameToken.text() == f.name)
                    error(field.nameToken, "field with this name already exists");
            }

            auto fieldName = field.nameToken.text();
            auto fieldType = resolve(scope, field.type);

            newType->fields.push_back({ fieldName, fieldType });
            auto offset = rec->bases.size() + index;
            newType->symbols[fieldName] = mkref<MemberVarSymbol>(fieldName, offset, fieldType);
        }

        for (const auto& c : rec->constructors) {
            auto args = mapArgumentList(scope, c.args);

            // Make sure this constructor doesn't already exist:
            for (const auto& c : newType->constructors) {
                if (argumentListIdentical(args, c.params))
                    error(c.selfToken, "constructor with these arguments already exists");
            }

            // Default constructor:
            if (args.size() == 0)
                newType->defaultConstructorIndex = newType->constructors.size();

            // Copy constructor:
            if (args.size() == 1) {
                if (args[0].type->equals(selfType))
                    newType->copyConstructorIndex = newType->constructors.size();
            }

            newType->constructors.push_back({ c.selfToken, args });
        }

        for (const auto& method : rec->methods) {
            auto sig = mapFunctionSignature(scope, method.signature);

            // We'll need to cache the signature here because we're doing it
            // twice. The other time we mapFunctionSignature for this method
            // will be in the analyzeRecordBody call.

            if (!sig->templates.empty())
                error(sig->nameToken, "templated methods not yet supported");

            auto ms = mkref<MethodSignature>(selfType, *sig);
            ms->isVirtual = method.signature->isVirtual;

            newType->methods.push_back(ms);
            registerCallSymbol(newType->symbols, wrapShare(new MethodCallSymbol(selfType, ms, {})));
        }

        // Process base classes:
        Vec<size_t> currentPath;
        auto processBaseClass = [&](size_t index, RefA<RecordBaseType> base) {
            currentPath.push_back(index);

            for (auto [index, field] : enumerate(base->fields)) {
                auto offset = base->bases.size() + index;
                newType->symbols[field.name] =
                    mkref<MemberVarSymbol>(field.name, offset, field.type, currentPath);
            }

            for (auto ms : base->methods) {
                auto mcs = mkref<MethodCallSymbol>(base, ms, currentPath);
                registerCallSymbol(newType->symbols, mcs);
            }

            currentPath.pop_back();
        };

        for (auto [index, base] : enumerate(newType->bases))
            processBaseClass(index, base);

        if (rec->destructor)
            newType->explicitDestructorDecl = { rec->destructor->selfToken };

        // Make sure to mark our completion if we're a generic:
        if (auto g = dyn_cast<Generic>(newType))
            g->markComplete();
    }

    //
    // Typecheck record body and add the implementation details for this record
    // into the impl for this module:
    //

    void analyzeRecordBody(RefA<P::RecordDefinition> rec) {
        auto recImpl = mkref<Ast::RecordImpl>();
        recImpl->closingBrace = rec->closingBrace;

        auto sym = scope->lookupSymbol(rec->name.text());

        Ref<RecordBaseType> recordType;
        if (auto ts = dyn_cast<TypeSymbol>(sym)) {
            auto ty = dyn_cast<RecordType>(ts->type);
            impl->typeImpls[ty] = recImpl;

            recordType = ty;
        } else if (auto gs = dyn_cast<GenericSymbol>(sym)) {
            auto generic = dyn_cast<RecordGeneric>(gs->generic);
            impl->genericImpls[generic] = recImpl;

            recordType = generic->trivialInstance()->recordType();
        } else {
            unreachable("");
        }

        auto selfScope = mkref<SelfImplScope>(scope, recordType);

        for (const auto& c : rec->constructors)
            recImpl->constructors.push_back(makeConstructorDefinition(selfScope, c, recordType));

        for (const auto& method : rec->methods) {
            auto sig = mapFunctionSignature(selfScope, method.signature);

            auto fs = mkref<FunctionScope>(selfScope, sig, true);
            auto b = checkBlock(method.body, fs);
            fs->validate(b);

            recImpl->methods.push_back(mkref<T::FunctionDefinition>(sig, b));
        }

        if (rec->destructor) {
            auto fs = wrapShare(new FunctionScope(selfScope, voidTy, {}, {}, true));
            auto b = checkBlock(rec->destructor->block, fs);
            recImpl->destructor = { rec->destructor->selfToken, b };
        }
    }

    void populateTypeInformation();

    Ref<T::ConstructorDefinition> makeConstructorDefinition(RefA<TcScope> selfScope,
                                                            const P::ConstructorDefinition& c,
                                                            RefA<RecordBaseType> recordType);

    void typecheckInterfaceImpl(const P::TraitImpl& ti);

    void run() {
        //
        // PHASE 1:
        // Populate the type, generic, and symbol tables with the information to
        // do type checking:
        //

        for (const auto& dep : deps)
            importScope->importFrom(dep);

        // Populate scope with everything needed to type check:
        populateTypeInformation();

        //
        // @HACK:
        //
        // Right now just cache the function signatures we get so we have the
        // same argument list on the definition and the scope symbol.
        //
        // This is necessary for template definitions, since the substitution
        // map checks based on the pointer value of the template argument, which
        // will be different here if we mapFunctionSignature() twice. We could
        // use an approach like the record population system, where we cache a
        // function pointer to complete the definition. Not worth the effort
        // right now though.
        //

        Map<Ref<P::FunctionDefinition>, Ref<FunctionSignature>> globalSignatures;

        for (const auto& fn : mi->functions) {
            auto sig = mapFunctionSignature(scope, fn->signature);

            assert(!globalSignatures[fn]);
            globalSignatures[fn] = sig;

            scope->registerCallSymbol(mkref<GlobalCallSymbol>(sig));
        }

        for (const auto& fn : mi->externs) {
            auto sig = mapFunctionSignature(scope, fn);
            scope->registerCallSymbol(mkref<GlobalCallSymbol>(sig));

            impl->externs.push_back(sig);
        }

        //
        // PHASE 2:
        // Do type checking on function bodies:
        //

        for (const auto& ti : mi->interfaceImpls)
            typecheckInterfaceImpl(ti);

        // Do semantic analysis on function bodies:
        for (const auto& t : mi->types) {
            if (auto rec = dyn_cast<P::RecordDefinition>(t))
                analyzeRecordBody(rec);
        }

        for (auto fn : mi->functions) {
            auto sig = globalSignatures[fn];
            auto fs = mkref<FunctionScope>(scope, sig, false);
            auto b = checkBlock(fn->body, fs);
            fs->validate(b);

            auto def = mkref<Ast::FunctionDefinition>(sig, b);
            if (sig->isTemplated())
                impl->genericFunctions[fn->signature->name()] = def;
            else
                impl->functions.push_back(def);
        }
    }
};

Ref<T::ConstructorDefinition>
Typechecker::makeConstructorDefinition(RefA<TcScope> selfScope, const P::ConstructorDefinition& c,
                                       RefA<RecordBaseType> recordType) {
    auto args = mapArgumentList(selfScope, c.args);
    auto fs = wrapShare(new FunctionScope(selfScope, voidTy, args, {}, true));

    Map<Str, Pair<Vec<Ref<TExpr>>, Token>> inits;
    for (const auto& init : c.inits) {
        auto fieldName = init.name.text();
        if (lookup(inits, fieldName))
            error(init.name, "this field initializer already specified");

        Vec<Ref<TExpr>> args;
        for (const auto& arg : init.args)
            args.push_back(checkExpr(arg, fs));

        inits.insert(make_pair(fieldName, make_pair(args, init.name)));
    }

    Map<size_t, Ref<TExpr>> fieldInits;
    for (size_t i = 0; i < recordType->fields.size(); ++i) {
        const auto& field = recordType->fields[i];
        auto maybeInitForThisField = lookup(inits, field.name);

        if (!maybeInitForThisField) {
            // Make sure types without default construction are specified
            // explicitly:
            if (!field.type->defaultConstructible()) {
                error(c.selfToken, "no initialize provided for field %s", field.name.c_str());
            } else {
                // @TODO
                // Possibly eventually generalize to non-record types (zero-init
                // here?):

                if (auto fieldRecord = field.type->recordType()) {
                    if (fieldRecord->defaultConstructorIndex) {
                        auto cidx = *fieldRecord->defaultConstructorIndex;
                        fieldInits[i] = wrapShare(
                            new T::ConstructorExpr(c.selfToken, cidx, fieldRecord, {}, {}));
                    }
                }
            }

            continue;
        }

        auto [initList, fieldToken] = *maybeInitForThisField;
        inits.erase(field.name);

        if (auto fieldRecord = field.type->recordType()) {
            // We're a record type so we're expecting a constructor call:
            if (auto call = generateConstructorCall(fieldToken, fieldRecord, initList, true)) {
                fieldInits[i] = call;
            }
        }

        if (!fieldInits[i]) {
            // This is a field init, expecting just a single value of the
            // correct type:
            if (initList.size() != 1)
                error(fieldToken, "non-constructor initializer cannot have "
                                  "multiple arguments");

            auto init = initList[0];
            unifyTypes(fieldToken.loc(), field.type, init->type());
            fieldInits[i] = init;
        }

        if (!fieldInits[i])
            error(fieldToken, "no constructor call matching type signature");
    }

    if (!inits.empty()) {
        const auto& [fieldName, fieldInfo] = *inits.begin();
        error(fieldInfo.second, "unknown field %s", fieldName.c_str());
    }

    auto b = checkBlock(c.block, fs);
    fs->validate(b);

    return mkref<T::ConstructorDefinition>(c.selfToken, args, b, fieldInits);
}

void Typechecker::typecheckInterfaceImpl(const P::TraitImpl& ti) {
    auto templateScope = mkref<TcScope>(scope);
    auto templates = mapTemplateArgList(templateScope, ti.templates);
    Ref<Type> theType = resolve(templateScope, ti.theType);

    auto theTrait = resolveTrait(templateScope, ti.theTrait);

    auto traitImpl = mkref<T::TraitImpl>(theTrait);
    auto instanceInfo = mkref<TraitInstanceInfo>(theTrait);
    instanceInfo->type = theType;

    if (auto gi = theType->genericInstance()) {
        auto g = gi->parent();
        g->traits.push_back(instanceInfo);
        instanceInfo->templates = templates;

        impl->genericInterfaceImpls[g].push_back(traitImpl);
    } else {
        impl->interfaceImpls[theType].push_back(traitImpl);
        theType->traits.push_back(instanceInfo);
    }

    traitImpl->methods.resize(theTrait->methods.size());
    instanceInfo->methods.resize(theTrait->methods.size());

    auto selfScope = mkref<SelfImplScope>(templateScope, theType, SSF_NoTemplates);
    SubstitutionMap& subMap = instanceInfo->types;

    for (const auto& [name, ref] : ti.typeMap) {
        auto type = resolve(selfScope, ref);
        selfScope->symbols[name] = mkref<TypeSymbol>(type);

        if (auto internalType = lookup(theTrait->types, name))
            subMap[*internalType] = type;
    }

    for (const auto& [requiredName, requiredType] : theTrait->types) {
        if (!lookup(subMap, requiredType))
            error(ti.theTrait->tok, "required type %s not provided", requiredName.c_str());
    }

    for (const auto& method : ti.methods) {
        auto sig = replaceTypesFromMap(mapFunctionSignature(selfScope, method->signature), subMap);

        // Find where we'll end up putting this function defintion inside the
        // trait implementation, since order matters:

        Maybe<size_t> index;
        for (const auto& [i, traitMethod] : enumerate(theTrait->methods)) {
            auto m = replaceTypesFromMap(traitMethod, subMap);
            if (traitSignaturesMatch(theType, m, sig)) {
                index = i;
                break;
            }
        }

        if (!index)
            error(sig->nameToken, "not a trait method");

        auto fs = mkref<FunctionScope>(selfScope, sig, true);
        auto body = checkBlock(method->body, fs);
        fs->validate(body);

        auto def = mkref<T::FunctionDefinition>(sig, body);
        traitImpl->methods[*index] = def;
        instanceInfo->methods[*index] = sig;
    }

    // Make sure all methods are provided:
    for (size_t i = 0; i < traitImpl->methods.size(); ++i) {
        if (!traitImpl->methods[i])
            error(ti.theTrait->tok, "no implementation provided for %s",
                  theTrait->methods[i]->name().c_str());
    }
}

template <typename... Args>
[[noreturn]] void cycleError(const char* fmt, Args... args) {
    printf("\033[1;31mCycle error:\033[0m ");

    if (fmt)
        printf(fmt, args...);

    printf("\n");
    exit(-1);
}

static void checkForCyclicTypes(RefA<TcScope> scope) {
    // @TODO:
    // This isn't entirely accurate here since we're hashing by string.
    // X<T> could be any number of types depending on the template instance's scope.
    //
    // We could resolve template argument types to their originating scope and attempt to make
    // them unique. This should catch all the degenerate cases and just give false positives
    // though. Another approach is canonicalizing template instances and doing direct pointer
    // comparison.
    //

    Set<Str> currentlyVisiting;
    Set<Str> visited;

    std::function<void(RefA<RecordBase>)> visitRecord;
    std::function<void(RefA<VariantBase>)> visitVariant;

    auto visitType = [&](RefA<Type> type) {
        if (lookup(visited, type->name))
            return;

        if (lookup(currentlyVisiting, type->name))
            cycleError("%s", type->name.c_str());

        currentlyVisiting.insert(type->name);

        if (auto rec = type->recordType())
            visitRecord(rec);
        if (auto var = type->variantType())
            visitVariant(var);

        currentlyVisiting.erase(type->name);
        visited.insert(type->name);
    };

    visitRecord = [&](RefA<RecordBase> rec) {
        for (const auto& field : rec->fields)
            visitType(field.type);
        for (const auto& base : rec->bases)
            visitType(base);
    };

    visitVariant = [&](RefA<VariantBase> rec) {
        for (const auto& field : rec->fields) {
            if (field.type)
                visitType(field.type);
        }
    };

    // Maybe find a better way to iterate over these?
    for (const auto& [name, sym] : scope->symbols) {
        if (auto ts = dyn_cast<TypeSymbol>(sym))
            visitType(ts->type);
        if (auto gs = dyn_cast<GenericSymbol>(sym))
            visitType(gs->generic->trivialInstance());
    }
}

void Typechecker::populateTypeInformation() {
    //
    // This code works in two passes. First it populates empty definitions
    // for all traits, types and generics. After this, it goes back and
    // cross-references everything with those definitions.
    //
    // We can still have circular references involved, so those need to be
    // checked after the fact.
    //

    Vec<std::function<void()>> deferredChecks;

    for (const auto& trait : mi->interfaces) {
        auto name = trait.name.text();
        if (lookup(scope->symbols, name))
            error(trait.name, "name conflict");

        auto us = Interface::make(name);

        deferredChecks.push_back([=]() {
            auto selfScope = mkref<SelfScope>(scope, us->traitSelf);

            for (const auto& [name, constraint] : trait.types) {
                auto ty = mkref<TraitInternalType>(name, us);
                us->types[name] = ty;
                selfScope->registerType(ty);
            }

            for (const auto& method : trait.methods)
                us->methods.push_back(mapFunctionSignature(selfScope, method));
        });

        scope->registerInterface(us);
    }

    for (const auto& type : mi->types) {
        auto name = type->name.text();
        if (lookup(scope->symbols, name))
            error(type->name, "name conflict");

        if (type->templates.size() > 0) {
            Ref<Generic> g;

            auto templateScope = mkref<TcScope>(scope);

            // Make the typed argument list:
            auto templates = mapTemplateArgList(templateScope, type->templates);

            if (const auto& rec = dyn_cast<P::RecordDefinition>(type)) {
                auto rk = mkref<RecordGeneric>(type->name, templates);
                deferredChecks.push_back([=]() { fillRecordBody(templateScope, rec, rk); });
                g = rk;
            } else if (const auto& var = dyn_cast<P::VariantDefinition>(type)) {
                auto vk = mkref<VariantGeneric>(type->name, templates);
                deferredChecks.push_back([=]() { fillVariantBody(templateScope, var, vk); });
                g = vk;
            }

            scope->registerGeneric(g);
        } else {
            Ref<Type> ty;
            if (const auto& rec = dyn_cast<P::RecordDefinition>(type)) {
                auto rt = mkref<RecordType>(type->name);
                deferredChecks.push_back([=]() { fillRecordBody(scope, rec, rt); });
                ty = rt;
            } else if (const auto& var = dyn_cast<P::VariantDefinition>(type)) {
                auto vt = mkref<VariantType>(type->name);
                deferredChecks.push_back([=]() { fillVariantBody(scope, var, vt); });
                ty = vt;
            }

            scope->registerType(ty);
        }
    }

    for (const auto& tc : deferredChecks)
        tc();

    checkForCyclicTypes(scope);
}

Pair<Ref<T::ModuleImplementation>, Ref<TcScope>> typecheck(RefA<P::ModuleImplementation> mi,
                                                           VecA<Ref<TcScope>> deps) {
    Typechecker tc(mi, deps);
    tc.run();
    return make_pair(tc.impl, tc.scope);
}
