#pragma once

#include "ast.h"
#include "parsetree.h"
#include "type.h"
#include "trait.h"

extern Ref<Type> boolTy, intTy, voidTy;

namespace P = Parsetree;
namespace T = Ast;

using PExpr = P::Expr;
using TExpr = T::Expr;

// Error handling:

[[noreturn]] inline void error(SourceLoc loc, const char* fmt, va_list va) {
    auto lineText = lexedFiles[loc.file]->getErrorLineText(loc.offset);
    printf("\033[1;31mSemantic error:\033[0m %s(%u,%u):\n%s\n", loc.file, loc.line, loc.col,
           lineText.c_str());

    Str underline(std::max(int(loc.col - 1), 0), ' ');
    underline.push_back('^');
    printf("%s\n", underline.c_str());

    if (fmt)
        vprintf(fmt, va);

    printf("\n");
    exit(-1);
}

[[noreturn]] inline FORMAT_ATTRIBUTE(2, 3) void error(SourceLoc loc, const char* fmt, ...) {
    va_list va;
    va_start(va, fmt);
    error(loc, fmt, va);
}

[[noreturn]] inline FORMAT_ATTRIBUTE(2, 3) void error(Token tok, const char* fmt, ...) {
    va_list va;
    va_start(va, fmt);
    error(tok.loc(), fmt, va);
}

[[noreturn]] inline FORMAT_ATTRIBUTE(2, 3) void error(Ref<TExpr> e, const char* fmt, ...) {
    va_list va;
    va_start(va, fmt);
    error(e->loc, fmt, va);
}

[[noreturn]] inline FORMAT_ATTRIBUTE(2, 3) void error(PExpr* e, const char* fmt, ...) {
    va_list va;
    va_start(va, fmt);
    error(e->loc, fmt, va);
}

template <typename... Args>
void warnAtLocation(SourceLoc loc, const char* fmt, Args... args) {
    printf("Warning %s(%u,%u):\n", loc.file, loc.line, loc.col);

    if (fmt)
        printf(fmt, args...);

    printf("\n");
}

//
// Typechecker symbol types:
//

// A variant field:
struct VariantSymbol : Symbol {
    Ref<VariantBase> variant;
    VariantField field;
    size_t fieldIndex;

    VariantSymbol(RefA<VariantBase> variant, size_t fieldIndex)
        : variant(variant), field(variant->fields[fieldIndex]), fieldIndex(fieldIndex) {}
};

struct CallSymbol : Symbol {
    Ref<FunctionSignature> sig;

protected:
    CallSymbol(RefA<FunctionSignature> sig) : sig(sig) {}
};

// A regular function call:
struct GlobalCallSymbol : CallSymbol {
    GlobalCallSymbol(RefA<FunctionSignature> sig) : CallSymbol(sig) {}
};

// A local method call inside a record scope:
struct MethodCallSymbol : CallSymbol {
    Ref<Type> recordType;
    Ref<MethodSignature> msig;
    Vec<size_t> path;

    MethodCallSymbol(RefA<Type> recordType, RefA<MethodSignature> sig, VecA<size_t> path)
        : CallSymbol(sig), recordType(recordType), msig(sig), path(path) {
    }
};

// Special typesque symbols:

struct TypeSymbol : Symbol {
    Ref<Type> type;
    TypeSymbol(RefA<Type> type) : type(type) {
    }
};

struct GenericSymbol : Symbol {
    Ref<Generic> generic;
    GenericSymbol(RefA<Generic> generic) : generic(generic) {
    }
};

struct InterfaceSymbol : Symbol {
    Ref<Interface> iface;
    InterfaceSymbol(RefA<Interface> iface) : iface(iface) {
    }
};

struct GenericInterfaceSymbol : Symbol {
    Ref<InterfaceGeneric> tg;
    GenericInterfaceSymbol(RefA<InterfaceGeneric> tg) : tg(tg) {
    }
};

//
// This is a pack of function symbols, not really a symbol in the traditional
// way, but a way of packing a bunch of functions into a single symbol so we
// can represent all the overloads as a single symbol rather than a multimap
// for the symbols:
//

struct FunctionsOverloadSymbol : Symbol {
    Vec<Ref<CallSymbol>> functions;

    bool alreadyExists(const ArgumentList& args) const;
    Ref<Ast::Expr> createCallExpr(RefA<struct TcScope> scope, Token nameToken,
                                  Vec<Ref<Ast::Expr>> args, const Maybe<Vec<Ref<Type>>>& templates);
};

Ref<Ast::Expr> checkExpr(RefA<Parsetree::Expr> e, RefA<TcScope> scope);
T::Block checkBlock(const P::Block& block, RefA<TcScope> scope);
Ref<TExpr> performCopyExprIfNeeded(RefA<TExpr> e);
bool blockReturns(const T::Block& b);
bool argumentListIdentical(const ArgumentList& a, const ArgumentList& b);
bool isLvalue(RefA<TExpr> e);
Ref<TExpr> generateConstructorCall(Token nameToken, RefA<RecordBaseType> rec, VecA<Ref<TExpr>> args,
                                   bool nullOnFailure = false);
void unifyTypes(SourceLoc errorLoc, Ref<Type> lhs, Ref<Type> rhs);

Ref<Interface> resolveTrait(RefA<TcScope> scope, RefA<P::TypeRef> ref);
Ref<Type> resolve(const TcScope* scope, RefA<P::TypeRef> tr);
inline Ref<Type> resolve(const Ref<TcScope>& scope, RefA<P::TypeRef> tr) {
    return resolve(scope.get(), tr);
}

// Operator typechecking:
Ref<TExpr> attemptCoerceExpression(RefA<TExpr> e, RefA<Type> type);
Ref<TExpr> actOnInfixExpr(const Ref<TExpr>& lhs, Token opToken, const Ref<TExpr>& rhs);
Ref<TExpr> actOnUnaryOp(Token op, RefA<TExpr> expr);
