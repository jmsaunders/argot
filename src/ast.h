//
// ast.h
// Fully resolved/typed abstract syntax tree. Output of type checking phase.
//

#pragma once

#include "symbols.h"
#include "token.h"
#include "type.h"

namespace Ast {

struct Expr;
struct Stmt;

struct VarDecl {
    Str name;
    Ref<Expr> init;

    VarDecl(Str name, RefA<Expr> init) : name(name), init(init) {
    }
};

// Sequence of statements, possibly followed by an expression without a semicolon:
// { let a = 1; a }
struct Block {
    Vec<Ref<Stmt>> stmts;
    Ref<Expr> expr;

    Maybe<SourceLoc> openingBrace, closingBrace;
};

struct ExprVisitor {
    virtual void accept(struct Expr*) {
        unreachable("");
    }

#define DEFINE_VISITOR(TYPE)                                                                       \
    virtual void accept(struct TYPE* e) {                                                          \
        accept((Expr*)e);                                                                          \
    }

    // clang-format off
    DEFINE_VISITOR(IfExpr) DEFINE_VISITOR(UnaryExpr) DEFINE_VISITOR(InfixExpr) DEFINE_VISITOR(LogicalOpExpr)
    DEFINE_VISITOR(ShiftExpr) DEFINE_VISITOR(VarRefExpr) DEFINE_VISITOR(CallExpr) DEFINE_VISITOR(TemplateCallExpr)
    DEFINE_VISITOR(ConstructorExpr) DEFINE_VISITOR(MemberExpr) DEFINE_VISITOR(MemberCallExpr)  DEFINE_VISITOR(VariantInitExpr)
    DEFINE_VISITOR(RecordInitExpr) DEFINE_VISITOR(MatchExpr) DEFINE_VISITOR(AssignmentExpr) DEFINE_VISITOR(CastExpr)
    DEFINE_VISITOR(ArrayIndexExpr) DEFINE_VISITOR(SizeofExpr) DEFINE_VISITOR(SelfExpr) DEFINE_VISITOR(ImplicitCopyExpr)
    DEFINE_VISITOR(InterfaceDispatchExpr) DEFINE_VISITOR(StringLiteralExpr) DEFINE_VISITOR(IntLiteralExpr)
    DEFINE_VISITOR(FloatLiteralExpr) DEFINE_VISITOR(BoolLiteralExpr) DEFINE_VISITOR(EqTestExpr)
    DEFINE_VISITOR(ConstructIntoExpr) DEFINE_VISITOR(ConstructFromVariadicExpr) DEFINE_VISITOR(FunctionRefExpr)
    DEFINE_VISITOR(ArrayRepeatExpr)
    // clang-format on

#undef DEFINE_VISITOR
};

#define DEFINE_VISIT                                                                               \
    void visit(ExprVisitor* v) override {                                                          \
        v->accept(this);                                                                           \
    }

//
// Expressions:
//

struct Expr {
    SourceLoc loc;

    virtual ~Expr() {
    }
    virtual void visit(ExprVisitor* v) = 0;

    //
    // The reason behind the accessor and hiding the type field itself:
    //
    // Since we have type variables flying around the place I'm very worried
    // about using a type variable as if it were a real type, instead of deferring
    // to its bound type.
    //
    // To handle this, we always access an expression's type through the type() method
    // which will automatically "derefence" the type, resulting in a fully bound type
    // if one currently exists.
    //

    Ref<Type> type() const {
        return _type->resolve();
    }

  protected:
    Expr(RefA<Type> type, SourceLoc loc) : loc(loc), _type(type) {
    }
    Ref<Type> _type;
};

// A reference to a variable:
struct VarRefExpr : Expr {
    Ref<VariableSymbol> var;

    VarRefExpr(RefA<VariableSymbol> var, SourceLoc loc) : Expr(var->type, loc), var(var) {
    }

    DEFINE_VISIT
};

// A function value referred to without calling yet:
static Vec<Ref<Type>> extractArgumentTypes(const ArgumentList& args) {
    Vec<Ref<Type>> ret;
    for (const auto& a : args)
        ret.push_back(a.type);
    return ret;
}

struct FunctionRefExpr : Expr {
    Ref<FunctionSignature> sig;

    FunctionRefExpr(RefA<FunctionSignature> sig, SourceLoc loc)
        : Expr(mkref<FunctionType>(sig->returnType, extractArgumentTypes(sig->params)), loc),
          sig(sig) {
    }

    DEFINE_VISIT
};

// a == b
struct EqTestExpr : Expr {
    bool equiv; // == or !=
    Ref<Expr> lhs, rhs;

    EqTestExpr(RefA<Expr> lhs, bool equiv, RefA<Expr> rhs)
        : Expr(Type::getBool(), lhs->loc), equiv(equiv), lhs(lhs), rhs(rhs) {
    }

    DEFINE_VISIT
};

// a + b
struct InfixExpr : Expr {
    // TODO This operator should not come from the lexer's operator but be parsed into
    // something more semantically-aware.
    TokenType op;
    Ref<Expr> lhs, rhs;

    InfixExpr(RefA<Type> type, TokenType op, RefA<Expr> lhs, RefA<Expr> rhs)
        : Expr(type, lhs->loc), op(op), lhs(lhs), rhs(rhs) {
    }

    DEFINE_VISIT
};

// a || b
struct LogicalOpExpr : Expr {
    TokenType op;
    Ref<Expr> lhs, rhs;

    LogicalOpExpr(TokenType op, RefA<Expr> lhs, RefA<Expr> rhs)
        : Expr(Type::getBool(), lhs->loc), op(op), lhs(lhs), rhs(rhs) {
    }

    DEFINE_VISIT
};

// a << 1
enum class ShiftType { left, right };
struct ShiftExpr : Expr {
    ShiftType st;
    Ref<Expr> lhs, rhs;

    ShiftExpr(ShiftType st, RefA<Expr> lhs, RefA<Expr> rhs)
        : Expr(lhs->type(), lhs->loc), st(st), lhs(lhs), rhs(rhs) {
    }

    DEFINE_VISIT
};

// -a
struct UnaryExpr : Expr {
    // TODO This operator should not come from the lexer's operator but be parsed into
    // something more semantically-aware.
    TokenType op;
    Ref<Expr> e;

    UnaryExpr(RefA<Type> type, Token op, RefA<Expr> e) : Expr(type, op.loc()), op(op.type), e(e) {
    }

    DEFINE_VISIT
};

// f(1)
struct CallExpr : Expr {
    Ref<FunctionSignature> sig;
    SourceLoc callLoc;
    Vec<Ref<Expr>> args;

    CallExpr(RefA<FunctionSignature> sig, SourceLoc loc, const Vec<Ref<Expr>>& args)
        : Expr(sig->returnType, loc), sig(sig), callLoc(loc), args(args) {
    }

    DEFINE_VISIT
};

// f<int>(1)
struct TemplateCallExpr : Expr {
    Ref<FunctionSignature> sig;
    Vec<Ref<Expr>> args;
    Vec<Ref<Type>> templates;

    TemplateCallExpr(RefA<FunctionSignature> sig, SourceLoc loc, const Vec<Ref<Type>>& templates,
                     const Vec<Ref<Expr>>& args)
        : Expr(sig->returnType, loc), sig(sig), args(args), templates(templates) {
    }

    DEFINE_VISIT
};

// A constructor call for instantiating structs:
struct ConstructorExpr : Expr {
    size_t constructorIndex; // The index into the constructors list
    Vec<Ref<Expr>> args;
    ArgumentList argsInfo;

    ConstructorExpr(Token name, size_t offset, RefA<Type> type, const Vec<Ref<Expr>>& args,
                    const ArgumentList& argsInfo)
        : Expr(type, name.loc()), constructorIndex(offset), args(args), argsInfo(argsInfo) {
    }

    DEFINE_VISIT
};

// Is it a direct access or through a pointer?
enum class MemberAccessKind { Dot, Arrow };

// a.b or a->b
struct MemberExpr : Expr {
    MemberAccessKind kind;
    Ref<Expr> lhs;
    Ref<MemberVarSymbol> sym;

    MemberExpr(MemberAccessKind kind, RefA<Expr> lhs, RefA<MemberVarSymbol> sym)
        : Expr(sym->type, lhs->loc), kind(kind), lhs(lhs), sym(sym) {
    }

    DEFINE_VISIT
};

// a.b()
struct MemberCallExpr : Expr {
    MemberAccessKind kind;
    Ref<Expr> lhs;
    Ref<FunctionSignature> sig;
    Vec<Ref<Expr>> args;
    Vec<size_t> path;

    MemberCallExpr(SourceLoc loc, MemberAccessKind kind, RefA<Expr> lhs,
                   RefA<FunctionSignature> sig, const Vec<Ref<Expr>>& args, VecA<size_t> path)
        : Expr(sig->returnType, loc), kind(kind), lhs(lhs), sig(sig), args(args), path(path) {
    }

    DEFINE_VISIT
};

// a.b() for a of templated T type constrained by a trait providing method b
struct InterfaceDispatchExpr : Expr {
    MemberAccessKind kind;
    Ref<Expr> lhs;
    Ref<Interface> interface;
    size_t methodId;
    Vec<Ref<Expr>> args;

    InterfaceDispatchExpr(MemberAccessKind kind, RefA<Expr> lhs, RefA<Interface> trait,
                          size_t methodId, const Vec<Ref<Expr>>& args, RefA<Type> result)
        : Expr(result, lhs->loc), kind(kind), lhs(lhs), interface(trait), methodId(methodId),
          args(args) {
    }

    DEFINE_VISIT
};

// A(123)
struct VariantInitExpr : Expr {
    Ref<VariantBaseType> variant;
    VariantField field;
    size_t fieldIndex;
    Ref<Expr> args;

    VariantInitExpr(RefA<VariantBaseType> variant, VariantField field, SourceLoc loc,
                    size_t fieldIndex, RefA<Expr> e)
        : Expr(variant, loc), variant(variant), field(field), fieldIndex(fieldIndex), args(e) {
    }

    DEFINE_VISIT
};

// Vector3{ 1.f, 2.f, 3.f }
struct RecordInitExpr : Expr {
    Vec<Ref<Expr>> inits;
    RecordInitExpr(SourceLoc loc, RefA<Type> type, const Vec<Ref<Expr>>& inits)
        : Expr(type, loc), inits(inits) {
    }

    DEFINE_VISIT
};

// a /= 2.f
struct AssignmentExpr : Expr {
    Token op;
    Ref<Expr> lhs, rhs;

    AssignmentExpr(RefA<Expr> lhs, Token op, RefA<Expr> rhs)
        : Expr(lhs->type(), op.loc()), op(op), lhs(lhs), rhs(rhs) {
    }

    DEFINE_VISIT
};

// self
struct SelfExpr : Expr {
    SelfExpr(RefA<Type> type, SourceLoc loc) : Expr(type, loc) {
    }
    DEFINE_VISIT
};

// For accessing fields and methods implicitly:
struct ImplicitSelfExpr : SelfExpr {
    ImplicitSelfExpr(RefA<Type> type, SourceLoc loc) : SelfExpr(type, loc) {
    }
    DEFINE_VISIT
};

// f(a)
//   for a some object type with a copy constructor receiving by value
struct ImplicitCopyExpr : Expr {
    Ref<Expr> exprToCopy;

    ImplicitCopyExpr(RefA<Expr> exprToCopy)
        : Expr(exprToCopy->type(), exprToCopy->loc), exprToCopy(exprToCopy) {
    }

    DEFINE_VISIT
};

// a[1]
struct ArrayIndexExpr : Expr {
    Ref<Expr> arr, idx;

    ArrayIndexExpr(RefA<Type> ty, RefA<Expr> arr, RefA<Expr> idx)
        : Expr(ty, arr->loc), arr(arr), idx(idx) {
    }

    DEFINE_VISIT
};

// Arm of match exprs and if let exprs
struct Pattern {
    SourceLoc loc;

    Pattern(const SourceLoc& loc) : loc(loc) {
    }
    virtual ~Pattern() {
    }
};

// A
struct VariantPattern : Pattern {
    Str fieldName;
    unsigned id;

    VariantPattern(Token field, unsigned id)
        : Pattern(field.loc()), fieldName(field.text()), id(id) {
    }
};

// A(T)
struct UnwrapPattern : VariantPattern {
    Ref<Type> unwrapType;
    Str unwrapName;

    UnwrapPattern(RefA<Type> unwrapType, StrA unwrapName, Token field, unsigned id)
        : VariantPattern(field, id), unwrapType(unwrapType), unwrapName(unwrapName) {
    }
};

// A -> 1
struct MatchArm {
    Ref<Pattern> pat;
    Block block;
};

// match foo { A -> 1 B -> 2 }
struct MatchExpr : Expr {
    Ref<Expr> match;
    Vec<MatchArm> arms;
    Maybe<Block> fallthrough;
    bool completeMatch;

    MatchExpr(RefA<Type> type, SourceLoc matchToken, RefA<Expr> match, const Vec<MatchArm>& arms,
              const Maybe<Block>& fallthrough, bool completeMatch)
        : Expr(type, matchToken), match(match), arms(arms), fallthrough(fallthrough),
          completeMatch(completeMatch) {
    }

    DEFINE_VISIT
};

// if a { b } else { c }
struct IfExpr : Expr {
    Ref<Expr> condition;
    Block body;
    Maybe<Block> elseBody;

    IfExpr(RefA<Type> type, RefA<Expr>& condition, const Block& body, Maybe<Block> elseBody)
        : Expr(type, condition->loc), condition(condition), body(body), elseBody(elseBody) {
    }

    DEFINE_VISIT
};

// abc : int
struct CastExpr : Expr {
    Ref<Expr> e;
    CastExpr(RefA<Type> type, RefA<Expr> e) : Expr(type, e->loc), e(e) {
    }
    DEFINE_VISIT
};

// sizeof(int)
struct SizeofExpr : Expr {
    Ref<Type> sizeofType;
    SizeofExpr(RefA<Type> ty, SourceLoc loc) : Expr(Type::getI32(), loc), sizeofType(ty) {
    }
    DEFINE_VISIT
};

// constructInto(blob, T(123))
struct ConstructIntoExpr : Expr {
    Token tok;
    Ref<Expr> mem, init;

    ConstructIntoExpr(const Token& tok, RefA<Expr> mem, RefA<Expr> init)
        : Expr(PointerType::of(init->type()), tok.loc()), tok(tok), mem(mem), init(init) {
    }

    DEFINE_VISIT
};

// T(constructorArgPack)
// These are generated inside the template definitions to do the actual forwarding of constructors
// calls.
struct ConstructFromVariadicExpr : Expr {
    ConstructFromVariadicExpr(SourceLoc loc, RefA<Type> type) : Expr(type, loc) {
    }

    DEFINE_VISIT
};

// "abc"
struct StringLiteralExpr : Expr {
    Str text;

    StringLiteralExpr(StrA text, SourceLoc loc)
        : Expr(PointerType::of(Type::getI8()), loc), text(text) {
    }

    DEFINE_VISIT
};

// 123
struct IntLiteralExpr : Expr {
    unsigned val;
    IntLiteralExpr(unsigned val, RefA<Type> type, SourceLoc loc) : Expr(type, loc), val(val) {
    }
    DEFINE_VISIT
};

// 123.f
struct FloatLiteralExpr : Expr {
    double val;
    FloatLiteralExpr(double val, SourceLoc loc) : Expr(Type::getF32(), loc), val(val) {
    }
    DEFINE_VISIT
};

// true
struct BoolLiteralExpr : Expr {
    bool val;
    BoolLiteralExpr(bool val, SourceLoc loc) : Expr(Type::getBool(), loc), val(val) {
    }
    DEFINE_VISIT
};

// [1; 5]
struct ArrayRepeatExpr : Expr {
    Ref<Expr> init;
    size_t length;

    ArrayRepeatExpr(SourceLoc loc, RefA<Expr> init, size_t length)
        : Expr(mkref<ArrayType>(init->type(), length), loc), init(init), length(length) {
    }

    DEFINE_VISIT
};

#undef DEFINE_VISITOR

//
// Statements:
//

struct Stmt {
    SourceLoc loc;
    Stmt(const SourceLoc& loc) : loc(loc) {
    }

    virtual ~Stmt() {
    }
};

struct WhileStmt : Stmt {
    Ref<Expr> condition;
    Block body;

    WhileStmt(SourceLoc loc, const Ref<Expr>& condition, const Block& body)
        : Stmt(loc), condition(condition), body(std::move(body)) {
    }
};

struct BreakStmt : Stmt {
    BreakStmt(SourceLoc loopLoc) : Stmt(loopLoc) {
    }
};

struct ContinueStmt : Stmt {
    ContinueStmt(SourceLoc loopLoc) : Stmt(loopLoc) {
    }
};

struct LetStmt : Stmt {
    Ref<VarDecl> decl;
    LetStmt(const Ref<VarDecl>& decl, SourceLoc loc) : Stmt(loc), decl(decl) {
    }
};

struct ReturnStmt : Stmt {
    Ref<Expr> val;
    ReturnStmt(const Ref<Expr>& val, SourceLoc loc) : Stmt(loc), val(val) {
    }
};

struct ForStmt : Stmt {
    Token bind;
    Ref<Expr> fromContainer;
    Block body;

    ForStmt(const Token& bind, RefA<Expr> fromContainer, const Block& body)
        : Stmt(bind.loc()), bind(bind), fromContainer(fromContainer), body(body) {
    }
};

struct ExprStmt : Stmt {
    Ref<Expr> expr;
    ExprStmt(const Ref<Expr>& expr) : Stmt(expr->loc), expr(expr) {
    }
};

//
// Top-level:
//

struct FunctionDefinition {
    Ref<FunctionSignature> signature;
    Block body;

    FunctionDefinition(RefA<FunctionSignature> sig, const Block& body) : signature(sig), body(body) {
    }
};

struct ConstructorDefinition {
    Token selfToken;
    ArgumentList params;
    Block block;
    Map<size_t, Ref<Expr>> fieldInits;

    ConstructorDefinition(Token selfToken, const ArgumentList& params, const Block& block,
                          const Map<size_t, Ref<Expr>>& fieldInits)
        : selfToken(selfToken), params(params), block(block), fieldInits(fieldInits) {
    }
};

struct DestructorImpl {
    Token selfToken;
    Block block;
};

struct MethodDefinition {
    Ref<MethodSignature> sig;
    Block body;

    MethodDefinition(RefA<MethodSignature> sig, const Block& body) : sig(sig), body(body) {
    }
};

struct RecordImpl {
    Vec<Ref<ConstructorDefinition>> constructors;
    Vec<Ref<FunctionDefinition>> methods;
    Maybe<DestructorImpl> destructor;

    // This is the closing brace of the type definition, so we have a reasonable place to emit the
    // destructor definition:
    SourceLoc closingBrace;
};

// A trait implementation:
struct TraitImpl {
    Ref<Interface> trait;
    Vec<Ref<FunctionDefinition>> methods;

    TraitImpl(RefA<Interface> trait) : trait(trait) {
    }
};

// The information we need to do code generation for a module:
struct ModuleImplementation {
    Str path;
    ModuleImplementation(StrA path) : path(path) {
    }

    Vec<Ref<FunctionDefinition>> functions;
    Map<Str, Ref<FunctionDefinition>> genericFunctions;

    Map<Ref<RecordType>, Ref<RecordImpl>> typeImpls;
    Map<Ref<RecordGeneric>, Ref<RecordImpl>> genericImpls;

    Map<Ref<Type>, Vec<Ref<TraitImpl>>> interfaceImpls;
    Map<Ref<Generic>, Vec<Ref<TraitImpl>>> genericInterfaceImpls;

    Vec<Ref<FunctionSignature>> externs;
};

bool exprBeginsLifetime(Expr* e);
} // namespace Ast
