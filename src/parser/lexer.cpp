#include "common.h"

#include "lexer.h"

Map<Str, Ref<struct LexedFile>> lexedFiles;

namespace {

TokenType operatorTokenFromChar(const char* c) {
    switch (*c) {
        case '*': return TStar;
        case '/': return TSlash;
        case '=': return TEq;
        case '(': return TLParen;
        case ')': return TRParen;
        case '{': return TLBrace;
        case '}': return TRBrace;
        case ';': return TSemi;
        case ':': return TColon;
        case '~': return TTilde;
        case ',': return TComma;
        case '[': return TLSquare;
        case ']': return TRSquare;
        case '.': return TDot;
        case '^': return TCaret;
        case '%': return TMod;
        case '\\': return TBackslash;

        case '#': return c[1] == '[' ? THashBracket : THash;

        case '!': return c[1] == '=' ? TBangEq : TBang;

        case '+': return c[1] == '+' ? TPlusPlus : TPlus;
        case '&': return c[1] == '&' ? TAmpAmp : TAmp;
        case '|': return c[1] == '|' ? TPipePipe : TPipe;
        case '<': return c[1] == '<' ? TLAngleLAngle : TLAngle;

        case '>':
            // Note that the lexer will never create a TRAngleRAngle token. The parser inserts
            // these where necessary:
            return TRAngle;

        case '-':
            if (c[1] == '-')
                return TMinusMinus;
            if (c[1] == '>')
                return TArrow;
            return TMinus;
    }
    unreachable("rip");
}

const Map<Str, TokenType> keywords = {
    { "fun", TFun },         { "let", TLet },       { "else", TElse },
    { "if", TIf },           { "while", TWhile },   { "true", TTrue },
    { "false", TFalse },     { "return", TReturn }, { "type", TType },
    { "match", TMatch },     { "break", TBreak },   { "continue", TContinue },
    { "self", TSelf },       { "open", TOpen },     { "ref", TRef },
    { "impl", TImpl },       { "for", TFor },       { "interface", TInterface },
    { "virtual", TVirtual },
};

} // namespace

// @TODO: I'd really like to remove this. This is atrocious and incomplete.
const char* operatorToString(TokenType op) {
    switch (op) {
        case TPlus: return "+";
        case TStar: return "*";
        case TSlash: return "/";
        case TMinus: return "-";
        case TTilde: return "~";
        case TLAngle: return "<";
        case TLEq: return "<=";
        case TRAngle: return ">";
        case TREq: return ">=";
        case TEq: return "=";
        case TArrow: return "->";

        case TLSquare: return "[";
        case TRSquare: return "]";

        case TColon: return ":";
        case TSemi: return ";";
        case TComma: return ",";

        case TAmp: return "&";
        case TAmpAmp: return "&&";
        case TPipe: return "|";
        case TPipePipe: return "||";

        case TPlusEq: return "+=";
        case TMinusEq: return "-=";
        case TStarEq: return "*=";
        case TSlashEq: return "/=";

        case TLBrace: return "{";
        case TRBrace: return "}";

        case TLParen: return "(";
        case TRParen: return ")";

        case TIdent: return "[id]";

        case TIf: return "if";
        case TReturn: return "return";
        case TFor: return "for";

        default: unreachable("unrecognized operator");
    }
}

// Ugh so these exist because the MS CRT explodes if you call isdigit/isalpha on
// non-ASCII text. SMH.

static bool isNumeric(char c) {
    return c >= '0' && c <= '9';
}

static bool isAlpha(char c) {
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

static bool isAlNum(char c) {
    return isNumeric(c) || isAlpha(c);
}

char translatedEscapedChar(char c) {
    if (c == 0)
        unreachable("EOF in string literal");

    switch (c) {
        case 'n': return '\n';
        case 'r': return '\r';
        case 't': return '\t';
        case '\'': return '\'';
        case '"': return '"';
        case '\\': return '\\';
    }

    if (isNumeric(c))
        return char(c - '0');

    unreachable("unrecognized escape in string");
}

Str getErrorLineText(const char* location, VecA<size_t> newlines, const char* buffer) {
    size_t offset = location - buffer;

    size_t startOfLineOffset;
    if (newlines.empty() || offset < newlines[0]) {
        startOfLineOffset = 0;
    } else {
        auto firstLarger = std::lower_bound(begin(newlines), end(newlines), offset);

        // We can subtract 1 off the firstLarger here because we know we're at least greater than
        // the first one (by the above check):
        startOfLineOffset = *(firstLarger - 1);
    }

    size_t count;
    if (auto newline = strstr(buffer + startOfLineOffset + 1, "\n"))
        count = newline - buffer - startOfLineOffset;
    else
        count = strlen(buffer + startOfLineOffset);

    return Str(buffer + startOfLineOffset, count);
}

#ifdef __clang__
#define FMT_ATTRIBUTE(a, b) __attribute__((__format__(__printf__, a, b)))
#else
#define FMT_ATTRIBUTE(a, b)
#endif

struct Lexer {
    Vec<Token> _tokens;
    Vec<size_t> _newlines;

    const char* filename;
    const char* buffer;
    Lexer(const char* filename, const char* buffer) : filename(filename), buffer(buffer) {
    }

    [[noreturn]] FORMAT_ATTRIBUTE(3, 4)
    void error(const char* curr, const char* fmt, ...) {
        auto lineText = getErrorLineText(curr, _newlines, buffer);

        size_t lineNo = _newlines.size() + 1;
        size_t linCol = curr - &buffer[_newlines.empty() ? 0 : (_newlines.back() + 1)] + 1;

        printf("\033[1;31mLexing error:\033[0m %s(%zu,%zu):\n%s\n", filename, lineNo, linCol,
               lineText.c_str());

        Str underline(std::max(int(linCol - 1), 0), ' ');
        underline.push_back('^');
        printf("%s\n", underline.c_str());

        if (fmt) {
            va_list va;
            va_start(va, fmt);
            auto text = format(fmt, va);
            printf("%s", text.c_str());
        }

        printf("\n");
        exit(-1);
    }

    void run() {
        const char* curr = buffer;

        auto addToken = [&](TokenType type, size_t len) -> Token& {
            _tokens.push_back(Token(type, filename, curr, len));
            auto& tok = _tokens.back();

            tok.col = unsigned(curr - buffer) + 1;
            if (_newlines.size())
                tok.col -= unsigned(_newlines.back()) + 1;

            tok.line = (unsigned)_newlines.size() + 1;

            return tok;
        };

        while (true) {
            // Advance over whitespaces:
            while (*curr == ' ' || *curr == '\t' || *curr == '\r')
                ++curr;

            // Keep track of location of newlines:
            if (*curr == '\n') {
                _newlines.push_back(curr - buffer);
                ++curr;
                continue;
            }

            // Comment:
            if (curr[0] == '/' && curr[1] == '/') {
                while (*curr != '\0' && *curr != '\n')
                    ++curr;
                continue;
            }

            // Multi-line comment:
            if (curr[0] == '/' && curr[1] == '*') {
                curr += 2;

                while (curr[0] != '\0' && curr[1] != '\0') {
                    if (curr[0] == '*' && curr[1] == '/') {
                        curr += 2;
                        break;
                    }

                    if (*curr == '\n')
                        _newlines.push_back(curr - buffer);

                    ++curr;
                }
                continue;
            }

            // Character literals:
            if (*curr == '\'') {
                auto c = curr[1];
                size_t len = 3;

                // Do character escaping:
                if (c == '\\') {
                    ++len;
                    c = translatedEscapedChar(curr[2]);
                } else if (c == 0) {
                    error(curr + 1, "eof in char literal");
                }

                if (curr[len-1] != '\'')
                    error(curr + 2, "char literals are one character");

                auto& tok = addToken(TChar, len);
                tok.num = c;
                curr += len;
                continue;
            }

            // Numbers:
            if (isNumeric(*curr)) {
                unsigned len = 1;
                TokenType type = TInt;

                if (curr[0] == '0') {
                    // Parse hex:
                    if (curr[1] == 'x' || curr[1] == 'X') {
                        ++len;

                        while (isNumeric(curr[len]) || (curr[len] >= 'a' && curr[len] <= 'f') ||
                               (curr[len] >= 'A' && curr[len] <= 'F')) {
                            ++len;
                        }

                        auto& tok = addToken(type, len);

                        auto num = strtoll(curr + 2, nullptr, 16);
                        if (num > 0xFFFFFFFF)
                            unreachable("number too large to be supported yet");

                        tok.num = num & 0xFFFFFFFF;

                        curr += len;
                        continue;
                    }
                }

                while (isNumeric(curr[len]))
                    ++len;

                // A floating point number!
                if (curr[len] == '.') {
                    ++len;
                    type = TDouble;

                    while (isNumeric(curr[len]))
                        ++len;
                    if (curr[len] == 'f') {
                        ++len;
                        type = TFloat;
                    }
                }

                auto& tok = addToken(type, len);
                if (type == TInt) {
                    tok.num = atoi(curr);
                } else {
                    tok.real = atof(curr);
                }

                curr += len;
                continue;
            }

            // Identifiers:
            if (isAlpha(*curr) || *curr == '_') {
                size_t len = 1;

                while (isAlNum(curr[len]) || curr[len] == '_')
                    ++len;

                auto type = TIdent;
                auto it = keywords.find(Str(curr, len));
                if (it != keywords.end())
                    type = it->second;

                addToken(type, len);
                curr += len;
                continue;
            }

            // String literal:
            if (*curr == '"') {
                const char *start = curr, *end = curr + 1;

                Vec<char> text;
                while (*end != '"' && *end != '\0') {
                    if (*end == '\\') {
                        text.push_back(translatedEscapedChar(end[1]));
                        end += 2;
                        continue;
                    }

                    if (*end == '"') {
                        break;
                    }

                    if (*end == 0)
                        unreachable("EOF in string literal");

                    text.push_back(*end);

                    ++end;
                }

                Str processedString(text.begin(), text.end());

                assert(*end == '"');
                unsigned line = (unsigned)_newlines.size() + 1;
                unsigned col = 0; // TODO
                _tokens.push_back(
                    Token(processedString, filename, start, end - start + 1, line, col));
                curr = end + 1;
                continue;
            }

            switch (*curr) {
                // Binary operators:
                case '+':
                case '-':
                case '*':
                case '/':
                case '&':
                case '|':
                case '=':
                case '>':
                case '<':
                case '^':
                case '%': {
                    unsigned length = 1, offset = 0;

                    // Single-line comment:
                    if (curr[0] == '/' && curr[1] == '/') {
                        curr += 2;
                        while (*curr != '\n')
                            ++curr;
                        continue;
                    }

                    // A repeated punctuation mark that becomes another operator:
                    auto isDoubleOp = [](char c) {
                        return c == '|' || c == '&' || c == '<' || c == '+' || c == '-';
                    };

                    // Whether this operator has a x= version:
                    auto isAssignable = [](char c) {
                        return c == '*' || c == '+' || c == '-' || c == '/' || c == '&' ||
                               c == '|' || c == '=' || c == '<' || c == '>' || c == '^' || c == '%';
                    };

                    if (isAssignable(curr[0]) && curr[1] == '=') {
                        offset = TStarEq - TStar;
                        ++length;
                    } else if ((curr[0] == '-' && curr[1] == '>') ||
                               (isDoubleOp(curr[0]) && curr[0] == curr[1])) {
                        ++length;
                    }

                    addToken(TokenType(operatorTokenFromChar(curr) + offset), length);
                    curr += length;

                    continue;
                }

                // Symbols:
                case ';':
                case ':':
                case '{':
                case '}':
                case '(':
                case ')':
                case '~':
                case ',':
                case '[':
                case ']':
                case '.':
                case '!':
                case '#':
                case '\\': 
                    size_t len = 1;

                    if (curr[0] == '#' && curr[1] == '[')
                        len = 2;
                    if (curr[0] == '!' && curr[1] == '=')
                        len = 2;

                    addToken(operatorTokenFromChar(curr), len);
                    curr += len;
                    continue;
            }

            if (*curr == '\0') {
                addToken(TEOF, 1);
                break;
            }

            error(curr, "not a valid token");
        }
    }
};

Ref<LexedFile> lex(StrA in, const char* textToLex) {
    auto filename = heapBackString(in);

    Lexer lex(filename, textToLex);
    lex.run();

    auto ret = mkref<LexedFile>();
    ret->filename = filename;
    ret->buffer = textToLex;
    ret->tokens = std::move(lex._tokens);
    ret->newlines = std::move(lex._newlines);

    lexedFiles[filename] = ret;
    return ret;
}