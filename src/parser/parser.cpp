//
// Top-level parsing code
//

#include "common.h"
#include "lexer.h"
#include "parse_private.h"

using namespace Parsetree;

//
// Statement parsing:
//

static Ref<Stmt> parseStmt(ParseContext& ctx);

Block parseFunctionBlock(ParseContext& ctx) {
    if (ctx == TLBrace) {
        return parseBlock(ctx);
    } else if (auto eq = ctx.tryConsume(TEq)) {
        Block ret;

        // Make a dummy location for debug information:
        ret.openingBrace = ret.closingBrace = eq->loc();

        if ((ret.expr = parseExpr(ctx)))
            return ret;
        else
            ctx.error("expected expr");
    } else {
        ctx.error("invalid start to function");
    }
}

Block parseBlock(ParseContext& ctx) {
    Vec<Ref<Stmt>> body;
    Ref<Expr> expr;

    auto openingBrace = ctx.consume(TLBrace);

    while (true) {
        if (auto stmt = parseStmt(ctx)) {
            body.push_back(stmt);
            continue;
        }

        // Lastly, try to parse an expression:
        if (auto e = parseExpr(ctx)) {
            if (ctx == TSemi) {
                body.push_back(mkref<ExprStmt>(e));
                ++ctx;
                continue;
            }

            // Kind of a bad hack to make it so you don't need semicolons on if statements:
            if (auto ifExpr = dyn_cast<IfExpr>(e)) {
                if (ctx != TRBrace || !ifExpr->yieldsValue()) {
                    body.push_back(mkref<ExprStmt>(e));
                    continue;
                }
            }

            expr = e;
            break;
        }

        break;
    }

    auto braceToken = ctx.consume(TRBrace);

    return { body, expr, openingBrace.loc(), braceToken.loc() };
}

static Ref<Stmt> parseLetBind(ParseContext& ctx) {
    ctx.expect(TLet);

    auto name = ctx.expectIdent("let binding requires name");

    // Optional type annotation:
    Ref<TypeRef> type;
    if (ctx.tryConsume(TColon))
        type = parseTypeRef(ctx);

    ctx.expect(TEq);

    auto expr = parseExpr(ctx);
    if (!expr)
        ctx.error("could not parse initializer");

    ctx.expect(TSemi);

    return mkref<LetStmt>(name, type, expr);
}

static Ref<Stmt> parseWhileStmt(ParseContext& ctx) {
    auto loopTok = *ctx;
    ctx.expect(TWhile);

    auto condition = parseExpr(ctx);
    if (!condition)
        ctx.error("could not parse while condition");

    auto block = parseBlock(ctx);

    return mkref<WhileStmt>(loopTok.loc(), condition, block);
}

static Ref<Stmt> parseReturnStmt(ParseContext& ctx) {
    auto token = *ctx++;

    Ref<Expr> val;
    if (ctx != TSemi && !(val = parseExpr(ctx)))
        ctx.error("failed to parse return expr");
    ctx.expect(TSemi);

    return mkref<ReturnStmt>(token.loc(), val);
}

static Ref<Stmt> parseBreakContinue(ParseContext& ctx) {
    assert(ctx == TBreak || ctx == TContinue);

    auto tok = *(ctx++);
    ctx.expect(TSemi);

    if (tok.is(TBreak))
        return mkref<BreakStmt>(tok.loc());
    else
        return mkref<ContinueStmt>(tok.loc());
}

static Ref<Stmt> parseForStmt(ParseContext& ctx) {
    ctx.expect(TFor);

    auto bind = ctx.expectIdent("loop variable");
    ctx.expect(TColon);
    auto from = parseExpr(ctx);
    auto body = parseBlock(ctx);

    return std::make_shared<ForStmt>(bind, from, body);
}

static Ref<Stmt> parseStmt(ParseContext& ctx) {
    switch (ctx->type) {
        case TLet: return parseLetBind(ctx);
        case TWhile: return parseWhileStmt(ctx);
        case TReturn: return parseReturnStmt(ctx);
        case TFor: return parseForStmt(ctx);

        case TContinue:
        case TBreak: return parseBreakContinue(ctx);

        default: return nullptr;
    }
}

// Parse the definition of a template:
// <A, B: Any, C: Foo + Bar>
static Vec<Ref<TemplateArg>> parseTemplateDefinitionList(ParseContext& ctx) {
    ctx.expect(TLAngle);

    Set<Str> names;
    Vec<Ref<TemplateArg>> types;
    while (true) {
        auto templateNameToken = ctx.expectIdent("generic type name");

        if (lookup(names, templateNameToken.text()))
            ctx.errorAt(templateNameToken, "duplicate template variable");

        Vec<Ref<TypeRef>> constraints;
        if (ctx.tryConsume(TColon)) {
            while (true) {
                if (auto c = parseTypeRef(ctx)) {
                    constraints.push_back(c);

                    if (ctx.tryConsume(TPlus))
                        continue;
                    break;
                }
            }
        }

        types.push_back(mkref<TemplateArg>(templateNameToken.text(), constraints));

        if (ctx.tryConsume(TComma))
            continue;
        break;
    }

    ctx.expect(TRAngle);

    return types;
}

struct Parser {
    Ref<ModuleImplementation> _ast;
    ParseContext iter;

    Parser(RefA<LexedFile> lexedFile)
        : _ast(mkref<ModuleImplementation>()), iter(lexedFile->tokens) {
        _ast->path = lexedFile->filename;
        while (iter.hasNext()) {
            parseTopLevel();
        }
    }

    // TODO generalize to namespace parsing:
    void parseTopLevel() {
        switch (iter->type) {
            case TOpen: parseOpen(); break;

            case THashBracket: parseAnnotation(); break;

            case TFun: parseFunctionDefinition(); break;

            case TType: parseTypeDefinition(); break;

            case TInterface: parseInterface(); break;

            case TImpl: parseTraitImpl(); break;

            default: error("invalid top-level token");
        }
    }

    void parseOpen() {
        expect(TOpen);

        Vec<Str> modulePath;
        while (true) {
            auto name = expectIdent("module name");
            modulePath.push_back(name.text());

            if (iter.tryConsume(TDot))
                continue;

            break;
        }

        _ast->imports.push_back({ modulePath });
    }

    // Parse the implementation of a trait for a type:
    void parseTraitImpl() {
        expect(TImpl);

        Vec<Ref<TemplateArg>> templates;
        if (iter == TLAngle)
            templates = parseTemplateDefinitionList(iter);

        auto trait = parseTypeRef(iter);
        expect(TFor);
        auto type = parseTypeRef(iter);
        expect(TLBrace);

        Vec<Ref<FunctionDefinition>> defs;
        Map<Str, Ref<TypeRef>> typeMap;

        while (true) {
            if (iter == TFun) {
                auto sig = parseFunctionSignature();
                auto body = parseFunctionBlock(iter);
                defs.push_back(mkref<FunctionDefinition>(sig, body));

                continue;
            } else if (iter.tryConsume(TType)) {
                auto name = iter.expectIdent();
                iter.expect(TEq);
                auto ty = parseTypeRef(iter);

                auto& it = typeMap[name.text()];
                if (it)
                    iter.errorAt(name, "name collision");
                it = ty;

                continue;
            }

            if (iter == TRBrace)
                break;

            error("unrecognized token in impl body");
        }

        expect(TRBrace);

        _ast->interfaceImpls.push_back({ templates, type, trait, defs, typeMap });
    }

    void parseInterface() {
        expect(TInterface);
        auto name = expectIdent();

        Vec<Ref<TemplateArg>> templates;
        if (iter == TLAngle)
            templates = parseTemplateDefinitionList(iter);

        expect(TLBrace);

        Vec<Ref<FunctionSignature>> methods;
        Set<Pair<Str, Ref<TypeRef>>> types;

        while (iter != TRBrace) {
            if (iter == TFun) {
                auto sig = parseFunctionSignature();
                if (sig->isTemplated())
                    error("trait methods cannot be templated");
                methods.push_back(sig);
            } else if (iter.tryConsume(TType)) {
                auto name = iter.expectIdent();

                Ref<TypeRef> constraint;
                if (iter.tryConsume(TColon))
                    constraint = parseTypeRef(iter);

                types.insert(std::make_pair(name.text(), constraint));
            } else {
                error("unrecognized trait token");
            }
        }
        expect(TRBrace);

        _ast->interfaces.push_back({ name, methods, types });
    }

    // These kind of annotations can only be used to parse external C functions ATM:
    void parseAnnotation() {
        expect(THashBracket);
        auto annot = expectIdent().text();
        expect(TRSquare);

        if (annot != "extern")
            error("only annotation is 'extern'");
        if (iter != TFun)
            error("can only have annotations on functions");

        auto sig = parseFunctionSignature();
        if (sig->isTemplated())
            error("C ABI externs cannot have template arguments");

        _ast->externs.push_back(sig);
    }

    // Produce an error at the current location:
    [[noreturn]] FORMAT_ATTRIBUTE(2, 3) inline void error(const char* fmt, ...) {
        va_list va;
        va_start(va, fmt);
        iter.errorAt(iter->loc(), fmt, va);
    }

    //
    // Token handling:
    //

    void expect(TokenType type) {
        iter.expect(type);
    }

    Token expectIdent(const char* msg = nullptr) {
        if (iter != TIdent) {
            error("Expected an identifier: %s\n", msg);
        }

        return *iter++;
    }

    void parseFunctionDefinition() {
        auto sig = parseFunctionSignature();
        auto body = parseFunctionBlock(iter);

        _ast->functions.push_back(mkref<FunctionDefinition>(sig, body));
    }

    // (a: int, b: float)
    ArgumentList parseArgumentList() {
        expect(TLParen);

        ArgumentList params;
        if (iter != TRParen) {
            while (true) {
                auto ref = bool(iter.tryConsume(TRef));
                auto name = expectIdent("expected parameter name").text();
                expect(TColon);

                auto type = parseTypeRef(iter);

                auto flags = ref ? VFlags::ref : VFlags::none;
                params.push_back(Argument(name, type, flags));

                if (iter.tryConsume(TComma))
                    continue;
                if (iter == TRParen)
                    break;
            }
        }

        expect(TRParen);
        return params;
    }

    // fn abc(a: int, b: float, c: double) -> bool
    Ref<FunctionSignature> parseFunctionSignature() {
        expect(TFun);

        auto name = expectIdent("expected function name");

        Vec<Ref<TemplateArg>> templates;
        if (iter == TLAngle)
            templates = parseTemplateDefinitionList(iter);

        auto args = parseArgumentList();

        Ref<TypeRef> returnType = nullptr;
        if (iter.tryConsume(TArrow))
            returnType = parseTypeRef(iter);

        return mkref<FunctionSignature>(name, args, returnType, templates);
    }

    // A | B(int) | C(T)
    void parseVariantDefinition(Token name, const Vec<Ref<TemplateArg>>& templates) {
        // There is an optional first pipe character:
        ignore(iter.tryConsume(TPipe));

        Vec<Field> fields;
        while (iter == TIdent) {
            auto name = expectIdent();

            Ref<TypeRef> type;
            if (iter.tryConsume(TLParen)) {
                type = parseTypeRef(iter);
                expect(TRParen);
            }
            fields.push_back({ name, type });

            if (iter.tryConsume(TPipe))
                continue;
            break;
        }

        if (fields.empty())
            error("variant definition malformed");

        _ast->types.push_back(mkref<VariantDefinition>(name, templates, fields));
    }

    void parseTypeDefinition() {
        expect(TType);
        auto name = expectIdent("Expected type name");

        // Template parameters:
        Vec<Ref<TemplateArg>> templates;
        if (iter == TLAngle)
            templates = parseTemplateDefinitionList(iter);

        // A brace means we're parsing a record type:
        if (iter.tryConsume(TEq)) {
            // @TODO This could be a variant type or a type synonym! Handle the latter case.
            parseVariantDefinition(name, templates);
        } else {
            parseRecordBody(name, templates);
        } 
    }

    void parseRecordBody(Token name, const Vec<Ref<TemplateArg>>& templates) {
        Vec<Ref<TypeRef>> bases;
        if (iter.tryConsume(TColon)) {
            while (true) {
                auto ty = parseTypeRef(iter);
                if (!ty)
                    error("expected base type");

                bases.push_back(ty);
                if (iter.tryConsume(TComma))
                    continue;
                break;
            }
        }

        expect(TLBrace);

        auto record = mkref<RecordDefinition>(name, templates, bases);
        _ast->types.push_back(record);

        while (true) {
            if (iter == TIdent) {
                Vec<Token> fieldNames = { expectIdent() };
                while (iter.tryConsume(TComma))
                    fieldNames.push_back(expectIdent("field name"));

                expect(TColon);
                auto type = parseTypeRef(iter);
                expect(TSemi);

                for (const auto& name : fieldNames)
                    record->fields.push_back({ name, type });
            } else if (iter == TSelf) {
                auto selfToken = *iter++;
                auto args = parseArgumentList();

                // Parse field initializers for this constructor:
                Vec<ConstructorInitField> initializers;
                if (iter.tryConsume(TColon)) {
                    while (true) {
                        auto field = iter.expectIdent("field name");
                        auto args = parseCallList(iter);

                        initializers.push_back({ field, args });

                        if (iter.tryConsume(TComma))
                            continue;
                        break;
                    }
                }

                auto block = parseBlock(iter);
                ConstructorDefinition def = { selfToken, args, initializers, block };
                record->constructors.push_back(def);
            } else if (iter == TTilde) {
                if (record->destructor)
                    error("cannot have multiple destructors");

                auto selfTok = *++iter;
                expect(TSelf);
                if (!parseArgumentList().empty())
                    iter.errorAt(selfTok, "destructors take no arguments");

                auto block = parseBlock(iter);
                DestructorDefinition def = { selfTok, block };
                record->destructor = def;
            } else if (iter == TFun || iter == TVirtual) {
                bool isVirtual = iter.tryConsume(TVirtual) != none;

                auto functionSig = parseFunctionSignature();
                auto sig = mkref<MethodSignature>(functionSig, isVirtual);

                auto body = parseFunctionBlock(iter);
                record->methods.push_back(MethodDefinition(sig, body));
            } else if (iter == TRBrace) {
                break;
            } else {
                error("unrecognized token when parsing record body");
            }
        }

        record->closingBrace = iter->loc();

        expect(TRBrace);
    }
};

//
// This is the entry-point to the top-level parser:
//

char* heapBackString(const Str& str) {
    char* text = new char[str.size() + 1];
    for (size_t i = 0; i < str.size(); ++i)
        text[i] = str.at(i);
    text[str.size()] = '\0';
    return text;
}

Ref<ModuleImplementation> parse(StrA filename) {
    auto maybeFileText = readFile(filename);

    if (!maybeFileText) {
        fprintf(stderr, "Input file %s does not exist", filename.c_str());
        exit(-1);
    }

    auto fileText = heapBackString(*maybeFileText);

    auto lexedFile = lex(filename, fileText);
    return Parser(lexedFile)._ast;
}
