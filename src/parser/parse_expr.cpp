//
// Expression parsing:
//

#include "common.h"

#include "lexer.h"
#include "parse_private.h"

enum Associativity { left, right };
struct OpInfo {
    int prec;
    Associativity assoc;
};

const Map<TokenType, OpInfo> precedence = {
    // Assignment operators:
    { TEq, { 2, right } },
    { TPlusEq, { 2, right } },
    { TMinusEq, { 2, right } },
    { TSlashEq, { 2, right } },
    { TAmpEq, { 2, right } },
    { TPipeEq, { 2, right } },
    { TStarEq, { 2, right } },

    { TColon, { 3, left } },

    { TPipePipe, { 4, left } },
    { TAmpAmp, { 5, left } },
    { TPipe, { 6, left } },
    { TAmp, { 7, left } },

    { TEqEq, { 8, left } },
    { TBangEq, { 8, left } },
    { TLEq, { 10, left } },
    { TREq, { 10, left } },

    // Comparison:
    { TLAngle, { 10, left } },
    { TRAngle, { 10, left } },

    // Shift:
    { TLAngleLAngle, { 12, left } },
    { TRAngleRAngle, { 12, left } },

    // Arithmetic:
    { TPlus, { 14, left } },
    { TMinus, { 14, left } },
    { TStar, { 15, left } },
    { TSlash, { 15, left } },
    { TMod, { 15, left } },

    { TDot, { 19, right } },
    { TArrow, { 19, right } },

    { TLSquare, { 20, right } },
};

static Maybe<Pair<int, Token>> shouldConsumeInfix(ParseContext& ctx, int prec) {
    auto tok = *ctx;

    //
    // Hack around the fact that >> isn't a real token in our stream:
    //
    // If we've got two '>' beside one another while parsing an expression, we
    // treat this as a new fake token.
    //

    bool isRightShift = false;
    auto next = ctx.peek(1);
    if (ctx == TRAngle && next == TRAngle && ctx->offset + 1 == next->offset) {
        isRightShift = true;
        tok.type = TRAngleRAngle;
        ++tok.length;
    }

    if (auto opInfo = lookup(precedence, tok.type)) {
        if (opInfo->prec > prec || (opInfo->prec == prec && opInfo->assoc == right)) {
            ++ctx;

            // If we're a right shift then we're consuming an additional token
            // for the second '>':
            if (isRightShift)
                ++ctx;

            return std::make_pair(opInfo->prec, tok);
        }
    }

    return none;
}

// Parse a comma-separated list of arguments to a call:
Vec<Ref<Expr>> parseCallList(ParseContext& ctx) {
    ctx.expect(TLParen);

    Vec<Ref<Expr>> args;
    while (true) {
        if (auto e = parseExpr(ctx)) {
            args.push_back(e);

            if (ctx == TComma) {
                ++ctx;
                continue;
            }
        }

        ctx.expect(TRParen);
        break;
    }
    return args;
}

Ref<TypeRef> attemptParseTypeRef(ParseContext& ctx) {
    auto nameToken = *ctx;

    Ref<TypeRef> ret;
    if (auto tok = ctx.tryConsume(TIdent)) {
        auto name = tok->text();

        // Regular template notation:
        if (ctx == TLAngle) {
            auto templates = parseTemplateInstList(ctx);
            ret = mkref<TemplateInstanceRef>(nameToken, templates);
        } else {
            ret = mkref<SimpleTypeRef>(*tok);
        }
    } else {
        return nullptr;
    }

    while (ctx.tryConsume(TStar))
        ret = mkref<PointerTypeRef>(nameToken, ret);

    return ret;
}

Maybe<Vec<Ref<TypeRef>>> attemptParseTemplateInst(ParseContext& ctx) {
    if (ctx != TLAngle)
        return none;

    auto speculativeParse = ctx.beginSpeculativeParse();
    ++ctx;

    Vec<Ref<TypeRef>> templates;
    while (true) {
        if (auto curr = attemptParseTypeRef(ctx))
            templates.push_back(curr);
        else
            return none;

        if (ctx.tryConsume(TComma))
            continue;

        break;
    }

    if (!ctx.tryConsume(TRAngle))
        return none;

    speculativeParse.accept();
    return templates;
}

// Parse an expression beginning with an identifier:
Ref<Expr> parseExprWithIdent(ParseContext& ctx) {
    auto name = ctx.expectIdent();

    // Intrinsics:
    if (name.text() == "sizeof") {
        ctx.expect(TLParen);
        auto type = parseTypeRef(ctx);
        ctx.expect(TRParen);

        return mkref<SizeofExpr>(name.loc(), type);
    }

    Maybe<Vec<Ref<TypeRef>>> templates;
    if (ctx == TLAngle)
        templates = attemptParseTemplateInst(ctx);

    if (ctx == TDot && ctx.peek(1) == TLBrace) {
        Ref<TypeRef> type;
        if (templates) {
            type = mkref<TemplateInstanceRef>(name, *templates);
        } else {
            type = mkref<SimpleTypeRef>(name);
        }

        ctx.expect(TDot);
        ctx.expect(TLBrace);

        // If the next token is a brace and we're a type then this is
        // constructing a structure:
        Vec<Ref<Expr>> inits;
        while (true) {
            if (auto e = parseExpr(ctx)) {
                inits.push_back(e);

                if (ctx == TComma)
                    ++ctx;
            } else {
                ctx.expect(TRBrace);
                break;
            }
        }

        return mkref<RecordInitExpr>(type, inits);
    }

    // Call syntax:
    if (ctx == TLParen) {
        auto args = parseCallList(ctx);

        return mkref<CallExpr>(name, templates, args);
    }

    return mkref<IdExpr>(name);
}

Ref<Expr> parseMatchExpr(ParseContext& ctx) {
    auto matchToken = *ctx++;

    auto match = parseExpr(ctx);
    ctx.expect(TLBrace);

    Vec<MatchArm> arms;
    while (true) {
        if (ctx == TRBrace)
            break;

        auto id = ctx.expectIdent("match label");

        Maybe<Token> unwrap;
        if (ctx.tryConsume(TLParen)) {
            unwrap = ctx.expectIdent("unwrap variable name");
            ctx.expect(TRParen);
        }

        auto pat = mkref<Pattern>(id, unwrap);

        // Parse a single expression after the arrow:
        Block block;
        if (ctx.tryConsume(TArrow)) {
            auto expr = parseExpr(ctx);
            block.expr = expr;
        } else {
            block = parseBlock(ctx);
        }

        arms.push_back({ pat, block });
    }

    ctx.expect(TRBrace);

    return mkref<MatchExpr>(matchToken, match, arms);
}

Ref<IfExpr> parseIfExpr(ParseContext& ctx) {
    auto ifToken = ctx.consume(TIf);

    auto condition = parseExpr(ctx);
    auto body = parseBlock(ctx);

    // Do we have an else clause?
    Maybe<Block> elseBody;
    if (ctx.tryConsume(TElse)) {
        if (ctx == TIf) {
            auto ie = parseIfExpr(ctx);

            Block elseBlock;
            if (!ie->yieldsValue())
                elseBlock.stmts.push_back(mkref<ExprStmt>(ie));
            else
                elseBlock.expr = ie;

            elseBody = elseBlock;
        } else {
            elseBody = parseBlock(ctx);
        }
    }

    return mkref<IfExpr>(ifToken, condition, body, elseBody);
}

// Parse a simple expression without any operators in it:
Ref<Expr> parseAtomExpr(ParseContext& ctx, int prec) {
    if (ctx == TIdent)
        return parseExprWithIdent(ctx);

    // Various constants:
    if (auto tok = ctx.tryConsume(TInt))
        return mkref<IntLiteralExpr>(tok->num, tok->loc(), ITT_INT);

    if (auto tok = ctx.tryConsume(TChar))
        return mkref<IntLiteralExpr>(tok->num, tok->loc(), ITT_CHAR);

    if (auto tok = ctx.tryConsume(TFloat))
        return mkref<FloatLiteralExpr>(tok->real, tok->loc());

    if (auto tok = ctx.tryConsume(TTrue))
        return mkref<BoolLiteralExpr>(true, tok->loc());

    if (auto tok = ctx.tryConsume(TFalse))
        return mkref<BoolLiteralExpr>(false, tok->loc());

    if (auto tok = ctx.tryConsume(TStringLiteral))
        return mkref<StringLiteralExpr>(tok->stringLiteral, tok->loc());

    if (auto tok = ctx.tryConsume(TSelf))
        return mkref<SelfExpr>(*tok);

    // Paren'ed expressions:
    if (ctx.tryConsume(TLParen)) {
        auto e = parseExpr(ctx);
        ctx.expect(TRParen);

        return e;
    }

    // Arrays:
    if (auto squareToken = ctx.tryConsume(TLSquare)) {
        if (ctx.tryConsume(TRSquare))
            ctx.errorAt(*squareToken, "zero-length errors not supported yet.");

        auto fst = parseExpr(ctx);

        // Repeated expr [a; 5]:
        if (ctx.tryConsume(TSemi)) {
            auto countExpr = parseExpr(ctx);
            ctx.expect(TRSquare);

            return mkref<ArrayRepeatExpr>(*squareToken, fst, countExpr);
        }

        // Otherwise, we're doing a standard [a,b,c] style init:
        Vec<Ref<Expr>> inits = { fst };

        while (true) {
            if (ctx.tryConsume(TComma)) {
                auto e = parseExpr(ctx);
                if (!e)
                    break;
                inits.push_back(e);
                continue;
            } else {
                break;
            }
        }

        ctx.expect(TRSquare);
        return mkref<ArrayInitExpr>(*squareToken, inits);
    }

    // |a| norm expressions:
    if (auto tok = ctx.tryConsume(TPipe)) {
        auto pipePrecedence = *lookup(precedence, TPipe);
        if (auto e = parseExpr(ctx, pipePrecedence.prec + 1)) {
            ctx.expect(TPipe);
            return mkref<NormExpr>(tok->loc(), e);
        } else {
            ctx.error("failed to parse norm expr");
        }
    }

    // Unary operators:
    if (ctx == TMinus || ctx == TTilde || ctx == TBang || ctx == TStar || ctx == TAmp) {
        auto op = *ctx++;

        Map<TokenType, int> unaryPrec = {
            { TStar, 18 },
            { TTilde, 18 },
            { TAmp, 18 },
        };

        int recursePrec = prec;
        if (auto p = lookup(unaryPrec, op.type))
            recursePrec = *p;

        if (auto e = parseExpr(ctx, recursePrec))
            return mkref<UnaryExpr>(op, e);

        ctx.error("Unable to parse expression following unary operator");
    }

    return nullptr;
}

// Parse the instantiation of a template:
Vec<Ref<TypeRef>> parseTemplateInstList(ParseContext& ctx) {
    ctx.expect(TLAngle);

    Vec<Ref<TypeRef>> templates;
    while (true) {
        auto curr = parseTypeRef(ctx);
        templates.push_back(curr);

        if (ctx.tryConsume(TComma))
            continue;

        break;
    }

    ctx.expect(TRAngle);

    return templates;
}

// Parse a reference to a type:
Ref<TypeRef> parseTypeRef(ParseContext& ctx) {
    auto nameToken = *ctx;

    Ref<TypeRef> ret;
    if (auto tok = ctx.tryConsume(TIdent)) {
        auto name = tok->text();

        // Regular template notation:
        if (ctx == TLAngle) {
            auto templates = parseTemplateInstList(ctx);
            ret = mkref<TemplateInstanceRef>(nameToken, templates);
        } else {
            ret = mkref<SimpleTypeRef>(*tok);
        }
    } else if (auto tok = ctx.tryConsume(TBackslash)) {
        ctx.expect(TLParen);

        Vec<Ref<TypeRef>> args;
        while (true) {
            if (ctx == TRParen)
                break;

            args.push_back(parseTypeRef(ctx));

            if (ctx.tryConsume(TComma))
                continue;
            else
                break;
        }

        ctx.expect(TRParen);

        Ref<TypeRef> returnType;
        if (ctx.tryConsume(TArrow))
            returnType = parseTypeRef(ctx);

        ret = mkref<FunctionTypeRef>(*tok, returnType, args);
    } else {
        ctx.error("unrecognized token in type description");
    }

    while (true) {
        if (ctx.tryConsume(TStar)) {
            ret = mkref<PointerTypeRef>(nameToken, ret);
        } else if (ctx.tryConsume(TLSquare)) {
            auto numExpr = parseExpr(ctx);

            if (auto il = dyn_cast<IntLiteralExpr>(numExpr))
                ret = mkref<ArrayTypeRef>(nameToken, ret, il->val);
            else
                ctx.errorAt(numExpr->loc, "expected constant int");

            ctx.expect(TRSquare);
        } else {
            break;
        }
    }

    return ret;
}

//
// This is the only exported function from this file. It is the entry to the
// expression parser.
//

Ref<Expr> parseExpr(ParseContext& ctx, int prec) {
    Ref<Expr> result;

    if (ctx == TMatch)
        result = parseMatchExpr(ctx);
    else if (ctx == TIf)
        result = parseIfExpr(ctx);
    else
        result = parseAtomExpr(ctx, prec);

    if (result) {
        while (auto infixOp = shouldConsumeInfix(ctx, prec)) {
            auto [newPrec, op] = *infixOp;

            //
            // There are a class of tokens here which start seemingly like infix
            // operators but are really kickoffs for different types of parsing
            // after an expression.
            //

            if (op.type == TLSquare) {
                // a[n] is array syntax:
                auto idx = parseExpr(ctx);
                ctx.expect(TRSquare);
                result = mkref<ArrayIndexExpr>(result, idx);
            } else if (op.type == TDot || op.type == TArrow) {
                auto name = ctx.expectIdent("Expected field as rhs of dot");

                if (ctx == TLParen) {
                    auto args = parseCallList(ctx);
                    result = mkref<DotCallExpr>(result, name, args, op.type == TArrow);
                } else {
                    result = mkref<DotExpr>(result, name, op.type == TArrow);
                }
            } else if (op.type == TColon) {
                // RHS of cast operator is the type we're casting to:
                if (auto ty = parseTypeRef(ctx))
                    result = mkref<CastExpr>(result, ty);
                else
                    ctx.error("expected type as rhs of cast operator");
            } else {
                if (auto rhs = parseExpr(ctx, newPrec))
                    result = mkref<InfixExpr>(result, op, rhs);
                else
                    ctx.error("expected rhs of binary operator");
            }
        }
    }

    return result;
}
