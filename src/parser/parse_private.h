//
// Private code shared by the parser
//

#pragma once

#include "parsetree.h"

using namespace Parsetree;

class ParseContext {
    const Vec<Token>& tokens;
    Vec<Token>::const_iterator curr;

  public:
    ParseContext(const Vec<Token>& tokens) : tokens(tokens), curr(tokens.begin()) {
    }

    struct SpeculativeParseHelper {
        ParseContext* pc;
        Vec<Token>::const_iterator curr;
        bool shouldRollback = true;

        SpeculativeParseHelper(ParseContext* pc) : pc(pc), curr(pc->curr) {
        }

        void accept() {
            shouldRollback = false;
        }

        ~SpeculativeParseHelper() {
            if (shouldRollback)
                pc->curr = curr;
        }
    };

    SpeculativeParseHelper beginSpeculativeParse() {
        return SpeculativeParseHelper(this);
    }

    bool operator==(TokenType type) const {
        return curr->is(type);
    }

    bool operator!=(TokenType type) const {
        return !curr->is(type);
    }

    Token operator*() const {
        return *curr;
    }

    bool hasNext() const {
        return curr != tokens.end() && !curr->is(TEOF);
    }

    ParseContext& operator++() {
        ++curr;
        return *this;
    }

    ParseContext operator++(int) {
        auto ret = *this;
        ++*this;
        return ret;
    }

    ParseContext peek(unsigned ahead = 1) {
        auto ret = *this;
        for (size_t i = 0; i < ahead; ++i)
            ++ret;
        return ret;
    }

    const Token* operator->() const {
        return &*curr;
    }

    void unread() {
        --curr;
    }

    [[noreturn]] void errorAt(SourceLoc loc, const char* fmt, va_list va) {
        auto lineText = lexedFiles[loc.file]->getErrorLineText(loc.offset);
        printf("\033[1;31mParse error:\033[0m %s(%u,%u):\n%s\n", loc.file, loc.line, loc.col,
               lineText.c_str());

        Str underline(std::max(int(loc.col - 1), 0), ' ');
        underline.push_back('^');
        printf("%s\n", underline.c_str());

        if (fmt) {
            auto text = format(fmt, va);
            printf("%s", text.c_str());
        }

        printf("\n");
        exit(-1);
    }

    [[noreturn]] FORMAT_ATTRIBUTE(3, 4) void errorAt(SourceLoc loc, const char* fmt, ...) {
        va_list va;
        va_start(va, fmt);
        errorAt(loc, fmt, va);
    }

    [[noreturn]] FORMAT_ATTRIBUTE(3, 4) void errorAt(Token token, const char* fmt, ...) {
        va_list va;
        va_start(va, fmt);
        errorAt(token.loc(), fmt, va);
    }

    [[noreturn]] FORMAT_ATTRIBUTE(2, 3) void error(const char* fmt, ...) {
        va_list va;
        va_start(va, fmt);
        errorAt(curr->loc(), fmt, va);
    }

    void expect(TokenType type) {
        ignore(consume(type));
    }

    Token expectIdent(const char* msg = "") {
        if (curr->type != TIdent) {
            errorAt(*curr, "Expected an identifier: %s\n", msg);
        }

        return *curr++;
    }

    // Tries to consume a type of token. Returns whether succesful.
    Maybe<Token> tryConsume(TokenType type) {
        if (curr->type == type) {
            return *(curr++);
        }

        return none;
    }

    Token consume(TokenType type) {
        if (curr->type == type)
            return *(curr++);

        char fullError[1024];
        ignore(snprintf(fullError, sizeof(fullError), "    Unexpected token '%s' (expected '%s')",
                        curr->text().c_str(), operatorToString(type)));
        errorAt(*curr, "%s", fullError);
    }
};

Ref<Expr> parseExpr(ParseContext& ctx, int prec = -1);

Block parseBlock(ParseContext& ctx);
Ref<TypeRef> parseTypeRef(ParseContext& ctx);
Vec<Ref<TypeRef>> parseTemplateInstList(ParseContext& ctx);
Vec<Ref<Expr>> parseCallList(ParseContext& ctx);
