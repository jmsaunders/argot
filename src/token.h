//
// token.h
// Results of lexical analysis.
//

#pragma once

#include "common.h"

// Token types:
// clang-format off
enum TokenType {
    // Built-in keywords:
    TLet, TFun, TIf, TElse, TWhile, TTrue, TFalse, TReturn, TType, TSelf, TMatch, TBreak,
    TContinue, TInterface, TOpen, TRef, TImpl, TFor, TVirtual,

    TIdent,
    TInt,
    TChar,
    TFloat,
    TDouble,
    TStringLiteral,

    // Binary operators and their x= form (these have to remain in the same order!):
    TStar, TPlus, TMinus, TSlash, TAmp, TPipe, TEq, TLAngle, TRAngle, TCaret, TMod,
    TStarEq, TPlusEq, TMinusEq, TSlashEq, TAmpEq, TPipeEq, TEqEq, TLEq, TREq, TCaretEq, TModEq,

    // !=
    TBangEq,

    // && || ++ --
    TAmpAmp, TPipePipe, TPlusPlus, TMinusMinus,

    // ( ) { } < >
    TLParen, TRParen, TLBrace, TRBrace, TLSquare, TRSquare,

    // << >> <= >=
    TLAngleLAngle, TRAngleRAngle,

    // ; : ~ , -> . ! \ 
    TSemi, TColon, TTilde, TComma, TArrow, TDot, TBang, THash, THashBracket, TBackslash,

    TEOF,
};
// clang-format on

struct SourceLoc {
    const char* offset;
    const char* file = nullptr;

    unsigned line = 0, col = 0;

    bool operator == (const SourceLoc& rhs) const {
        return offset == rhs.offset && file == rhs.file;
    }
};

namespace std {
    template <>
    struct hash<SourceLoc> {
        std::size_t operator()(const SourceLoc& k) const {
            return std::hash<size_t>()(size_t(k.offset)) ^ std::hash<string>()(k.file);
        }
    };
}

struct Token {
    Token(TokenType type, const char* filename, const char* offset, size_t len)
        : type(type), filename(filename), offset(offset), length(len) {
    }

    Token(StrA stringLiteral, const char* filename, const char* offset, size_t len, unsigned line, unsigned col)
        : type(TStringLiteral), filename(filename), offset(offset), length(len),
          line(line), col(col), stringLiteral(stringLiteral) {}

    TokenType type;
    const char* filename;
    const char* offset;
    size_t length;

    unsigned line = 0, col = 0;

    Str text() const {  return Str(offset, length); }

    union {
        unsigned int num;
        double real = 0;
    };
    Str stringLiteral;

    bool is(unsigned t) const {
        return type == t;
    }

    SourceLoc loc() const { return { offset, filename, line, col }; }
};

