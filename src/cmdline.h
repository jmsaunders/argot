//
// A simple command-line flag parser:
//

#pragma once

namespace cl {

struct PositionalOpt {
    Maybe<Str> value;

    PositionalOpt(Maybe<Str> def = none);
};

struct OptBase {
    Str name;

    virtual bool handleParam(Maybe<Str> assignedValue, Maybe<Str> nextArg) = 0;

  protected:
    OptBase(StrA name);
};

// returns whether or not it consumed the next argument:
template <typename T> bool parseFlag(Maybe<Str> assignedValue, Maybe<Str> nextArg, T* val);

template <> inline bool parseFlag<bool>(Maybe<Str> assignedValue, Maybe<Str> /*nextArg*/, bool* val) {
    auto parseBool = [](StrA val) {
        if (val == "true" || val == "1" || val == "True" || val == "yes" || val == "Yes")
            return true;
        if (val == "false" || val == "0" || val == "False" || val == "no" || val == "No")
            return false;

        printf("Unrecognized boolean flag: %s\n", val.c_str());
        exit(-1);
    };

    if (assignedValue)
        *val = parseBool(*assignedValue);
    else
        *val = true;

    return false;
}

template <>
inline bool parseFlag<int>(Maybe<Str> assignedValue, Maybe<Str> nextArgument, int* val) {
    if (assignedValue) {
        *val = atoi(assignedValue->c_str());
        return false;
    }

    if (!nextArgument) {
        printf("expected a value when parsing an integer");
        exit(-1);
    }

    *val = atoi(nextArgument->c_str());
    return true;
}

template <typename T> struct Opt : OptBase {
    T val;
    Opt(StrA name, T def = {}) : OptBase(name), val(def) {}

    bool handleParam(Maybe<Str> assignedValue, Maybe<Str> nextArg) override {
        return cl::parseFlag(assignedValue, nextArg, &val);
    }

    operator T() const { return val; }
};

} // namespace cl

void parseCommandLine(int argc, const char** argv);
extern cl::Opt<bool> verboseOpt;
