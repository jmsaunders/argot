// trait.h
// Code to handle trait information:

#pragma once

//
// A type that needs to be bound in a trait definition context:
//

struct TraitInternalType : ReplacableType, std::enable_shared_from_this<TraitInternalType> {
    TraitInternalType(StrA name, RefA<Interface> iface)
        : ReplacableType(name), iface(iface) {
    }

    Ref<Type> ref() override {
        return shared_from_this();
    }

    Ref<Type> type;
    Ref<Interface> iface;
};

// A compile-time interface or trait:
struct Interface {
    static Ref<Interface> make(StrA name);
    virtual ~Interface() {
    }

    Str name;
    Map<Str, Ref<TraitInternalType>> types;
    Vec<Ref<FunctionSignature>> methods;

    Ref<TraitSelfType> traitSelf;

    bool equals(RefA<Interface> other) const {
        // temporary hack but good enough for now.
        return name == other->name;
    }

    // Generate an identical interface holding information about the type it's working on.
    // This is so we can reconstruct at codegen time the type information.
    virtual Ref<Interface> specialize(RefA<Type> type);

  protected:
    Interface(Str name) : name(name) {
    }

    void fillout(Ref<Interface> other, RefA<Type> type) const;
};

// A type-parameterized trait:
struct InterfaceGeneric : std::enable_shared_from_this<InterfaceGeneric> {
    Str name;
    Vec<Ref<TemplateArgType>> templates;

    Vec<Ref<FunctionSignature>> methods;
    Map<Str, Ref<TraitInternalType>> types;

    InterfaceGeneric(StrA name) : name(name) {
    }

    virtual ~InterfaceGeneric() {
    }


    Ref<Interface> instantiate(VecA<Ref<Type>> subs);
    virtual Ref<Interface> instantiate(const SubstitutionMap& subs);
};

// An instance of a generic interface:
struct InterfaceGenericInstance : Interface {
    Ref<InterfaceGeneric> parent;
    SubstitutionMap args;

    static Ref<Interface> make(StrA name, RefA<InterfaceGeneric> parent,
                               const SubstitutionMap& args);

    virtual Ref<Interface> specialize(RefA<Type> type);

  protected:
    InterfaceGenericInstance(StrA name, RefA<InterfaceGeneric> parent, const SubstitutionMap& args)
        : Interface(name), parent(parent), args(args) {
    }
};

struct ConstructorArgTrait : Interface, std::enable_shared_from_this<ConstructorArgTrait> {
    static Ref<Interface> make(RefA<Type> forType);
    Ref<Type> forType;

    Ref<Interface> specialize(RefA<Type> type) override {
        return shared_from_this();
    }

  protected:
    ConstructorArgTrait(RefA<Type> forType) : Interface("ConstructorArgs"), forType(forType) {
    }
};

// A dummy type to represent the successfuly-bound constructor call:
struct ConstructorArgTraitIndex : Type, std::enable_shared_from_this<ConstructorArgTraitIndex> {
    Ref<RecordBaseType> record;
    size_t constructorIndex;

    ConstructorArgTraitIndex(RefA<ConstructorArgTrait> trait, RefA<Type> type,
                             size_t constructorIndex)
        : Type(prettyNameForType(type, constructorIndex)), record(type->recordType()),
          constructorIndex(constructorIndex) {

        Vec<Ref<FunctionSignature>> methods;
        for (const auto& m : trait->methods)
            methods.push_back(m);

        traits.push_back(mkref<TraitInstanceInfo>(trait, SubstitutionMap(), methods));
    }

    Ref<Type> ref() override {
        return shared_from_this();
    }

  private:
    static Str prettyNameForType(RefA<Type>, size_t constructorIndex);
};

struct ConstructorArgTraitGeneric : InterfaceGeneric {
    ConstructorArgTraitGeneric() : InterfaceGeneric("ConstructArgs") {
        static auto typeArgument = mkref<TemplateArgType>("ConstructType");
        templates = { typeArgument };
    }

    Ref<Interface> instantiate(const SubstitutionMap& subs) override {
        assert(subs.size() == 1);
        return ConstructorArgTrait::make(subs.begin()->second);
    }
};

bool constraintSatisfiedByType(RefA<Interface> i, RefA<Type> type);
Maybe<SubstitutionMap> inferTraitImplTemplates(RefA<TraitInstanceInfo>& supportedTraitInfo,
                                               RefA<GenericInstance> gi, Str* errorMsg);

//
// Implementation of a trait for a type:
//

struct TraitInstanceInfo {
    TraitInstanceInfo(Ref<Interface> trait) : trait(trait) {
    }
    TraitInstanceInfo(Ref<Interface> trait, const SubstitutionMap& types,
                      VecA<Ref<struct FunctionSignature>> methods)
        : trait(trait), types(types), methods(methods) {
    }

    Ref<Interface> trait;
    SubstitutionMap types;
    Vec<Ref<FunctionSignature>> methods;

    // Only relevant for impls of generic types:
    Vec<Ref<TemplateArgType>> templates;
    Ref<Type> type;
};
