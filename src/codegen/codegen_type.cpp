#include "common.h"

#include "codegen_private.h"

#include <llvm-c/DebugInfo.h>
#include <llvm-c/Target.h>

Ref<Type> CGEnv::fullyResolveType(Ref<Type> input) const {
    while (true) {
        auto curr = input->resolve();

        if (auto tv = curr->typeVariable())
            unreachable("we expect all type variables to be completely specified by this point!");

        curr = performTemplateSubstitution(curr);

        if (auto ki = curr->genericInstance())
            curr = ki->refine(templateSubs);

        if (!curr->equals(input)) {
            input = curr;
            continue;
        }

        break;
    }

    if (auto tit = dyn_cast<TraitInternalType>(input)) {
        auto ty = fullyResolveType(tit->type);
        auto resolvedTrait = replaceTypesFromMap(tit->iface, templateSubs);
        assert(ty);

        Maybe<Ref<TraitInstanceInfo>> maybeInst;
        for (const auto& ty : ty->traits) {
            if (ty->trait->equals(resolvedTrait)) {
                // Found our guy:
                maybeInst = ty;
                break;
            }
        }

        if (!maybeInst)
            unreachable("ICE: Bad book-keeping on traits!");
        auto inst = *maybeInst;

        // Exact equality doesn't work on the type map because we re-instantiate from :/
        // So we have to do a walk of the list:

        for (const auto& [from, to] : inst->types) {
            if (from->name == tit->name)
                return fullyResolveType(to);
        }

        unreachable("ICE: Bad book-keeping on traits!");
    }

    return input;
}

Ref<Type> CGEnv::performTemplateSubstitution(RefA<Type> in) const {
    if (auto tv = in->templateVariable()) {
        if (auto sub = lookup(templateSubs, tv))
            return *sub;

        unreachable("failed to find substitution in backend");
    }

    return in;
}

LLVMValueRef CGEnv::findTraitCall(RefA<Type> argType, RefA<Interface> trait, size_t methodId) {
    auto type = fullyResolveType(argType);

    if (auto gi = type->genericInstance()) {
        auto& genericsForTrait = genericTraitImpls[trait->name];
        if (auto methods = lookup(genericsForTrait, type->name))
            return methods->at(methodId);

        // We need to queue it and create the signatures as needed:
        auto traitInstance = lookupTraitInstance(argType, trait->name);
        assert(traitInstance && "no trait instance?");

        auto& methods = genericsForTrait[type->name];
        for (const auto& method : traitInstance->methods) {
            auto funcType =
                mapFunctionType(method->params, method->returnType, type, CallingConv::Argot);

            auto name = type->symbolName() + "#" + trait->name + "." + method->name();

            auto func = LLVMAddFunction(mod, name.c_str(), funcType);
            LLVMSetLinkage(func, LLVMExternalLinkage);

            populateFunctionDebugInformation(this, method->nameToken.loc(), func, name, name);

            methods.push_back(func);
        }

        SubstitutionMap subs;
        Str errorMsg;
        if (auto maybeSubmap = inferTraitImplTemplates(traitInstance, gi, &errorMsg)) {
            subs = *maybeSubmap;
        } else {
            auto fullError = format("typechecking bug! expected to be able to find interface "
                                    "instance but couldn't (%s)",
                                    errorMsg.c_str());
            unreachable(fullError.c_str());
        }

        traitTemplateInstances.push_back({ trait, gi->parent(), gi, methods, subs });

        return methods[methodId];
    } else {
        auto resolvedTrait = replaceTypesFromMap(trait, templateSubs);

        // This is going to pose problems for trait impls on templated things:
        auto implsForType = traitImpls[type];

        for (const auto& [trait, traitImpls] : implsForType) {
            if (trait->equals(resolvedTrait)) {
                return traitImpls[methodId];
            }
        }
    }

    return nullptr;
}

LLVMValueRef CGEnv::findTemplatedFunction(TemplateCallExpr* e, SubstitutionMap subs) {
    for (auto& s : subs)
        s.second = s.second->resolve();

    auto templateString = generateTemplateListString(e->sig->templates, subs);
    auto& result = functionTemplates[e->sig->name()][templateString];

    if (!result) {
        auto sig = replaceTypesFromMap(e->sig, templateSubs);
        auto literalSig = sig;

        // We may now have some variadic arguments here that we need to add to the concrete
        // definition of the function:
        for (const auto& [from, to] : subs) {
            if (auto cati = dyn_cast<ConstructorArgTraitIndex>(to)) {
                auto constructor = cati->record->constructors[cati->constructorIndex];
                for (const auto& constructorArgs : constructor.params)
                    literalSig->params.push_back(constructorArgs);
            }
        }

        result = createFunctionPrototype(literalSig, CallingConv::Argot, e->templates);

        FnTemplateInst inst = { sig, literalSig, result, subs };
        functionTemplateInstances[sig->name()].push_back(inst);
    }

    return result;
}

void populateFunctionDebugInformation(CGEnv* env, SourceLoc loc, LLVMValueRef function, StrA name,
                                      StrA mangledName) {
    auto file = env->lookupFileMetadataForLoc(loc);
    auto unit = env->lookupUnitMetadataForLoc(loc);

    auto functionType =
        LLVMDIBuilderCreateSubroutineType(env->debugBuilder, file, nullptr, 0, LLVMDIFlagZero);
    auto dbgFunction =
        LLVMDIBuilderCreateFunction(env->debugBuilder, unit, name.c_str(), name.size(),
                                    mangledName.c_str(), mangledName.length(), file, loc.line,
                                    functionType, false, true, loc.line, LLVMDIFlagZero, false);

    LLVMSetSubprogram(function, dbgFunction);
}

LLVMValueRef CGEnv::createFunctionPrototype(RefA<FunctionSignature> fn, CallingConv ty,
                                            const Vec<Ref<Type>> templates) {
    auto name = fn->name();

    Str mangledName = name;
    if (ty == CallingConv::Argot && fn->name() != "main")
        mangledName = "_a_" + name;

    if (fn->isTemplated())
        mangledName += generateTemplateListString(templates);

    auto type = mapFunctionType(fn->params, fn->returnType, nullptr, ty);
    auto func = LLVMAddFunction(mod, mangledName.c_str(), type);
    LLVMSetLinkage(func, LLVMExternalLinkage);

    // Only code generated by our compiler gets debug annotations:
    if (ty == CallingConv::Argot) {
        auto debugName = name;
        if (templates.size() > 0)
            debugName += generateTemplateListString(templates);

        populateFunctionDebugInformation(this, fn->nameToken.loc(), func, debugName, mangledName);
    }

    return func;
}

LLVMTypeRef CGEnv::mapType(Ref<Type> input) {
    input = fullyResolveType(input);

    // @TODO: This is extremely inefficient, we should look into figuring out if we're parameterized
    // by something that needs substitution (at this point in the pipeline, any template variables
    // need to be fully resolved):

    if (auto ki = input->genericInstance()) {
        auto other = ki->refine(templateSubs);
        if (!other->equals(input)) {
            return mapType(other);
        }
    }

    if (auto intTy = input->intType())
        return LLVMIntType(intTy->width);

    if (auto ft = dyn_cast<FloatType>(input)) {
        if (ft->width == 32)
            return LLVMFloatType();
        return LLVMDoubleType();
    }

    if (input->isBool())
        return LLVMIntType(1);

    if (input->isVoid())
        return LLVMVoidType();

    if (auto ptr = dyn_cast<PointerType>(input)) {
        if (ptr->interior->isVoid())
            return LLVMPointerType(LLVMIntType(8), 0);
        return LLVMPointerType(mapType(ptr->interior), 0);
    }

    if (auto v = input->variantType())
        return mapVariantType(v);

    if (auto r = input->recordType())
        return mapRecordType(r);

    if (auto f = dyn_cast<FunctionType>(input)) {
        auto retType = mapType(f->returnType);

        Vec<LLVMTypeRef> args;
        for (const auto& a : f->argTypes)
            args.push_back(mapType(a));

        LLVMTypeRef* argStart = args.size() > 0 ? &args[0] : nullptr;

        auto ft = LLVMFunctionType(retType, argStart, (unsigned) args.size(), false);
        return LLVMPointerType(ft, 0);
    }

    if (auto a = dyn_cast<ArrayType>(input)) {
        auto base = mapType(a->baseType);
        return LLVMArrayType(base, (unsigned) a->length);
    }

    unreachable("bad type?");
}

LLVMTypeRef CGEnv::mapFunctionType(const ArgumentList& params, RefA<Type> retType, RefA<Type> self,
                                   CallingConv type) {
    Vec<LLVMTypeRef> llvmParams;
    if (self)
        llvmParams.push_back(mapType(PointerType::of(self)));

    for (const auto& p : params) {
        if (isAggregate(p.type) || p.flags == VFlags::ref)
            llvmParams.push_back(mapType(PointerType::of(p.type)));
        else
            llvmParams.push_back(mapType(p.type));
    }

    auto returnsValue = type != CallingConv::Argot || !isAggregate(retType);

    auto ret = LLVMVoidType();
    if (returnsValue) {
        ret = mapType(retType);
    } else {
        llvmParams.push_back(mapType(PointerType::of(retType)));
    }

    auto args = llvmParams.size() ? &llvmParams.front() : nullptr;
    return LLVMFunctionType(ret, args, (unsigned)llvmParams.size(), false);
}

// Should an arbitrary type have a destructor?
static bool shouldCreateDestructor(RefA<Type> v) {
    if (auto rec = v->recordType()) {
        if (rec->explicitDestructorDecl)
            return true;

        for (const auto& field : rec->fields) {
            if (shouldCreateDestructor(field.type))
                return true;
        }
    }

    if (auto var = v->variantType()) {
        for (const auto& field : var->fields) {
            if (field.type && shouldCreateDestructor(field.type))
                return true;
        }
    }

    return false;
}

Ref<CGEnv::RecordInfo> CGEnv::lookupRecordInfo(Ref<Type> rec) {
    rec = fullyResolveType(rec);

    // We need to populate the record info by generating the type:
    ignore(mapType(rec));

    if (auto result = lookup(records, rec->symbolName()))
        return *result;

    // This isn't a record:
    return nullptr;
}

LLVMTypeRef CGEnv::mapRecordType(RefA<RecordBaseType> v) {
    auto symbolName = v->symbolName();

    if (auto ty = LLVMGetTypeByName(mod, symbolName.c_str()))
        return ty;

    auto ret = LLVMStructCreateNamed(ctx, symbolName.c_str());

    // Set up the LLVM struct for the record's layout:
    {
        std::vector<LLVMTypeRef> fields;

        // @TODO virtual method table is first element:

        // Base classes:
        for (const auto& base : v->bases)
            fields.push_back(mapType(base));

        for (const auto& f : v->fields)
            fields.push_back(mapType(f.type));

        LLVMTypeRef* fieldArgs = fields.empty() ? nullptr : &fields.front();
        LLVMStructSetBody(ret, fieldArgs, (unsigned)fields.size(), true);
    }

    auto si = std::make_shared<RecordInfo>(v);
    records[symbolName] = si;

    for (size_t i = 0; i < v->constructors.size(); ++i) {
        const auto& c = v->constructors[i];
        auto name = symbolName + ".self" + std::to_string(i);

        auto funcType = mapFunctionType(c.params, Type::getVoid(), v, CallingConv::Argot);
        auto func = LLVMAddFunction(mod, name.c_str(), funcType);
        LLVMSetLinkage(func, LLVMExternalLinkage);

        populateFunctionDebugInformation(this, c.selfToken.loc(), func, name, name);

        si->constructors.push_back(func);
    }

    for (const auto& m : v->methods) {
        auto funcType = mapFunctionType(m->params, m->returnType, v, CallingConv::Argot);
        auto name = v->symbolName() + "." + m->name();

        auto func = si->methods[m->nameToken.loc()] = LLVMAddFunction(mod, name.c_str(), funcType);
        LLVMSetLinkage(func, LLVMExternalLinkage);

        populateFunctionDebugInformation(this, m->nameToken.loc(), func, name, name);
    }

    if (shouldCreateDestructor(v)) {
        LLVMTypeRef params[] = { LLVMPointerType(ret, 0) };
        auto functionType = LLVMFunctionType(LLVMVoidType(), params, 1, false);
        Str name = v->symbolName() + ".~self";

        auto func = si->destructor = LLVMAddFunction(mod, name.c_str(), functionType);
        LLVMSetLinkage(func, LLVMExternalLinkage);
    }

    // If we're creating the struct definition for this generic instantiation then we need to
    // generate the methods for it too:
    if (auto gi = dyn_cast<RecordInstanceType>(v))
        recordTemplateInstances[gi->typedParent()].push_back({ gi->arguments, si });

    return ret;
}

LLVMTypeRef CGEnv::mapVariantType(RefA<VariantBaseType> v) {
    assert(v->fields.size());

    if (auto ty = LLVMGetTypeByName(mod, v->symbolName().c_str()))
        return ty;

    auto ret = LLVMStructCreateNamed(ctx, v->symbolName().c_str());
    auto layout = LLVMGetModuleDataLayout(mod);
    LLVMTypeRef mostAligned = nullptr, largest = nullptr;

    // Find the largest and most aligned types in the variant:
    for (const auto& f : v->fields) {
        if (f.type) {
            auto leftIsLarger = [&](LLVMTypeRef left, LLVMTypeRef right) {
                return LLVMStoreSizeOfType(layout, left) > LLVMStoreSizeOfType(layout, right);
            };
            auto leftIsMoreAligned = [&](LLVMTypeRef left, LLVMTypeRef right, bool allowEq) {
                if (allowEq)
                    return LLVMABIAlignmentOfType(layout, left) >=
                           LLVMABIAlignmentOfType(layout, right);
                return LLVMABIAlignmentOfType(layout, left) > LLVMABIAlignmentOfType(layout, right);
            };

            auto mapped = mapType(f.type);
            if (!largest || leftIsLarger(mapped, largest))
                largest = mapped;
            if (!mostAligned || leftIsMoreAligned(mapped, mostAligned, false) ||
                // Break ties in largest alignment with the largest size:
                (leftIsMoreAligned(mapped, mostAligned, true) && leftIsLarger(mapped, mostAligned)))
                mostAligned = mapped;
        }
    }

    // TODO is 64 bits a good size for the 'tag' on the union?
    // So using a 64-bit one is nice because it means we can do a memcmp for equivalence of
    // variant types without custom comparators.
    // Using 32-bit tags means if we're followed by a pointer (common), there's going to be 4
    // bytes of padding after the tag and before the pointer and we can't do comparions inside
    // that part.
    std::vector<LLVMTypeRef> fields = { LLVMIntType(64) };

    if (mostAligned) {
        assert(largest);
        fields.push_back(mostAligned);

        // If the most aligned type isn't the largest, add the remaining required storage as int8s
        // to the end:
        ptrdiff_t sizeDiff =
            LLVMStoreSizeOfType(layout, largest) - LLVMStoreSizeOfType(layout, mostAligned);
        if (sizeDiff > 0)
            fields.push_back(LLVMArrayType(LLVMIntType(8), (unsigned)sizeDiff));
    }

    LLVMStructSetBody(ret, &fields.front(), (unsigned)fields.size(), false);

    // @TODO: Destructors for variant types.

    return ret;
}

//
// Debug type information section:
//

// These aren't provided by the LLVM C library through some oversight. This is copied from:
// http://dwarfstd.org/doc/dwarf-2.0.0.pdf
enum DwarfTypes : unsigned {
    DT_address = 0x1,
    DT_boolean = 0x2,
    DT_complex_float = 0x3,
    DT_float = 0x4,
    DT_signed = 0x5,
    DT_signed_char = 0x6,
    DT_unsigned = 0x7,
    DT_unsigned_char = 0x8,
    DT_low_user = 0x80,
    DT_high_user = 0xFF,
};

static LLVMMetadataRef createVariantDebugLayout(CGEnv* env, RefA<VariantBaseType> v) {
    auto name = v->name;
    auto db = env->debugBuilder;
    auto loc = v->origin.loc();
    auto fileScope = env->lookupFileMetadataForLoc(loc);
    auto llvmType = env->mapType(v);
    auto layout = LLVMGetModuleDataLayout(env->mod);
    auto totalSizeBits = LLVMSizeOfTypeInBits(layout, llvmType);

    //
    // For variants for now we create an implicit 'tag' and a 'union' system
    // so we can see what the current state is and what its value is by looking
    // up in the second field's union.
    //

    Vec<LLVMMetadataRef> enumerationValues;
    Vec<Pair<Str, LLVMMetadataRef>> payloads;

    for (size_t i = 0; i < v->fields.size(); ++i) {
        const auto& f = v->fields[i];
        enumerationValues.push_back(LLVMDIBuilderCreateEnumerator(db, f.name().c_str(), i));

        if (f.type)
            payloads.push_back(std::make_pair(f.name(), env->mapDebugType(f.type)));
    }

    Vec<LLVMMetadataRef> fields;

    // Create enumeration field:
    {
        auto enumName = "E" + v->name;
        auto en = LLVMDIBuilderCreateEnumerationType(
            db, fileScope, enumName.c_str(), (unsigned)enumName.length(), fileScope, loc.line, 32,
            0, &enumerationValues[0], (unsigned)enumerationValues.size(),
            env->mapDebugType(Type::getI32()));

        auto tagName = Str("tag");
        auto d = LLVMDIBuilderCreateMemberType(db, fileScope, tagName.c_str(), tagName.length(),
                                               fileScope, loc.line, 32, 0, 0, LLVMDIFlagZero, en);
        fields.push_back(d);
    }

    // Create payload field if it exists:
    if (payloads.size()) {

        // This is a bit of a hack, but pretend our union's size is the 'rest' of
        // the struct, after the 32-bit tag:
        auto remainingBits = totalSizeBits - 32;
        auto offsetOfDataBits = LLVMOffsetOfElement(layout, llvmType, 1) * 8;

        LLVMMetadataRef payload = nullptr;
        if (payloads.size() == 1) {
            payload = payloads.front().second;
        } else {
            Vec<LLVMMetadataRef> unionMembers;
            for (const auto& [name, payload] : payloads) {
                auto d = LLVMDIBuilderCreateMemberType(db, fileScope, name.c_str(), name.length(),
                                                       fileScope, loc.line, remainingBits, 0, 0,
                                                       LLVMDIFlagZero, payload);
                unionMembers.push_back(d);
            }

            auto unionName = "U" + v->name;
            payload = LLVMDIBuilderCreateUnionType(
                db, fileScope, unionName.c_str(), unionName.length(), fileScope, loc.line,
                remainingBits, 0, LLVMDIFlagZero, &unionMembers[0], (unsigned)unionMembers.size(),
                0, name.c_str(), (unsigned)name.size());
        }

        auto tagName = Str("val");
        auto d = LLVMDIBuilderCreateMemberType(db, fileScope, tagName.c_str(), tagName.length(),
                                               fileScope, loc.line, remainingBits, 0, offsetOfDataBits,
                                               LLVMDIFlagZero, payload);
        fields.push_back(d);
    }

    return LLVMDIBuilderCreateStructType(db, fileScope, name.c_str(), name.length(), fileScope,
                                         loc.line, totalSizeBits, 0, LLVMDIFlagZero, nullptr,
                                         &fields[0], (unsigned)fields.size(), 0, nullptr,
                                         name.c_str(), (unsigned)name.length());
}

static LLVMMetadataRef createRecordDebugLayout(CGEnv* env, RefA<RecordBaseType> r) {
    auto layout = LLVMGetModuleDataLayout(env->mod);
    auto db = env->debugBuilder;
    auto name = r->name;

    auto loc = r->origin.loc();
    auto fileScope = env->lookupFileMetadataForLoc(loc);

    auto llvmType = env->mapType(r);
    auto numBytes = LLVMStoreSizeOfType(layout, llvmType);

    Vec<LLVMMetadataRef> elements;

    unsigned indexOffset = 0;
    for (const auto& base : r->bases) {
        auto fieldLlvmType = env->mapType(base);
        auto fieldDebugType = env->mapDebugType(base);

        auto align = LLVMABIAlignmentOfType(layout, fieldLlvmType);
        auto offset = LLVMOffsetOfElement(layout, llvmType, indexOffset++);
        auto size = LLVMStoreSizeOfType(layout, fieldLlvmType);

        auto fieldName = base->name;

        auto d = LLVMDIBuilderCreateMemberType(db, fileScope, fieldName.c_str(), fieldName.length(),
                                               fileScope, loc.line, size * 8, align * 8, offset * 8,
                                               LLVMDIFlagZero, fieldDebugType);
        elements.push_back(d);
    }

    for (auto [fieldIndex, f] : enumerate(r->fields)) {
        auto fieldName = f.name;
        auto fieldLlvmType = env->mapType(f.type);

        auto align = LLVMABIAlignmentOfType(layout, fieldLlvmType);
        auto offset = LLVMOffsetOfElement(layout, llvmType, (unsigned)(indexOffset + fieldIndex));
        auto size = LLVMStoreSizeOfType(layout, fieldLlvmType);

        auto fieldDebugType = env->mapDebugType(f.type);
        auto d = LLVMDIBuilderCreateMemberType(db, fileScope, fieldName.c_str(), fieldName.length(),
                                               fileScope, loc.line, size * 8, align * 8, offset * 8,
                                               LLVMDIFlagZero, fieldDebugType);
        elements.push_back(d);
    }

    LLVMMetadataRef* elemPtr = elements.empty() ? nullptr : &elements[0];

    return LLVMDIBuilderCreateStructType(db, fileScope, name.c_str(), name.length(), fileScope,
                                         loc.line, numBytes * 8, 0, LLVMDIFlagZero, nullptr,
                                         elemPtr, (unsigned)elements.size(), 0, nullptr,
                                         name.c_str(), (unsigned)name.length());
}

static LLVMMetadataRef createDebugFunctionType(CGEnv* env, RefA<FunctionType> in) {
    // @TODO: Correctly handle function pointer types...
    auto ptrBytes = uint64_t(LLVMPointerSize(LLVMGetModuleDataLayout(env->mod)));
    auto name = in->name;
    return LLVMDIBuilderCreatePointerType(env->debugBuilder, nullptr, ptrBytes * 8, 0, 0,
                                          name.c_str(), name.length());
}

LLVMMetadataRef createDebugTypeNoCache(CGEnv* env, Ref<Type> in) {
    auto name = in->name;
    auto db = env->debugBuilder;

    if (auto i = in->intType())
        return LLVMDIBuilderCreateBasicType(db, name.c_str(), name.length(), i->width, DT_signed);

    if (auto ft = dyn_cast<FloatType>(in))
        return LLVMDIBuilderCreateBasicType(db, name.c_str(), name.length(), ft->width, DT_float);

    if (in->isBool())
        return LLVMDIBuilderCreateBasicType(db, name.c_str(), name.length(), 8, DT_boolean);

    if (in->isVoid())
        return nullptr;

    if (auto p = in->pointerType()) {
        auto interior = env->mapDebugType(p->interior);
        auto ptrBytes = uint64_t(LLVMPointerSize(LLVMGetModuleDataLayout(env->mod)));
        return LLVMDIBuilderCreatePointerType(db, interior, ptrBytes * 8, 0, 0, name.c_str(),
                                              name.length());
    }

    if (auto a = dyn_cast<ArrayType>(in)) {
        auto interior = env->mapDebugType(a->baseType);
        auto layout = LLVMGetModuleDataLayout(env->mod);
        auto numBytes = LLVMStoreSizeOfType(layout, env->mapType(in));

        auto range = LLVMDIBuilderGetOrCreateSubrange(db, 0, a->length);
        auto subscripts = LLVMDIBuilderGetOrCreateArray(db, &range, 1);
        return LLVMDIBuilderCreateArrayType(db, numBytes * 8, 0, interior, &subscripts, 1);
    }

    if (auto ki = in->genericInstance()) {
        in = ki->refine(env->templateSubs);
        name = in->name;
    }

    if (auto r = in->recordType())
        return createRecordDebugLayout(env, r);

    if (auto v = in->variantType())
        return createVariantDebugLayout(env, v);

    if (auto f = dyn_cast<FunctionType>(in))
        return createDebugFunctionType(env, f);

    unreachable("bad type");
}

// @TODO This isn't thread safe:

static Map<Str, LLVMMetadataRef> debugTypeCache;
static Map<Str, LLVMMetadataRef> debugPlaceholders;

static Set<Str> debugTypeOngoingConstruction;
static int compositeTagNumber = 0;

static SourceLoc findLoc(RefA<Type> in) {
    if (auto rec = in->recordType())
        return rec->origin.loc();
    else if (auto var = in->variantType())
        return var->origin.loc();
    else if (auto p = in->pointerType())
        return findLoc(p->interior);

    unreachable("");
}

LLVMMetadataRef CGEnv::mapDebugType(Ref<Type> in) {
    in = fullyResolveType(in);

    auto name = in->name;
    auto& result = debugTypeCache[name];

    if (!result) {
        // Create a replacable placeholder type for this if we hit a cycle:
        if (lookup(debugTypeOngoingConstruction, name)) {
            if (auto placeholder = debugPlaceholders[name])
                return placeholder;

            auto loc = findLoc(in);
            auto fileScope = lookupFileMetadataForLoc(loc);
            unsigned tag = compositeTagNumber++;

            auto ret = LLVMDIBuilderCreateReplaceableCompositeType(
                debugBuilder, tag, name.c_str(), name.size(), fileScope, fileScope, loc.line,
                LLVMDWARFSourceLanguageC, 0, 0, LLVMDIFlagZero, name.c_str(), name.size());

            debugPlaceholders[name] = ret;
            return ret;
        } else {
            debugTypeOngoingConstruction.insert(name);
            result = createDebugTypeNoCache(this, in);
            debugTypeOngoingConstruction.erase(name);

            // If we've created a placeholder for ourselves, fill it up with the completed type we
            // just created:
            if (auto placeholder = lookup(debugPlaceholders, name)) {
                LLVMMetadataReplaceAllUsesWith(*placeholder, result);
                debugPlaceholders.erase(name);
            }
        }
    }

    return result;
}

static LLVMMetadataRef buildRoot() {
    auto text = Str("Argot TBAA");
    return LLVMValueAsMetadata(LLVMConstString(text.c_str(), (unsigned) text.size(), false));
}

LLVMMetadataRef CGEnv::mapTbaaType(Ref<Type> in) {
    static LLVMMetadataRef root = buildRoot();

    return LLVMMetadataRef();
}
