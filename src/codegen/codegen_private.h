#pragma once

#include "ast.h"
#include "trait.h"
#include "cmdline.h"

#include <llvm-c/Core.h>
#include <functional>

using namespace Ast;

extern cl::Opt<bool> disableDebugOpt;

enum class CallingConv {
    C,
    Argot,
};

// Global code generation state:
struct CGEnv {
    //
    // LLVM state required for building IR:
    //

    LLVMModuleRef mod = nullptr;
    LLVMContextRef ctx = nullptr;
    LLVMDIBuilderRef debugBuilder = nullptr;

    // Debug information:
    Stack<LLVMMetadataRef> debugScope;

    void generateModuleDebugInformation(const Vec<Ref<ModuleImplementation>>& mods);
    LLVMMetadataRef lookupFileMetadataForLoc(SourceLoc loc);
    LLVMMetadataRef lookupUnitMetadataForLoc(SourceLoc loc);

    // Mapping into the methods list for each LLVM type we're interested in:
    // TODO We shouldn't find through string but an unambiguous 'function' symbol id.
    struct RecordInfo {
        Ref<RecordBaseType> type;
        Vec<LLVMValueRef> constructors;
        Map<SourceLoc, LLVMValueRef> methods;
        LLVMValueRef destructor = nullptr;

        RecordInfo(RefA<RecordBaseType> type) : type(type) {
        }
    };
    Ref<RecordInfo> lookupRecordInfo(Ref<Type> rec);

    // We resolve global functions by their definition points:
    struct FunctionInfo {
        LLVMValueRef handle;
        CallingConv type;
    };
    Map<SourceLoc, FunctionInfo> functions;

    // Type mapping:
    Ref<Type> fullyResolveType(Ref<Type> input) const;
    LLVMTypeRef mapType(Ref<Type> input);
    LLVMMetadataRef mapDebugType(Ref<Type> in);
    LLVMMetadataRef mapTbaaType(Ref<Type> in);

    // @TODO: I'd like to move this into the CGFunction part but then we'd have types problems with
    // resolving types because half the type info would be in CGEnv and half would be in CGFunction.
    //
    // When generating templates, this is the mapping of the type variables into real types:
    SubstitutionMap templateSubs;

    // Aggregate types aren't passed around as values but stack-backed pointers:
    bool isAggregate(RefA<Type> type) const {
        auto resolved = fullyResolveType(type);
        return resolved->recordType() || resolved->variantType() || is<ArrayType>(resolved);
    }

    LLVMValueRef findTraitCall(RefA<Type> type, RefA<Interface> trait, size_t id);
    LLVMValueRef findTemplatedFunction(TemplateCallExpr* e, SubstitutionMap subs);

  private:
    Ref<Type> performTemplateSubstitution(RefA<Type> in) const;

    // File debug information:
    struct DebugInfo {
        LLVMMetadataRef file, unit;
    };
    Map<Str, DebugInfo> debugData;

    // Right now we're mapping by string type because the Ref<Type> pointers aren't unique for
    Map<Str, Ref<RecordInfo>> records;

    LLVMTypeRef mapVariantType(RefA<VariantBaseType> v);
    LLVMTypeRef mapRecordType(RefA<RecordBaseType> v);
    LLVMTypeRef mapFunctionType(const ArgumentList& params, RefA<Type> retType, RefA<Type> self,
                                CallingConv type);
    LLVMValueRef createFunctionPrototype(RefA<FunctionSignature> fn, CallingConv ty,
                                         const Vec<Ref<Type>> templates = {});

    Map<Ref<Type>, Map<Ref<Interface>, Vec<LLVMValueRef>>> traitImpls;

    //
    // This is used to later generate the template instantiations at the top level for the templates
    // we want to instantiate:
    //

    Map<Str, Map<Str, LLVMValueRef>> functionTemplates;

    friend struct CG;
    struct RecordTemplateInst {
        SubstitutionMap subMap;
        Ref<RecordInfo> si;
    };
    Map<Ref<RecordGeneric>, Vec<RecordTemplateInst>> recordTemplateInstances;

    struct FnTemplateInst {
        Ref<FunctionSignature> sig;
        Ref<FunctionSignature> literalSig;
        LLVMValueRef decl;
        SubstitutionMap subs;
    };
    Map<Str, Vec<FnTemplateInst>> functionTemplateInstances;

    struct ImplTemplateInst {
        Ref<Interface> trait;
        Ref<Generic> g;
        Ref<GenericInstance> type;
        Vec<LLVMValueRef> methods;

        // This is the substitution map in terms of the impl itself. That is, if we have some type
        // A<T,V> and an impl<I> for A<I, I> on it, the I will be in the dictionary, but the Ts and
        // Vs will not be.
        // This is because the code generated for the impl will not be in terms of T,V but in terms
        // of I.

        SubstitutionMap subs;
    };
    Vec<ImplTemplateInst> traitTemplateInstances;

    // Trait -> Type Instance -> [methods]
    Map<Str, Map<Str, Vec<LLVMValueRef>>> genericTraitImpls;
};

// Function-level codegen state:
struct CGFunction {
    CGFunction(CGEnv* env, const Block& body, LLVMValueRef function, SourceLoc loc,
               const ArgumentList& params, RefA<Type> returnType, RefA<Type> selfType,
               Maybe<std::function<void(CGFunction*)>> insertBefore,
               Maybe<std::function<void(CGFunction*)>> insertAfter);

    LLVMValueRef insertAlloca(StrA name, LLVMTypeRef type);
    LLVMValueRef allocaVariable(Ref<VarDecl> decl);

    // Basic block helpers:
    LLVMBasicBlockRef createBasicBlock(StrA name);
    bool builderBlockHasReturned() const;

    void removeFromScopeDestructing(RefA<Expr> expr);

    LLVMTypeRef mapType(RefA<Type> type) const {
        return env->mapType(type);
    }

    //
    // Builders:
    //

    LLVMValueRef codegenBlock(const Block& block, LLVMValueRef into = nullptr);

    void cgStmt(RefA<Stmt> stmt);

    LLVMValueRef cgExpr(RefA<Expr> e);
    LLVMValueRef cgExpr(Expr* e);

    void cgIntoExpr(RefA<Expr> expr, LLVMValueRef loc, bool forwarded = false);
    void cgIntoExpr(Expr* expr, LLVMValueRef loc, bool forwarded = false);

    void setCurrentDebugLocation(SourceLoc loc);
    void registerDebugVariable(StrA name, SourceLoc loc, RefA<Type> type, LLVMValueRef addr);

    CGEnv* env;
    LLVMContextRef llvmContext = nullptr;
    LLVMValueRef llvmHandle = nullptr;
    LLVMBuilderRef builder = nullptr;
    LLVMBasicBlockRef exitBlock = nullptr;

    LLVMValueRef returnPhi = nullptr;

    Vec<LLVMValueRef> arguments;

    struct LoopEarlyOutInfo {
        size_t scopeDepth;
        LLVMBasicBlockRef block;
    };

    Map<Ref<VarDecl>, LLVMValueRef> variables;
    Map<Str, LLVMValueRef> unwrappedSymbols;

  private:

    void addLoopInfo(SourceLoc loc, LLVMBasicBlockRef b, LLVMBasicBlockRef c);
    void removeLoopInfo(SourceLoc);

    // Loop early-out book-keeping:
    Map<SourceLoc, LoopEarlyOutInfo> breakInfo, continueInfo;

    //
    // State maintenance for destructors:
    //

    struct Destructable {
        Expr* e;
        LLVMValueRef val;
    };
    Vec<Destructable> immediatelyDestructing;

    struct Scope {
        Vec<Destructable> scopeDestructing;
    };
    Vec<Scope> _scopes;

    void pushScope();
    void popScope();

    void promoteToScopeDestructing(RefA<Expr> expr, LLVMValueRef alloca);
    void emitDestructorsForScope(const Scope& scope);
    void emitTemporaryDestructors();
    void emitAllDestructors();
};

void populateFunctionDebugInformation(CGEnv* env, SourceLoc loc, LLVMValueRef function, StrA name,
                                      StrA mangledName);
bool createDebugScopeForBlock(CGEnv* env, const Block& block);
Ref<TraitInstanceInfo> lookupTraitInstance(RefA<Type> type, StrA name);

// Platform-specific linker invocation:
void invokeLinker(StrA objectName);
