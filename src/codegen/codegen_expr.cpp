#include "common.h"

#include "codegen_private.h"

#include <llvm-c/DebugInfo.h>
#include <llvm-c/TargetMachine.h>

using namespace std;

// LLVM 'constant integer', used for GEPs
LLVMValueRef llvmCI32(size_t value) {
    return LLVMConstInt(LLVMIntType(32), value, false);
}
LLVMValueRef llvmCI64(size_t value) {
    return LLVMConstInt(LLVMIntType(64), value, false);
}

bool typeHasDestructor(RefA<Type> type, CGEnv* env) {
    // Get the LLVM type of this so we can see if it has a destructor or not:
    if (auto rec = env->lookupRecordInfo(type))
        return rec->destructor != nullptr;

    return false;
}

bool willBeDestructing(Expr* e, CGEnv* env) {
    return exprBeginsLifetime(e) && typeHasDestructor(e->type(), env);
}

// Given an expr, make it an l-value.
// This will return the memory backing whatever expression we're referring to is or
// back it on the stack if needed for temporaries.

LLVMValueRef cgLvalue(CGFunction* func, RefA<Expr> expr);
LLVMValueRef cgLvalue(CGFunction* func, Expr* expr) {
    auto builder = func->builder;

    if (auto var = dynamic_cast<VarRefExpr*>(expr)) {
        if (auto let = dyn_cast<LetVariableSymbol>(var->var)) {
            return func->variables[let->origin->decl];
        } else if (auto mv = dyn_cast<MemberVarSymbol>(var->var)) {
            auto self = LLVMGetParam(func->llvmHandle, 0);
            return LLVMBuildStructGEP(builder, self, (unsigned)mv->offset, "");
        } else if (auto arg = dyn_cast<FunArgSymbol>(var->var)) {
            return func->arguments[arg->argumentNumber];
        } else if (auto unwrap = dyn_cast<UnwrappedSymbol>(var->var)) {
            return func->unwrappedSymbols[unwrap->name];
        }
    }

    if (auto s = dynamic_cast<SelfExpr*>(expr)) {
        return LLVMGetParam(func->llvmHandle, 0);
    }

    if (auto si = dynamic_cast<RecordInitExpr*>(expr)) {
        auto ret = func->insertAlloca("", func->mapType(expr->type()));
        func->cgIntoExpr(si, ret);
        return ret;
    }

    if (auto ai = dynamic_cast<ArrayIndexExpr*>(expr)) {
        auto ptr = func->cgExpr(ai->arr);
        auto idx = func->cgExpr(ai->idx);

        auto ty = ai->arr->type();

       if (is<ArrayType>(ty)) {
            LLVMValueRef geps[] = { llvmCI32(0), idx, };
            return LLVMBuildInBoundsGEP(builder, ptr, geps, _countof(geps), "");
        } else {
            LLVMValueRef geps[] = { idx, };
            return LLVMBuildInBoundsGEP(builder, ptr, geps, _countof(geps), "");
        }
    }

    if (auto m = dynamic_cast<MemberExpr*>(expr)) {
        auto loc = func->cgExpr(m->lhs);

        Vec<LLVMValueRef> path = { llvmCI32(0) };
        for (auto p : m->sym->path)
            path.push_back(llvmCI32(p));

        path.push_back(llvmCI32(m->sym->offset));

        return LLVMBuildInBoundsGEP(builder, loc, &path[0], (unsigned) path.size(), "");
    }

    if (auto s = dynamic_cast<UnaryExpr*>(expr)) {
        if (s->op == TStar)
            return func->cgExpr(s->e);
    }

    unreachable("not implemented");
}

LLVMValueRef cgLvalue(CGFunction* func, RefA<Expr> expr) {
    return cgLvalue(func, expr.get());
}

void CGFunction::setCurrentDebugLocation(SourceLoc loc) {
    auto dloc =
        LLVMDIBuilderCreateDebugLocation(llvmContext, loc.line, 0, env->debugScope.top(), nullptr);
    LLVMSetCurrentDebugLocation(builder, LLVMMetadataAsValue(llvmContext, dloc));
}

// Create a basic block, placing it after the basic block we're currently operating in:

LLVMBasicBlockRef CGFunction::createBasicBlock(StrA name) {
    auto currentBlock = LLVMGetInsertBlock(builder);

    auto newBlock = LLVMInsertBasicBlock(currentBlock, name.c_str());
    LLVMMoveBasicBlockAfter(newBlock, currentBlock);

    return newBlock;
}

// Local free-standing functions that do the codegen for more complicated constructs:

namespace {

void emitIfExpr(CGFunction* func, IfExpr* e, LLVMValueRef loc) {
    auto builder = func->builder;
    auto env = func->env;

    auto cond = func->cgExpr(e->condition);
    auto thenBlock = func->createBasicBlock("if.then");
    auto doneBlock = func->createBasicBlock("if.done");

    LLVMBasicBlockRef elseBlock;
    if (e->elseBody)
        elseBlock = LLVMInsertBasicBlock(doneBlock, "if.else");
    else
        elseBlock = doneBlock;

    LLVMBuildCondBr(builder, cond, thenBlock, elseBlock);

    bool keepDone = false;

    // If we don't have an else body, we will always fall through:
    if (!e->elseBody)
        keepDone = true;

    auto buildBlock = [&](const Block& block, LLVMBasicBlockRef llvmBlock) {
        LLVMPositionBuilderAtEnd(builder, llvmBlock);
        auto newScope = createDebugScopeForBlock(env, block);

        func->codegenBlock(block, loc);

        if (!func->builderBlockHasReturned()) {
            if (block.closingBrace)
                func->setCurrentDebugLocation(*block.closingBrace);

            LLVMBuildBr(builder, doneBlock);
            keepDone = true;
        }

        if (newScope)
            env->debugScope.pop();
    };

    buildBlock(e->body, thenBlock);
    if (e->elseBody)
        buildBlock(*e->elseBody, elseBlock);

    if (keepDone) {
        LLVMPositionBuilderAtEnd(builder, doneBlock);
    } else {
        LLVMDeleteBasicBlock(doneBlock);
    }
}

void emitMatchExpr(CGFunction* func, MatchExpr* m, LLVMValueRef loc) {
    auto builder = func->builder;

    auto matchType = m->match->type()->variantType();
    assert(matchType && "matching on non-variant?");

    auto match = func->cgExpr(m->match);
    auto tagNumber = LLVMBuildLoad(builder, LLVMBuildStructGEP(builder, match, 0, ""), "");

    Vec<LLVMBasicBlockRef> blocksToDoneBlock;

    auto buildBlock = [&](const Block& b) {
        func->codegenBlock(b, loc);

        if (!func->builderBlockHasReturned())
            blocksToDoneBlock.push_back(LLVMGetInsertBlock(builder));
    };

    for (const auto& arm : m->arms) {
        auto elseBlock = func->createBasicBlock("match.else");
        auto thenBlock = func->createBasicBlock("match.then");

        auto buildJump = [&](RefA<VariantPattern> pat) {
            auto test = LLVMBuildICmp(builder, LLVMIntPredicate::LLVMIntEQ, tagNumber,
                                      llvmCI64(pat->id), "");
            LLVMBuildCondBr(builder, test, thenBlock, elseBlock);

            LLVMPositionBuilderAtEnd(builder, thenBlock);
        };

        if (auto label = dyn_cast<VariantPattern>(arm.pat)) {
            buildJump(label);

            if (auto unwrap = dyn_cast<UnwrapPattern>(label)) {
                auto payloadType = LLVMPointerType(func->mapType(unwrap->unwrapType), 0);
                auto payloadPtr = LLVMBuildStructGEP(builder, match, 1, "");
                auto typedPayload = LLVMBuildBitCast(builder, payloadPtr, payloadType, "");
                func->unwrappedSymbols[unwrap->unwrapName] = typedPayload;

                func->registerDebugVariable(unwrap->unwrapName, unwrap->loc, unwrap->unwrapType,
                                            typedPayload);
            }
        } else {
            unreachable("unexpected match pattern");
        }

        buildBlock(arm.block);

        LLVMPositionBuilderAtEnd(builder, elseBlock);
    }

    if (m->fallthrough) {
        buildBlock(*m->fallthrough);
    } else {
        // We don't want to build the unreachable unless we're actually expecting a value at the
        // end. It's perfectly valid to just destructure part of it and not care about the rest.
        if (loc)
            LLVMBuildUnreachable(builder);
        else
            blocksToDoneBlock.push_back(LLVMGetInsertBlock(builder));
    }

    auto doneBlock = func->createBasicBlock("match.done");
    for (const auto& b : blocksToDoneBlock) {
        LLVMPositionBuilderAtEnd(builder, b);
        LLVMBuildBr(builder, doneBlock);
    }

    LLVMPositionBuilderAtEnd(builder, doneBlock);
}

void emitCopyConstruction(CGFunction* func, ImplicitCopyExpr* copy, LLVMValueRef loc) {
    auto env = func->env;

    // If we resolve to having no nontrivial copy, then just emit into our location as
    // usual:
    auto ty = env->fullyResolveType(copy->type());
    if (!ty->nontrivialCopy()) {
        func->cgIntoExpr(copy->exprToCopy, loc);
        return;
    }

    // Otherwise, we know we have to do a complex copy here:
    if (auto rt = ty->recordType()) {
        assert(rt->copyConstructorIndex);
        auto si = env->lookupRecordInfo(ty);
        auto call = si->constructors[*rt->copyConstructorIndex];

        auto toCopy = func->cgExpr(copy->exprToCopy);

        std::vector<LLVMValueRef> args = { loc, toCopy };

        LLVMBuildCall(func->builder, call, &args.front(), (unsigned)args.size(), "");
    } else {
        unreachable("copy construction not yet implemented for this type");
    }
}

void emitRecordInitExpr(CGFunction* func, RecordInitExpr* e, LLVMValueRef loc) {
    auto builder = func->builder;
    LLVMBuildStore(builder, LLVMConstNull(func->mapType(e->type())), loc);

    unsigned i = 0;
    for (const auto& init : e->inits) {
        auto fieldPtr = LLVMBuildStructGEP(builder, loc, i++, "");
        func->cgIntoExpr(init, fieldPtr);
    }
}

// Build the arguments list for a function call. This handles reference parameters:

Vec<LLVMValueRef> buildCallArguments(CGFunction* func, const ArgumentList& argsInfo,
                                     Vec<Ref<Expr>>& args, LLVMValueRef self) {
    Vec<LLVMValueRef> ret;
    if (self)
        ret.push_back(self);

    for (size_t i = 0; i < args.size(); ++i) {
        // @TODO Right now this code assumes the non-explicit variadic arguments aren't by
        // reference. This should be a constant reference when that concept exists.
        bool ref = false;
        if (i < argsInfo.size()) {
            const auto& argInfo = argsInfo[i];
            ref = argInfo.flags == VFlags::ref;
        }
        auto& arg = args[i];

        if (ref)
            ret.push_back(cgLvalue(func, arg));
        else
            ret.push_back(func->cgExpr(arg));
    }

    return ret;
}

void emitConstructorExpr(CGFunction* func, ConstructorExpr* e, LLVMValueRef loc) {
    auto args = buildCallArguments(func, e->argsInfo, e->args, loc);

    auto recordData = func->env->lookupRecordInfo(e->type());
    auto f = recordData->constructors[e->constructorIndex];
    assert(f != nullptr);

    LLVMBuildCall(func->builder, f, &args.front(), (unsigned)args.size(), "");
}

void emitVariantInitExpr(CGFunction* func, VariantInitExpr* vi, LLVMValueRef loc) {
    auto builder = func->builder;
    auto llvmType = func->mapType(vi->type());

    // This zero-store is *VERY* important because otherwise LLVM can't optimize as much because it
    // can't merge uses with different initialization patterns since the struct gets kind of
    // partially initialized. Just zero-initialize it so it'record easier to reason about. More
    // instructions, more fast??
    LLVMBuildStore(builder, LLVMConstNull(llvmType), loc);

    auto tagLocation = LLVMBuildStructGEP(builder, loc, 0, "");
    LLVMBuildStore(builder, llvmCI64((unsigned)vi->fieldIndex), tagLocation);

    if (vi->args) {
        auto storeType = func->mapType(vi->args->type());

        auto valueLocation = LLVMBuildStructGEP(builder, loc, 1, "");
        auto casted = LLVMBuildBitCast(builder, valueLocation, LLVMPointerType(storeType, 0), "");

        func->cgIntoExpr(vi->args, casted);
    }
}

void emitLogicalOpExpr(CGFunction* func, LogicalOpExpr* e, LLVMValueRef loc) {
    auto builder = func->builder;

    auto lhsval = func->cgExpr(e->lhs);
    auto trueBlock = func->createBasicBlock("logical.true");
    auto falseBlock = func->createBasicBlock("logical.false");
    auto doneBlock = func->createBasicBlock("logical.complete");

    LLVMBuildCondBr(builder, lhsval, trueBlock, falseBlock);

    if (e->op == TPipePipe) { // Or operator:
        // true || anything -> true:
        LLVMPositionBuilderAtEnd(builder, trueBlock);
        LLVMBuildStore(builder, LLVMConstInt(func->mapType(e->type()), true, false), loc);
        LLVMBuildBr(builder, doneBlock);

        // false || a -> a
        LLVMPositionBuilderAtEnd(builder, falseBlock);
        func->cgIntoExpr(e->rhs, loc);
        LLVMBuildBr(builder, doneBlock);
    } else if (e->op == TAmpAmp) { // And operator:
        // true && a -> a:
        LLVMPositionBuilderAtEnd(builder, trueBlock);
        func->cgIntoExpr(e->rhs, loc);
        LLVMBuildBr(builder, doneBlock);

        // false && a -> false
        LLVMPositionBuilderAtEnd(builder, falseBlock);
        LLVMBuildStore(builder, LLVMConstInt(func->mapType(e->type()), false, false), loc);
        LLVMBuildBr(builder, doneBlock);
    }

    LLVMPositionBuilderAtEnd(builder, doneBlock);
}

LLVMValueRef lowerBuiltinTraits(CGFunction* func, InterfaceDispatchExpr* e) {
    auto ty = e->lhs->type();
    if (e->kind == MemberAccessKind::Arrow)
        ty = dyn_cast<PointerType>(ty)->interior;

    ty = func->env->fullyResolveType(ty);

    auto lhs = func->cgExpr(e->lhs);
    auto resolvedIf = replaceTypesFromMap(e->interface, func->env->templateSubs);

    LLVMValueRef (*creator)(LLVMBuilderRef, LLVMValueRef LHS, LLVMValueRef RHS, const char* Name) = nullptr;
    if (ty->intType()) {
        if (resolvedIf->name == "Sum<int>")
            creator = LLVMBuildAdd;
        else if (resolvedIf->name == "Prod<int>")
            creator = LLVMBuildMul;
        else if (resolvedIf->name == "Diff<int>")
            creator = LLVMBuildSub;
    } else if (auto f = dyn_cast<FloatType>(ty)) {
        if (resolvedIf->name == "Sum<float>")
            creator = LLVMBuildFAdd;
        else if (resolvedIf->name == "Prod<float>")
            creator = LLVMBuildFMul;
        else if (resolvedIf->name == "Diff<float>")
            creator = LLVMBuildFSub;
    }

    if (creator) {
        auto rhs = func->cgExpr(e->args[0]);
        return creator(func->builder, lhs, rhs, "");
    }

    return nullptr;
}

} // namespace

struct CGIntoExpr : ExprVisitor {
    CGIntoExpr(CGFunction* func, LLVMValueRef ptr)
        : ptr(ptr), func(func), env(func->env), builder(func->builder) {
    }

    // The location we are storing the result of this expression into:
    LLVMValueRef ptr;
    CGFunction* func;
    CGEnv* env;
    bool visited = true;
    LLVMBuilderRef builder;

    void accept(Expr*) override {
        visited = false;
    }

    void accept(IfExpr* e) override {
        emitIfExpr(func, e, ptr);
    }

    void accept(LogicalOpExpr* e) override {
        emitLogicalOpExpr(func, e, ptr);
    }

    void accept(MatchExpr* m) override {
        emitMatchExpr(func, m, ptr);
    }

    void accept(ImplicitCopyExpr* e) override {
        emitCopyConstruction(func, e, ptr);
    }

    void accept(ConstructorExpr* e) override {
        emitConstructorExpr(func, e, ptr);
    }

    void accept(RecordInitExpr* e) override {
        emitRecordInitExpr(func, e, ptr);
    }

    void accept(VariantInitExpr* vi) override {
        emitVariantInitExpr(func, vi, ptr);
    }

    void accept(CallExpr* e) override {
        auto fn = lookup(env->functions, e->sig->nameToken.loc());
        assert(fn);
        auto fi = *fn;

        auto args = buildCallArguments(func, e->sig->params, e->args, nullptr);

        bool aggregateRet = env->isAggregate(e->type());

        // Argot ABI says return values are final argument:
        if (fi.type == CallingConv::Argot && aggregateRet)
            args.push_back(ptr);

        func->setCurrentDebugLocation(e->loc);

        auto argptr = args.size() ? &args.front() : nullptr;
        auto result = LLVMBuildCall(builder, fi.handle, argptr, (unsigned)args.size(), "");

        if ((fi.type == CallingConv::C || !aggregateRet) && !e->type()->isVoid())
            LLVMBuildStore(builder, result, ptr);
    }

    void accept(TemplateCallExpr* e) override {
        auto templates = e->templates;
        for (auto& t : templates)
            t = env->fullyResolveType(t);

        SubstitutionMap subs;
        for (size_t i = 0; i < e->templates.size(); ++i)
            subs[e->sig->templates[i]] = templates[i];

        auto call = env->findTemplatedFunction(e, subs);
        auto args = buildCallArguments(func, e->sig->params, e->args, nullptr);

        bool aggregateRet = env->isAggregate(e->type());
        if (aggregateRet)
            args.push_back(ptr);

        auto argptr = args.size() ? &args.front() : nullptr;
        auto result = LLVMBuildCall(builder, call, argptr, (unsigned)args.size(), "");

        if (!aggregateRet && !e->type()->isVoid())
            LLVMBuildStore(builder, result, ptr);
    }

    void accept(InterfaceDispatchExpr* e) override {
        auto ty = e->lhs->type();
        if (e->kind == MemberAccessKind::Arrow)
            ty = dyn_cast<PointerType>(ty)->interior;

        ty = env->fullyResolveType(ty);

        if (auto call = env->findTraitCall(ty, e->interface, e->methodId)) {
            LLVMValueRef loc;
            if (env->isAggregate(ty)) {
                loc = func->cgExpr(e->lhs);
            } else {
                // We have to explicitly promote things we're doing trait dispatch on that aren't
                // aggregate types. I don't like this a lot but we make some real assumptions about
                // 'self's being lvalues.
                loc = func->insertAlloca("trait_self", env->mapType(ty));
                func->cgIntoExpr(e->lhs, loc);
            }

            const auto& sig = e->interface->methods[e->methodId];
            auto args = buildCallArguments(func, sig->params, e->args, loc);

            bool aggregateRet = env->isAggregate(e->type());
            if (aggregateRet)
                args.push_back(ptr);

            auto result = LLVMBuildCall(builder, call, &args.front(), (unsigned)args.size(), "");
            if (!aggregateRet && !e->type()->isVoid())
                LLVMBuildStore(builder, result, ptr);
        } else {
            if (auto result = lowerBuiltinTraits(func, e))
                LLVMBuildStore(builder, result, ptr);
            else
                unreachable("implement builtin trait calls");
        }
    }

    void accept(MemberCallExpr* e) override {
        Ref<Type> ty = e->lhs->type();

        LLVMValueRef loc;
        if (e->kind == MemberAccessKind::Arrow)
            ty = dyn_cast<PointerType>(ty)->interior;

        loc = func->cgExpr(e->lhs);

        // This code handles traversing base classes to find the correct 'self' pointer:
        if (e->path.size() > 0) {
            Vec<LLVMValueRef> gep = { llvmCI32(0) };
            for (const auto& p : e->path) {
                ty = ty->recordType()->bases[p];
                gep.push_back(llvmCI32(p));
            }

            loc = LLVMBuildInBoundsGEP(builder, loc, &gep[0], (unsigned)gep.size(), "");
        }

        auto si = env->lookupRecordInfo(ty);
        auto call = si->methods[e->sig->nameToken.loc()];

        auto args = buildCallArguments(func, e->sig->params, e->args, loc);

        bool aggregateRet = env->isAggregate(e->type());
        if (aggregateRet)
            args.push_back(ptr);

        auto result = LLVMBuildCall(builder, call, &args.front(), (unsigned)args.size(), "");
        if (!aggregateRet && !e->type()->isVoid())
            LLVMBuildStore(builder, result, ptr);
    }

    void accept(ArrayRepeatExpr* are) {
        for (size_t i = 0; i < are->length; ++i) {
            LLVMValueRef gep[] = { llvmCI32(0), llvmCI32(i) };
            auto loc = LLVMBuildInBoundsGEP(builder, ptr, gep, _countof(gep), "");
            func->cgIntoExpr(are->init, loc);
        }
    }

    void accept(ConstructFromVariadicExpr* e) override {
        auto recordData = env->lookupRecordInfo(e->type());

        // @TODO We shouldn't need to look through the template subs to figure out what constructor
        // we're calling. This should be part of a CGFunction.
        for (const auto& [from, to] : env->templateSubs) {
            if (auto cati = dyn_cast<ConstructorArgTraitIndex>(to)) {
                auto constructor = cati->record->constructors[cati->constructorIndex];

                // @TODO This code shouldn't need to query LLVM about what we told it but it's by
                // far the most convenient approach right now.
                auto fty = LLVMGetElementType(LLVMTypeOf(func->llvmHandle));
                auto argc = LLVMCountParamTypes(fty) - 1; // Don't count return type

                Vec<LLVMValueRef> args = { ptr };
                for (size_t i = func->arguments.size(); i < argc; ++i)
                    args.push_back(LLVMGetParam(func->llvmHandle, (unsigned)i));

                auto f = recordData->constructors[cati->constructorIndex];
                assert(f != nullptr);

                LLVMBuildCall(builder, f, &args.front(), (unsigned)args.size(), "");
                return;
            }
        }

        unreachable("");
    }
};

// Forwarded means we received this request through cgExpr and it is responsible for adding the
// result into the immediatelyDestructing list.

void CGFunction::cgIntoExpr(Expr* expr, LLVMValueRef loc, bool forwarded) {
    setCurrentDebugLocation(expr->loc);

    CGIntoExpr into(this, loc);
    expr->visit(&into);

    // If we don't treat this specially, then just generate the value and do an explicit store:
    if (!into.visited) {
        auto val = cgExpr(expr);

        // Aggregate types are referred to by their pointer value on the stack
        // so do a load/store rather than just a store:
        if (env->isAggregate(expr->type()))
            val = LLVMBuildLoad(builder, val, "");

        LLVMBuildStore(builder, val, loc);
    }

    if (!forwarded && willBeDestructing(expr, env))
        immediatelyDestructing.push_back({ expr, loc });
}

void CGFunction::cgIntoExpr(RefA<Expr> expr, LLVMValueRef loc, bool forwarded) {
    cgIntoExpr(expr.get(), loc, forwarded);
}

struct CGExpr : ExprVisitor {
    CGExpr(CGFunction* func) : builder(func->builder), env(func->env), func(func) {
    }

    LLVMValueRef ret = nullptr;
    LLVMBuilderRef builder;
    CGEnv* env;
    CGFunction* func;

    void accept(IntLiteralExpr* e) override {
        ret = LLVMConstInt(env->mapType(e->type()), e->val, false);
    }

    void accept(FloatLiteralExpr* e) override {
        ret = LLVMConstReal(env->mapType(e->type()), e->val);
    }

    void accept(BoolLiteralExpr* e) override {
        ret = LLVMConstInt(env->mapType(e->type()), e->val, false);
    }

    void accept(StringLiteralExpr* e) override {
        auto length = unsigned(e->text.size());

        auto global = LLVMAddGlobal(
            env->mod, LLVMArrayType(env->mapType(Type::getI8()), length + 1), "string");
        LLVMSetLinkage(global, LLVMInternalLinkage);
        LLVMSetInitializer(global, LLVMConstString(e->text.c_str(), length, false));
        LLVMSetGlobalConstant(global, true);

        // Step through both the pointer type and then the array type to get a
        // pointer to the first element:
        LLVMValueRef gep[] = {
            llvmCI32(0),
            llvmCI32(0),
        };
        ret = LLVMBuildInBoundsGEP(builder, global, gep, 2, "");
    }

    void buildIntegerArithmetic(InfixExpr* e) {
        auto lhs = func->cgExpr(e->lhs), rhs = func->cgExpr(e->rhs);

        switch (e->op) {
            case TPlus: ret = LLVMBuildAdd(builder, lhs, rhs, ""); break;
            case TMinus: ret = LLVMBuildSub(builder, lhs, rhs, ""); break;
            case TStar: ret = LLVMBuildMul(builder, lhs, rhs, ""); break;
            // TODO handle signed/unsigned differences here:
            case TSlash: ret = LLVMBuildUDiv(builder, lhs, rhs, ""); break;

            case TLAngle: ret = LLVMBuildICmp(builder, LLVMIntSLT, lhs, rhs, ""); break;
            case TLEq: ret = LLVMBuildICmp(builder, LLVMIntSLE, lhs, rhs, ""); break;
            case TRAngle: ret = LLVMBuildICmp(builder, LLVMIntSGT, lhs, rhs, ""); break;
            case TREq: ret = LLVMBuildICmp(builder, LLVMIntSGE, lhs, rhs, ""); break;
            case TEqEq: ret = LLVMBuildICmp(builder, LLVMIntEQ, lhs, rhs, ""); break;
            case TBangEq: ret = LLVMBuildICmp(builder, LLVMIntNE, lhs, rhs, ""); break;

            case TAmp: ret = LLVMBuildAnd(builder, lhs, rhs, ""); break;
            case TPipe: ret = LLVMBuildOr(builder, lhs, rhs, ""); break;
            case TCaret: ret = LLVMBuildXor(builder, lhs, rhs, ""); break;

            // TODO follow up on this part for signed/unsigned:
            case TMod: ret = LLVMBuildURem(builder, lhs, rhs, ""); break;

            default: unreachable("bad infix operator");
        }
    }

    void buildFloatArithmetic(InfixExpr* e) {
        auto lhs = func->cgExpr(e->lhs), rhs = func->cgExpr(e->rhs);

        switch (e->op) {
            case TPlus: ret = LLVMBuildFAdd(builder, lhs, rhs, ""); break;
            case TMinus: ret = LLVMBuildFSub(builder, lhs, rhs, ""); break;
            case TStar: ret = LLVMBuildFMul(builder, lhs, rhs, ""); break;
            case TSlash: ret = LLVMBuildFDiv(builder, lhs, rhs, ""); break;

            case TEqEq: ret = LLVMBuildFCmp(builder, LLVMRealOEQ, lhs, rhs, ""); break;
            case TBangEq: ret = LLVMBuildFCmp(builder, LLVMRealONE, lhs, rhs, ""); break;
            case TLAngle: ret = LLVMBuildFCmp(builder, LLVMRealOLT, lhs, rhs, ""); break;
            case TLEq: ret = LLVMBuildFCmp(builder, LLVMRealOLE, lhs, rhs, ""); break;
            case TRAngle: ret = LLVMBuildFCmp(builder, LLVMRealOGT, lhs, rhs, ""); break;
            case TREq: ret = LLVMBuildFCmp(builder, LLVMRealOGE, lhs, rhs, ""); break;

            default: unreachable("bad infix operator");
        }
    }

    void accept(ShiftExpr* e) override {
        auto lhs = func->cgExpr(e->lhs), rhs = func->cgExpr(e->rhs);

        if (e->st == ShiftType::left)
            ret = LLVMBuildShl(builder, lhs, rhs, "");
        else
            // TODO follow up on this part for signed/unsigned:
            ret = LLVMBuildAShr(builder, lhs, rhs, "");
    }

    LLVMValueRef getOrCreateMemcmp() {
        static LLVMValueRef ret;

        if (!ret) {
            auto i8ptr = LLVMPointerType(LLVMInt8Type(), 0);
            LLVMTypeRef argsTy[] = { i8ptr, i8ptr, LLVMInt64Type() };
            auto ty = LLVMFunctionType(LLVMInt32Type(), argsTy, _countof(argsTy), false);
            ret = LLVMAddFunction(env->mod, "memcmp", ty);
        }

        return ret;
    }

    // Eventually we need to make comparisons type trait overrides:
    void accept(EqTestExpr* e) override {
        auto lhs = func->cgExpr(e->lhs), rhs = func->cgExpr(e->rhs);

        if (is<PointerType>(e->lhs->type())) {
            // Pointer types get an integer-style comparison:
            auto lhs = func->cgExpr(e->lhs);
            auto rhs = func->cgExpr(e->rhs);

            lhs = LLVMBuildPtrToInt(builder, lhs, LLVMInt64Type(), "");
            rhs = LLVMBuildPtrToInt(builder, rhs, LLVMInt64Type(), "");

            auto op = e->equiv ? LLVMIntEQ : LLVMIntNE;
            ret = LLVMBuildICmp(builder, op, lhs, rhs, "");
        } else {
            // Make sure this is only being used for aggregate comparison:
            assert(env->isAggregate(e->lhs->type()));
            auto i8ptr = LLVMPointerType(LLVMInt8Type(), 0);

            lhs = LLVMBuildBitCast(builder, lhs, i8ptr, "");
            rhs = LLVMBuildBitCast(builder, rhs, i8ptr, "");

            auto memcmp = getOrCreateMemcmp();
            auto size = LLVMSizeOf(env->mapType(e->lhs->type()));

            LLVMValueRef args[] = { lhs, rhs, size };
            auto cmp = LLVMBuildCall(builder, memcmp, args, _countof(args), "");

            auto op = e->equiv ? LLVMIntEQ : LLVMIntNE;
            ret = LLVMBuildICmp(builder, op, cmp, llvmCI32(0), "");
        }
    }

    void accept(InfixExpr* e) override {
        auto componentType = e->lhs->type()->resolve();

        if (componentType->intType())
            buildIntegerArithmetic(e);
        else if (is<FloatType>(componentType))
            buildFloatArithmetic(e);
        else
            unreachable("");
    }

    void accept(LogicalOpExpr* e) override {
        LLVMValueRef loc = func->insertAlloca("logical_op_result", env->mapType(e->type()));
        emitLogicalOpExpr(func, e, loc);
        ret = LLVMBuildLoad(builder, loc, "");
    }

    void accept(UnaryExpr* vi) override {
        // This needs to be enormously improved and merged into the infix expr code:

        // I'm envisioning a system where we have a function that takes two expressions and 'unites'
        // them through a common operator system and then we can do an assign below as needed for
        // the  assignment code:

        if (vi->op == TMinus) {
            auto e = func->cgExpr(vi->e);
            if (is<FloatType>(vi->type()))
                ret = LLVMBuildFSub(builder, LLVMConstNull(LLVMTypeOf(e)), e, "");
            else
                ret = LLVMBuildSub(builder, LLVMConstNull(LLVMTypeOf(e)), e, "");
        } else if (vi->op == TStar) {
            ret = func->cgExpr(vi->e);

            if (!env->isAggregate(vi->type()))
                ret = LLVMBuildLoad(builder, ret, "");
        } else if (vi->op == TAmp) {
            ret = cgLvalue(func, vi->e);
        } else if (vi->op == TBang) {
            ret = LLVMBuildNot(builder, func->cgExpr(vi->e), "");
        } else {
            unreachable("unimplemented unary op");
        }
    }

    void accept(VarRefExpr* e) override {
        if (auto arg = dyn_cast<FunArgSymbol>(e->var)) {
            ret = func->arguments[arg->argumentNumber];
            if (!env->isAggregate(e->type()))
                ret = LLVMBuildLoad(builder, func->arguments[arg->argumentNumber], "");
        } else {
            ret = cgLvalue(func, e);

            if (!env->isAggregate(e->type()))
                ret = LLVMBuildLoad(builder, ret, "");
        }
    }

    void accept(IfExpr* e) override {
        bool buildLoad = false;
        if (!e->type()->isVoid()) {
            ret = func->insertAlloca("if_result", env->mapType(e->type()));
            buildLoad = !env->isAggregate(e->type());
        }

        emitIfExpr(func, e, ret);

        if (buildLoad)
            ret = LLVMBuildLoad(builder, ret, "");
    }

    void accept(MatchExpr* m) override {
        auto ty = m->type();
        if (!ty->isVoid())
            ret = func->insertAlloca("match_value", env->mapType(ty));

        emitMatchExpr(func, m, ret);

        if (ret && !env->isAggregate(ty))
            ret = LLVMBuildLoad(builder, ret, "");
    }

    void accept(ImplicitCopyExpr* e) override {
        auto ty = env->fullyResolveType(e->type());
        if (env->isAggregate(ty)) {
            ret = func->insertAlloca("implicit_copy", env->mapType(e->type()));
            if (ty->nontrivialCopy()) {
                emitCopyConstruction(func, e, ret);
            } else {
                // Emit a load/store which is basically memcpy:
                auto val = func->cgExpr(e->exprToCopy);
                LLVMBuildStore(builder, LLVMBuildLoad(builder, val, ""), ret);
            }
        } else {
            ret = func->cgExpr(e->exprToCopy);
        }
    }

    void accept(CastExpr* m) override {
        auto originType = m->e->type();
        auto resultType = m->type();

        auto e = func->cgExpr(m->e);

        auto llvmResultType = env->mapType(resultType);

        // @TODO signedness
        // These calls are assuming we're using signed integers:

        if (is<IntType>(originType) && is<IntType>(resultType)) {
            auto oi = dyn_cast<IntType>(originType);
            auto ri = dyn_cast<IntType>(resultType);

            assert(oi->width != ri->width);
            if (oi->width > ri->width)
                ret = LLVMBuildTrunc(builder, e, llvmResultType, "");
            else
                ret = LLVMBuildSExt(builder, e, llvmResultType, "");
        } else if (is<IntType>(originType) && is<FloatType>(resultType))
            ret = LLVMBuildSIToFP(builder, e, llvmResultType, "");
        else if (is<FloatType>(originType) && is<IntType>(resultType))
            ret = LLVMBuildFPToSI(builder, e, llvmResultType, "");
        else if (originType->pointerType() && is<IntType>(resultType))
            ret = LLVMBuildPtrToInt(builder, e, llvmResultType, "");
        else
            ret = LLVMBuildBitCast(builder, e, llvmResultType, "");
    }

    void accept(SizeofExpr* e) override {
        auto ty = env->mapType(e->sizeofType);
        auto layout = LLVMGetModuleDataLayout(env->mod);
        ret = LLVMConstInt(env->mapType(e->type()), LLVMStoreSizeOfType(layout, ty), false);
    }

    void accept(AssignmentExpr* ai) override {
        auto op = ai->op.type;
        auto lhs = cgLvalue(func, ai->lhs);

        ret = func->cgExpr(ai->rhs);

        auto storedValue = ret;
        if (env->isAggregate(env->fullyResolveType(ai->type())))
            storedValue = LLVMBuildLoad(builder, ret, "");

        LLVMBuildStore(builder, storedValue, lhs);
    }

    void accept(ConstructIntoExpr* e) override {
        auto loc = func->cgExpr(e->mem);
        loc = LLVMBuildBitCast(builder, loc, env->mapType(e->type()), "");
        func->cgIntoExpr(e->init, loc);
        ret = loc;
    }

    //
    // Aggregate temporaries:
    //

    void accept(RecordInitExpr* si) override {
        auto recordType = env->mapType(si->type());
        ret = func->insertAlloca("record_tmp", recordType);
        emitRecordInitExpr(func, si, ret);
    }

    void accept(ConstructorExpr* e) override {
        auto recordType = env->mapType(e->type());
        ret = func->insertAlloca("constructed_tmp", recordType);
        emitConstructorExpr(func, e, ret);
    }

    void accept(VariantInitExpr* vi) override {
        auto llvmType = env->mapType(vi->type());
        ret = func->insertAlloca("variant_tmp", llvmType);
        emitVariantInitExpr(func, vi, ret);
    }

    void accept(SelfExpr* vi) override {
        if (env->isAggregate(vi->type()))
            ret = LLVMGetParam(func->llvmHandle, 0);
        else
            ret = LLVMBuildLoad(builder, LLVMGetParam(func->llvmHandle, 0), "");
    }

    void accept(ArrayIndexExpr* e) override {
        ret = cgLvalue(func, e);

        if (!env->isAggregate(e->type()))
            ret = LLVMBuildLoad(builder, ret, "");
    }

    void accept(MemberExpr* e) override {
        LLVMValueRef addr = func->cgExpr(e->lhs);

        assert(e->sym->path.empty());

        ret = LLVMBuildStructGEP(builder, addr, (unsigned)e->sym->offset, "");

        if (!env->isAggregate(e->type()))
            ret = LLVMBuildLoad(builder, ret, "");
    }

    void forwardToCgInto(Expr* e) {
        auto ty = e->type();
        if (!ty->isVoid())
            ret = func->insertAlloca("tmp", env->mapType(ty));

        func->cgIntoExpr(e, ret, true);

        if (ret && !env->isAggregate(ty))
            ret = LLVMBuildLoad(builder, ret, "");
    }

    void accept(FunctionRefExpr* e) override {
        auto fn = lookup(env->functions, e->sig->nameToken.loc());
        assert(fn);
        ret = fn->handle;
    }

    void accept(CallExpr* e) override {
        forwardToCgInto(e);
    }
    void accept(TemplateCallExpr* e) override {
        forwardToCgInto(e);
    }
    void accept(InterfaceDispatchExpr* e) override {
        if (auto result = lowerBuiltinTraits(func, e))
            ret = result;
        else
            forwardToCgInto(e);
    }
    void accept(MemberCallExpr* e) override {
        forwardToCgInto(e);
    }
};

LLVMValueRef CGFunction::cgExpr(Expr* e) {
    setCurrentDebugLocation(e->loc);

    CGExpr cg(this);
    e->visit(&cg);

    if (willBeDestructing(e, env))
        immediatelyDestructing.push_back({ e, cg.ret });

    return cg.ret;
}

LLVMValueRef CGFunction::cgExpr(RefA<Expr> e) {
    return cgExpr(e.get());
}
