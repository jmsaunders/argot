#include "common.h"

#include "codegen_private.h"

#include <llvm-c/Analysis.h>
#include <llvm-c/DebugInfo.h>
#include <llvm-c/TargetMachine.h>
#include <llvm-c/Transforms/PassManagerBuilder.h>

cl::Opt<bool> disableDebugOpt("no-debug");
cl::Opt<int> optLevelOpt("opt", 0);

cl::Opt<bool> validateLlvmOpt("validate-llvm");
cl::Opt<bool> emitLlvmOpt("emit-llvm");
cl::Opt<bool> emitAsmOpt("emit-asm");

const auto producerString = "argotc";

void CGFunction::addLoopInfo(SourceLoc s, LLVMBasicBlockRef b, LLVMBasicBlockRef c) {
    LoopEarlyOutInfo eoib = { _scopes.size(), b };
    breakInfo[s] = eoib;

    LoopEarlyOutInfo eoic = { _scopes.size(), c };
    continueInfo[s] = eoic;
}

void CGFunction::removeLoopInfo(SourceLoc s) {
    breakInfo.erase(breakInfo.find(s));
    continueInfo.erase(continueInfo.find(s));
}

void CGFunction::emitDestructorsForScope(const Scope& scope) {
    for (auto it = scope.scopeDestructing.rbegin(); it != scope.scopeDestructing.rend(); ++it) {
        auto d = *it;

        auto si = env->lookupRecordInfo(d.e->type());

        LLVMValueRef args[] = { d.val };
        LLVMBuildCall(builder, si->destructor, args, 1, "");
    }
}

void CGFunction::emitAllDestructors() {
    for (const auto& s : _scopes)
        emitDestructorsForScope(s);
}

void CGFunction::pushScope() {
    _scopes.push_back({});
}

void CGFunction::popScope() {
    emitDestructorsForScope(_scopes.back());
    _scopes.erase(_scopes.end() - 1);
}

bool createDebugScopeForBlock(CGEnv* env, const Block& block) {
    if (block.openingBrace) {
        auto file = env->lookupFileMetadataForLoc(*block.openingBrace);
        auto newScope = LLVMDIBuilderCreateLexicalBlock(env->debugBuilder, env->debugScope.top(),
                                                        file, block.openingBrace->line, 0);

        env->debugScope.push(newScope);
        return true;
    }

    return false;
}

void CGFunction::emitTemporaryDestructors() {
    for (auto it = immediatelyDestructing.rbegin(); it != immediatelyDestructing.rend();
         ++it) {
        auto d = *it;
        auto si = env->lookupRecordInfo(d.e->type());

        LLVMValueRef args[] = { d.val };
        LLVMBuildCall(builder, si->destructor, args, 1, "");
    }

    immediatelyDestructing.clear();
}

// This object is being consumed by something, don't destruct it:
void CGFunction::removeFromScopeDestructing(RefA<Expr> expr) {
    for (auto it = immediatelyDestructing.begin(); it != immediatelyDestructing.end();
         ++it) {
        if (expr.get() == it->e) {
            immediatelyDestructing.erase(it);
            break;
        }
    }
}

LLVMMetadataRef CGEnv::lookupFileMetadataForLoc(SourceLoc loc) {
    if (auto data = lookup(debugData, Str(loc.file)))
        return data->file;

    unreachable("could not find metadata");
}

LLVMMetadataRef CGEnv::lookupUnitMetadataForLoc(SourceLoc loc) {
    if (auto data = lookup(debugData, Str(loc.file)))
        return data->unit;

    unreachable("could not find metadata");
}

void CGFunction::registerDebugVariable(StrA name, SourceLoc loc, RefA<Type> type,
                                       LLVMValueRef addr) {
    auto db = env->debugBuilder;

    auto debugType = env->mapDebugType(type);
    assert(debugType);

    auto fileScope = env->lookupFileMetadataForLoc(loc);

    auto info =
        LLVMDIBuilderCreateAutoVariable(db, env->debugScope.top(), name.c_str(), name.length(),
                                        fileScope, loc.line, debugType, false, LLVMDIFlagZero, 0);

    auto dbgloc =
        LLVMDIBuilderCreateDebugLocation(llvmContext, loc.line, 0, env->debugScope.top(), nullptr);

    auto currentBlock = LLVMGetInsertBlock(builder);
    auto expr = LLVMDIBuilderCreateExpression(db, nullptr, 0);
    LLVMDIBuilderInsertDeclareAtEnd(db, addr, info, expr, dbgloc, currentBlock);
}

CGFunction::CGFunction(struct CGEnv* env, const Block& body, LLVMValueRef function, SourceLoc loc,
                       const ArgumentList& params, RefA<Type> returnType, RefA<Type> selfType,
                       Maybe<std::function<void(CGFunction*)>> insertBefore,
                       Maybe<std::function<void(CGFunction*)>> insertAfter)
    : env(env), llvmContext(env->ctx), llvmHandle(function),
      builder(LLVMCreateBuilderInContext(llvmContext)) {
    assert(env->debugScope.empty());
    env->debugScope.push(LLVMGetSubprogram(function));

    auto entryBlock = LLVMAppendBasicBlock(function, "entry");
    auto codeBlock = LLVMAppendBasicBlock(function, "code");

    // Emit the exit block:
    exitBlock = LLVMAppendBasicBlock(function, "exit");
    LLVMPositionBuilderAtEnd(builder, exitBlock);

    bool aggregateReturn = env->isAggregate(returnType);
    if (!aggregateReturn && !returnType->isVoid())
        returnPhi = LLVMBuildPhi(builder, env->mapType(returnType), "");

    if (insertAfter)
        (*insertAfter)(this);

    if (body.closingBrace)
        setCurrentDebugLocation(*body.closingBrace);

    if (aggregateReturn)
        LLVMBuildRetVoid(builder);
    else
        LLVMBuildRet(builder, returnPhi);

    // Start the function code:
    LLVMPositionBuilderAtEnd(builder, codeBlock);

    {
        setCurrentDebugLocation(loc);

        if (selfType) {
            auto self = LLVMGetParam(function, 0);
            arguments.push_back(self);

            registerDebugVariable("self", loc, selfType, self);
        }

        unsigned offset = selfType ? 1 : 0;
        for (unsigned i = 0; i < params.size(); ++i) {
            const auto& p = params[i];
            auto argType = env->fullyResolveType(p.type);

            LLVMValueRef stackPos = nullptr;
            auto param = LLVMGetParam(function, (unsigned)i + offset);
            if (p.flags == VFlags::none && !env->isAggregate(argType)) {
                // Standard copy-in arguments:
                stackPos = insertAlloca(p.name, env->mapType(argType));
                LLVMBuildStore(builder, param, stackPos);
            } else {
                stackPos = param;
            }

            arguments.push_back(stackPos);

            //
            // This code is only kind of correct.
            //
            // On x86/x64 a struct passed as an argument that is greater than 8 bytes is passed
            // as a pointer and allocated on the caller's stack frame. As such, arguments
            // populated through LLVMDIBuilderCreateParameterVariable need to know whether
            // they're greater than or less than 8 bytes and need to create another layer of
            // indirection on our stack to account for what the debugger is expecting.
            //
            // I get around this by lying about it being an argument to the function for now,
            // and just say it's a regular local variable. I don't want to have to worry about
            // calling convention stuff right now, since it'll vary by the platform we're
            // lowering to and I don't think we gain much by doing it right.
            //

            registerDebugVariable(p.name, loc, argType, stackPos);
        }
    }

    if (insertBefore)
        (*insertBefore)(this);

    // Generate the body of the function call:
    if (returnPhi) {
        LLVMValueRef vals[] = { codegenBlock(body) };
        LLVMBasicBlockRef blocks[] = { LLVMGetInsertBlock(builder) };

        if (!builderBlockHasReturned())
            LLVMAddIncoming(returnPhi, vals, blocks, 1);
    } else {
        LLVMValueRef retPtr = LLVMGetLastParam(function);
        codegenBlock(body, retPtr);
    }

    if (!builderBlockHasReturned())
        LLVMBuildBr(builder, exitBlock);

    setCurrentDebugLocation(loc);
    env->debugScope.pop();

    // Finally, add the jump from the variable decl block into the code
    // starting block:
    LLVMPositionBuilderAtEnd(builder, entryBlock);
    LLVMBuildBr(builder, codeBlock);
}

LLVMValueRef CGFunction::codegenBlock(const Block& block, LLVMValueRef into) {
    pushScope();

    for (const auto& s : block.stmts)
        cgStmt(s);

    LLVMValueRef ret = nullptr;
    if (block.expr) {
        if (into)
            cgIntoExpr(block.expr, into);
        else
            ret = cgExpr(block.expr);

        // Values yielded from scope are never immediately destructing:
        removeFromScopeDestructing(block.expr);
    }

    // Destructor calls' debug location should be the closing brace for the scope:
    if (block.closingBrace)
        setCurrentDebugLocation(*block.closingBrace);

    emitTemporaryDestructors();

    popScope();

    return ret;
}

bool CGFunction::builderBlockHasReturned() const {
    return LLVMGetBasicBlockTerminator(LLVMGetInsertBlock(builder));
}

LLVMValueRef CGFunction::insertAlloca(StrA name, LLVMTypeRef type) {
    auto currDebug = LLVMGetCurrentDebugLocation(builder);
    LLVMSetCurrentDebugLocation(builder, nullptr);

    auto currentBlock = LLVMGetInsertBlock(builder);
    LLVMPositionBuilderAtEnd(builder, LLVMGetEntryBasicBlock(llvmHandle));
    auto alloca = LLVMBuildAlloca(builder, type, name.c_str());
    LLVMPositionBuilderAtEnd(builder, currentBlock);

    LLVMSetCurrentDebugLocation(builder, currDebug);
    return alloca;
}

LLVMValueRef CGFunction::allocaVariable(Ref<VarDecl> decl) {
    auto alloca = insertAlloca(decl->name, env->mapType(decl->init->type()));
    variables[decl] = alloca;
    return alloca;
}

// Remove from the immediately destructing list and put it into the scope
// destructing list:
void CGFunction::promoteToScopeDestructing(RefA<Expr> expr, LLVMValueRef alloca) {
    for (auto it = immediatelyDestructing.begin(); it != immediatelyDestructing.end();
         ++it) {
        auto d = *it;

        if (expr.get() == d.e) {
            d.val = alloca;
            _scopes.back().scopeDestructing.push_back(d);
            immediatelyDestructing.erase(it);
            break;
        }
    }
}

static void cgForStmt(CGFunction* func, RefA<ForStmt> fs) {
    auto env = func->env;
    auto builder = func->builder;
    const auto& containerExpr = fs->fromContainer;
    const auto& containerType = containerExpr->type();

    auto containerAlloca = func->insertAlloca("container", env->mapType(containerType));
    func->cgIntoExpr(containerExpr, containerAlloca);

    // Make our iterator:
    auto iterableTrait = lookupTraitInstance(containerType, "Iterable");
    assert(iterableTrait && "iterator does not yield an iterable");

    auto iteratorType = iterableTrait->types.begin()->second;
    auto iteratorTrait = lookupTraitInstance(iteratorType, "Iterator");

    auto iteratorAlloca = func->insertAlloca("iterator", env->mapType(iteratorType));

    // Build the iter() call:
    LLVMValueRef iterFn = func->env->findTraitCall(containerType, iterableTrait->trait, 0);
    LLVMValueRef iterArgs[] = { containerAlloca, iteratorAlloca };
    LLVMBuildCall(builder, iterFn, iterArgs, _countof(iterArgs), "");

    auto valueType = iteratorTrait->types.begin()->second;
    auto valueAlloca = func->insertAlloca("value", env->mapType(valueType));
    func->unwrappedSymbols[fs->bind.text()] = valueAlloca;

    // Find the next/done functions on the iterator:
    assert(iteratorTrait->methods.size() == 2);
    auto nextFn = env->findTraitCall(iteratorType, iteratorTrait->trait, 0);
    auto doneFn = env->findTraitCall(iteratorType, iteratorTrait->trait, 1);

    // @TODO this shouldn't be handled here. It should be handled in a kind of trait mapping
    // system..
    if (iteratorTrait->methods[1]->name() == "next")
        std::swap(nextFn, doneFn);

    LLVMBasicBlockRef doneBlock = func->createBasicBlock("for.done");
    LLVMBasicBlockRef loopBlock = func->createBasicBlock("for.body");
    LLVMBasicBlockRef loopHeader = func->createBasicBlock("for.header");

    LLVMBuildBr(builder, loopHeader);

    // Build the loop header:
    LLVMPositionBuilderAtEnd(builder, loopHeader);

    LLVMValueRef doneCallArgs[] = { iteratorAlloca };
    auto done = LLVMBuildCall(builder, doneFn, doneCallArgs, _countof(doneCallArgs), "");
    LLVMBuildCondBr(builder, done, doneBlock, loopBlock);

    // Build the loop body:
    LLVMPositionBuilderAtEnd(builder, loopBlock);
    func->registerDebugVariable(fs->bind.text(), fs->bind.loc(), valueType, valueAlloca);

    if (env->isAggregate(valueType)) {
        LLVMValueRef nextArgs[] = { iteratorAlloca, valueAlloca };
        LLVMBuildCall(builder, nextFn, nextArgs, _countof(nextArgs), "");
    } else {
        LLVMValueRef nextArgs[] = { iteratorAlloca };
        auto result = LLVMBuildCall(builder, nextFn, nextArgs, _countof(nextArgs), "");
        LLVMBuildStore(builder, result, valueAlloca);
    }

    func->codegenBlock(fs->body);

    if (!func->builderBlockHasReturned())
        LLVMBuildBr(builder, loopHeader);

    LLVMPositionBuilderAtEnd(builder, doneBlock);
}

void CGFunction::cgStmt(RefA<Stmt> stmt) {
    auto loc = LLVMDIBuilderCreateDebugLocation(env->ctx, stmt->loc.line, 0, env->debugScope.top(),
                                                nullptr);
    LLVMSetCurrentDebugLocation(builder, LLVMMetadataAsValue(env->ctx, loc));

    if (auto ret = dyn_cast<ReturnStmt>(stmt)) {
        // When we're returning we need to emit our destructor calls:
        emitAllDestructors();

        if (ret->val) {
            if (returnPhi) {
                LLVMValueRef vals[] = { cgExpr(ret->val) };
                LLVMBasicBlockRef blocks[] = { LLVMGetInsertBlock(builder) };
                LLVMAddIncoming(returnPhi, vals, blocks, 1);
            } else {
                auto retPtr = LLVMGetLastParam(llvmHandle);
                cgIntoExpr(ret->val, retPtr);
                removeFromScopeDestructing(ret->val);
            }
        }

        LLVMBuildBr(builder, exitBlock);
    } else if (auto expr = dyn_cast<ExprStmt>(stmt)) {
        cgExpr(expr->expr);
    } else if (auto let = dyn_cast<LetStmt>(stmt)) {
        auto decl = let->decl;
        auto alloca = allocaVariable(decl);

        cgIntoExpr(decl->init, alloca);

        registerDebugVariable(decl->name, let->loc, decl->init->type(), alloca);

        promoteToScopeDestructing(decl->init, alloca);
    } else if (auto wh = dyn_cast<WhileStmt>(stmt)) {
        auto newScope = createDebugScopeForBlock(env, wh->body);

        auto testBlock = createBasicBlock("while.test");
        auto loopBlock = createBasicBlock("while.loop");
        auto endBlock = createBasicBlock("while.end");

        // Generate the jump to the test (loop condition) block and codegen that block:
        LLVMBuildBr(builder, testBlock);
        LLVMPositionBuilderAtEnd(builder, testBlock);
        LLVMBuildCondBr(builder, cgExpr(wh->condition), loopBlock, endBlock);

        addLoopInfo(wh->loc, endBlock, testBlock);

        // Lower the body of the loop:
        LLVMPositionBuilderAtEnd(builder, loopBlock);
        codegenBlock(wh->body);

        if (!builderBlockHasReturned()) {
            setCurrentDebugLocation(*wh->body.closingBrace);
            LLVMBuildBr(builder, testBlock);
        }

        LLVMPositionBuilderAtEnd(builder, endBlock);

        if (newScope)
            env->debugScope.pop();

        removeLoopInfo(wh->loc);
    } else if (auto c = dyn_cast<ContinueStmt>(stmt)) {
        // @TODO Emit destructors!
        auto ci = lookup(continueInfo, c->loc);
        assert(ci);
        LLVMBuildBr(builder, ci->block);
    } else if (auto c = dyn_cast<BreakStmt>(stmt)) {
        // @TODO Emit destructors!
        auto bi = lookup(breakInfo, c->loc);
        assert(bi);
        LLVMBuildBr(builder, bi->block);
    } else if (auto fs = dyn_cast<ForStmt>(stmt)) {
        cgForStmt(this, fs);
    } else {
        unreachable("what is this statement");
    }

    emitTemporaryDestructors();
}

struct CG {
    CGEnv env;

    // We use the global context for now. I was having some problems with GEP instructions getting
    // rewritten because they were using the wrong integer types, causing anytihng >=O1 to hang.
    LLVMContextRef llvmContext = LLVMGetGlobalContext();

    LLVMModuleRef llvmModule = LLVMModuleCreateWithNameInContext("main", llvmContext);
    LLVMDIBuilderRef debugBuilder;

    // Initialized in configureLLVM()
    struct LLVMOpaqueTargetMachine* targetMachine = nullptr;

    CG(const Vec<Ref<ModuleImplementation>>& modules);

    void generateGenericInstances(const Map<Ref<RecordGeneric>, Ref<RecordImpl>>& genericImpls,
                                  const Map<Str, Ref<FunctionDefinition>>& genericFunctionImpls,
                                  const Map<Ref<Generic>, Vec<Ref<TraitImpl>>>& genericTraitImpls) {
        while (!env.recordTemplateInstances.empty() || !env.functionTemplateInstances.empty() ||
               !env.traitTemplateInstances.empty()) {
            // Generate the functions for instantiated struct templates:
            while (!env.recordTemplateInstances.empty()) {
                auto genericRecordInstance = *env.recordTemplateInstances.begin();
                env.recordTemplateInstances.erase(env.recordTemplateInstances.begin());

                auto recordImpl = genericImpls.at(genericRecordInstance.first->shared_from_this());

                for (const auto& implInfo : genericRecordInstance.second) {
                    env.templateSubs = implInfo.subMap;

                    const auto& recordTy = implInfo.si;
                    generateRecordImpl(recordTy, recordImpl);
                }
            }

            // Generate templated functions:
            while (!env.functionTemplateInstances.empty()) {
                auto fnInsts = *env.functionTemplateInstances.begin();
                env.functionTemplateInstances.erase(env.functionTemplateInstances.begin());

                for (const auto& inst : fnInsts.second) {
                    env.templateSubs = inst.subs;
                    const auto& sig = inst.sig;
                    auto defnInfo = lookup(genericFunctionImpls, fnInsts.first);
                    assert(defnInfo);
                    generateFunction((*defnInfo)->body, inst.decl, sig->nameToken.loc(),
                                     sig->params, sig->returnType);
                }
            }

            while (!env.traitTemplateInstances.empty()) {
                auto request = env.traitTemplateInstances.back();
                env.traitTemplateInstances.pop_back();

                auto implList = lookup(genericTraitImpls, request.g);
                assert(implList);

                // Find the impl:
                Ref<TraitImpl> ourImpl;
                for (const auto& traitImpl : *implList) {
                    if (traitImpl->trait == request.trait) {
                        ourImpl = traitImpl;
                        break;
                    }
                }
                assert(ourImpl);

                env.templateSubs = request.subs;

                for (size_t i = 0; i < request.methods.size(); ++i) {
                    const auto& methodImpl = ourImpl->methods[i];
                    const auto& sig = methodImpl->signature;
                    generateFunction(methodImpl->body, request.methods[i], sig->nameToken.loc(),
                                     sig->params, sig->returnType);
                }
            }
        }
    }

    // Populate the environment's functions table so we can do lookups and
    // generate them later as needed:
    void populateFunctionDeclarations(RefA<ModuleImplementation> ast) {
        auto gen = [&](RefA<FunctionSignature> fn, CallingConv ty) {
            auto func = env.createFunctionPrototype(fn, ty);
            env.functions[fn->nameToken.loc()] = { func, ty };
        };

        for (const auto& fn : ast->functions)
            gen(fn->signature, CallingConv::Argot);

        for (const auto& fn : ast->externs)
            gen(fn, CallingConv::C);
    }

    //
    // Generate definitions for the LLVM functions the record exposes:
    //

    void generateRecordImpl(RefA<CGEnv::RecordInfo> recordTy, RefA<RecordImpl> recordImpl) {
        auto self = recordTy->type;

        // Generate constructors:
        for (size_t i = 0; i < recordImpl->constructors.size(); ++i) {
            const auto& c = recordImpl->constructors[i];
            auto llfunc = recordTy->constructors[i];

            // Generate field initializers:
            auto fieldInitializer = [&](CGFunction* func) {
                for (unsigned i = 0; i < self->fields.size(); ++i) {
                    if (auto fi = lookup(c->fieldInits, size_t(i))) {
                        auto objAddr = LLVMGetParam(func->llvmHandle, 0);
                        auto fieldAddr = LLVMBuildStructGEP(func->builder, objAddr, i, "");
                        func->cgIntoExpr(*fi, fieldAddr);

                        // field initializers are captured by the field, don't destruct:
                        func->removeFromScopeDestructing(*fi);
                    }
                }
            };

            generateFunction(c->block, llfunc, c->selfToken.loc(), c->params, Type::getVoid(), self,
                             fieldInitializer);
        }

        // Generate non-generic record methods:
        for (const auto& fn : recordImpl->methods) {
            const auto& sig = fn->signature;
            auto llfunc = recordTy->methods[sig->nameToken.loc()];
            assert(llfunc);
            generateFunction(fn->body, llfunc, sig->nameToken.loc(), sig->params, sig->returnType,
                             self);
        }

        // Generate destructor:
        if (recordTy->destructor) {
            Block b;
            SourceLoc loc;

            // Do we have an explicit destructor?
            if (auto d = recordImpl->destructor) {
                b = d->block;
                loc = d->selfToken.loc();
            } else {
                // No, our fake location is the closing brace of the record definition:
                loc = recordImpl->closingBrace;
            }

            auto name = self->symbolName() + ".~self";
            populateFunctionDebugInformation(&env, loc, recordTy->destructor, name, name);

            // The epilogue of a destructor body is the destructor calls for all their fields:
            auto destroyFields = [&](CGFunction* func) {
                for (const auto& [index, field] : enumerate(self->fields)) {
                    func->setCurrentDebugLocation(loc);
                    auto si = env.lookupRecordInfo(field.type);
                    if (si && si->destructor) {
                        auto builder = func->builder;

                        auto selfPtr = LLVMGetParam(func->llvmHandle, 0);
                        auto fieldLoc = LLVMBuildStructGEP(builder, selfPtr, (unsigned)index, "");

                        LLVMValueRef args[] = { fieldLoc };
                        LLVMBuildCall(builder, si->destructor, args, 1, "");
                    }
                }
            };

            generateFunction(b, recordTy->destructor, loc, {}, Type::getVoid(), self, none,
                             destroyFields);
        }
    }

    void generateFunction(const Block& body, LLVMValueRef function, SourceLoc loc,
                          const ArgumentList& params, RefA<Type> returnType,
                          RefA<Type> selfType = nullptr,
                          Maybe<std::function<void(CGFunction*)>> insertBefore = none,
                          Maybe<std::function<void(CGFunction*)>> insertAfter = none);

    void configureLLVM() {
        // Initialize all the things we're going to use:
        LLVMInitializeAllTargetInfos();
        LLVMInitializeAllTargets();
        LLVMInitializeAllTargetMCs();
        LLVMInitializeAllAsmParsers();
        LLVMInitializeAllAsmPrinters();

        const auto triple = "x86_64-pc-windows-msvc";
        LLVMSetTarget(llvmModule, triple);

        LLVMTargetRef target = nullptr;
        char* error = nullptr;
        if (LLVMGetTargetFromTriple(triple, &target, &error))
            unreachable(error);

        auto cglevel = LLVMCodeGenLevelNone;
        if (optLevelOpt == 1)
            cglevel = LLVMCodeGenLevelLess;
        else if (optLevelOpt == 2)
            cglevel = LLVMCodeGenLevelDefault;
        else if (optLevelOpt >= 3)
            cglevel = LLVMCodeGenLevelAggressive;

        targetMachine = LLVMCreateTargetMachine(target, triple, "generic", "", cglevel,
                                                LLVMRelocDefault, LLVMCodeModelDefault);

        auto layout = LLVMCreateTargetDataLayout(targetMachine);
        LLVMSetModuleDataLayout(llvmModule, layout);

        // Make sure we're emitting debug information in CodeView:
        LLVMAddModuleFlag(llvmModule, LLVMModuleFlagBehaviorWarning, "Debug Info Version",
                          strlen("Debug Info Version"),
                          LLVMValueAsMetadata(LLVMConstInt(LLVMInt32Type(), 3, 0)));
        LLVMAddModuleFlag(llvmModule, LLVMModuleFlagBehaviorWarning, "CodeView", 8,
                          LLVMValueAsMetadata(LLVMConstInt(LLVMInt32Type(), 1, 0)));
    }
};

void CGEnv::generateModuleDebugInformation(const Vec<Ref<ModuleImplementation>>& mods) {
    // Populate the debug file information:
    bool optimized = optLevelOpt > 0;

    for (const auto& m : mods) {
        auto [filePath, fileName] = separateFilePath(m->path);

        auto dbgFile = LLVMDIBuilderCreateFile(debugBuilder, fileName.c_str(), fileName.length(),
                                               filePath.c_str(), filePath.length());

        auto unit = LLVMDIBuilderCreateCompileUnit(debugBuilder, LLVMDWARFSourceLanguageC99,
                                                   dbgFile, producerString, strlen(producerString),
                                                   optimized, nullptr, 0, 0, nullptr, 0,
                                                   LLVMDWARFEmissionFull, 0, true, false);

        LLVMDIBuilderCreateModule(debugBuilder, unit, fileName.c_str(), fileName.length(), "", 0,
                                  "", 0, "", 0);

        debugData[m->path] = { dbgFile, unit };
    }
}

// Populate the environment's functions table so we can do lookups and
// generate them later as needed:

CG::CG(const Vec<Ref<ModuleImplementation>>& modules)
    : debugBuilder(LLVMCreateDIBuilder(llvmModule)) {
    Timer bitcodeGenerationTimer;

    env.ctx = llvmContext;
    env.mod = llvmModule;
    env.debugBuilder = debugBuilder;

    configureLLVM();
    env.generateModuleDebugInformation(modules);

    Map<Ref<RecordGeneric>, Ref<RecordImpl>> genericImpls;
    Map<Str, Ref<FunctionDefinition>> genericFunctionImpls;
    Map<Ref<Generic>, Vec<Ref<TraitImpl>>> genericTraitImpls;

    Vec<std::function<void()>> lowerTraitImpls;

    // Populate global function declarations (code generation happens later):
    for (const auto& mod : modules) {
        populateFunctionDeclarations(mod);

        for (const auto& [g, impl] : mod->genericImpls)
            genericImpls[g] = impl;

        for (const auto& [fn, impl] : mod->genericFunctions)
            genericFunctionImpls[fn] = impl;

        for (const auto& [gen, impls] : mod->genericInterfaceImpls) {
            for (const auto& ti : impls)
                genericTraitImpls[gen].push_back(ti);
        }

        // Generate the signature information for the impls and cache a set of functions to
        // complete the code generation process:
        for (const auto [type, impls] : mod->interfaceImpls) {
            for (const auto& impl : impls) {
                auto& implData = env.traitImpls[type][impl->trait];
                assert(implData.empty() && "already populated?");

                for (const auto& method : impl->methods) {
                    const auto& sig = method->signature;
                    auto funcType =
                        env.mapFunctionType(sig->params, sig->returnType, type, CallingConv::Argot);
                    auto name = type->symbolName() + "." + sig->name();

                    auto func = LLVMAddFunction(llvmModule, name.c_str(), funcType);
                    implData.push_back(func);
                    LLVMSetLinkage(func, LLVMExternalLinkage);

                    lowerTraitImpls.push_back([=]() {
                        populateFunctionDebugInformation(&env, sig->nameToken.loc(), func, name,
                                                         name);
                        generateFunction(method->body, func, sig->nameToken.loc(), sig->params,
                                         sig->returnType, type);
                    });
                }
            }
        }
    }

    // Generate code for all global functions:
    for (const auto& mod : modules) {
        for (const auto& fn : mod->functions) {
            const auto& sig = fn->signature;
            const auto& loc = sig->nameToken.loc();
            generateFunction(fn->body, env.functions[loc].handle, loc, sig->params,
                             sig->returnType);
        }
    }

    // Generate code for all record methods:
    for (const auto& mod : modules) {
        for (const auto& [ty, recordImpl] : mod->typeImpls) {
            auto recordTy = env.lookupRecordInfo(ty);
            generateRecordImpl(recordTy, recordImpl);
        }
    }

    // Generate code for concrete trait implementations:
    for (const auto& f : lowerTraitImpls)
        f();

    generateGenericInstances(genericImpls, genericFunctionImpls, genericTraitImpls);

    LLVMDIBuilderFinalize(debugBuilder);

    if (disableDebugOpt)
        LLVMStripModuleDebugInfo(llvmModule);

#ifdef _NDEBUG
    if (validateLlvmOpt)
#endif
    {
        char* msg;
        if (LLVMVerifyModule(llvmModule, LLVMReturnStatusAction, &msg)) {
            fprintf(stdout, "***LLVM verification error:***\n%s\nWrote out.ll\n", msg);
            LLVMDisposeMessage(msg);

            LLVMPrintModuleToFile(llvmModule, "out.ll", nullptr);
            return;
        }
    }

    if (verboseOpt)
        printf("Bitcode generation: %fms\n", bitcodeGenerationTimer.elapsedMs());

    Timer optimizationPassesTimer;

    if (optLevelOpt > 0) {
        auto passBuilder = LLVMPassManagerBuilderCreate();
        auto passManager = LLVMCreatePassManager();

        LLVMPassManagerBuilderSetOptLevel(passBuilder, optLevelOpt);
        LLVMPassManagerBuilderPopulateModulePassManager(passBuilder, passManager);

        LLVMPassManagerBuilderUseInlinerWithThreshold(passBuilder, 500);
        LLVMPassManagerBuilderPopulateLTOPassManager(passBuilder, passManager, true, true);

        LLVMRunPassManager(passManager, llvmModule);
    }

    if (verboseOpt)
        printf("Optimization: %fms\n", optimizationPassesTimer.elapsedMs());

    if (emitLlvmOpt)
        LLVMPrintModuleToFile(llvmModule, "out.ll", nullptr);

    if (emitAsmOpt) {
        char* error = nullptr;
        LLVMMemoryBufferRef mem;

        // This function oddly returns true on error:
        if (LLVMTargetMachineEmitToMemoryBuffer(targetMachine, llvmModule, LLVMAssemblyFile, &error,
                                                &mem)) {
            printf("Failed to output assembly:\n%s", error);
            exit(-1);
        }

        fprintf(stdout, "%s\n", LLVMGetBufferStart(mem));
        LLVMDisposeMemoryBuffer(mem);
    }

    Timer emitTimer;
    const auto objName = "foo.obj";

    // This function oddly returns true on error:
    char* error = nullptr;
    if (LLVMTargetMachineEmitToFile(targetMachine, llvmModule, const_cast<char*>(objName),
                                    LLVMObjectFile, &error)) {
        printf("Failed to output object file:\n%s", error);
        exit(-1);
    }

    if (verboseOpt)
        printf("Emit object: %fms\n", emitTimer.elapsedMs());

    LLVMDisposeModule(llvmModule);
    invokeLinker(objName);
}

void CG::generateFunction(const Block& body, LLVMValueRef function, SourceLoc loc,
                          const ArgumentList& params, RefA<Type> returnType, RefA<Type> selfType,
                          Maybe<std::function<void(CGFunction*)>> insertBefore,
                          Maybe<std::function<void(CGFunction*)>> insertAfter) {
    auto ignored = mkown<CGFunction>(&env, body, function, loc, params, returnType, selfType,
                                     insertBefore, insertAfter);
}

void codegen(const Vec<Ref<ModuleImplementation>>& modules) {
    CG cg(modules);
}
