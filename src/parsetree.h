//
// parsetree.h
// Untyped output of the parser.
//

#pragma once

#include "lexer.h"
#include "token.h"

namespace Parsetree {

struct Expr;
struct Stmt;

struct TypeRef {
    Token tok;
    TypeRef(Token tok) : tok(tok) {
    }
    virtual ~TypeRef() {
    }
};

struct Block {
    Vec<Ref<Stmt>> stmts;
    Ref<Expr> expr;

    Maybe<SourceLoc> openingBrace, closingBrace;
};

struct ExprVisitor {
    virtual void accept(struct Expr* e) {
        unreachable("");
    }

#define DEF_VISITOR(TYPE) virtual void accept(struct TYPE* e) = 0;

    // clang-format off
    DEF_VISITOR(IfExpr) DEF_VISITOR(CallExpr) DEF_VISITOR(IdExpr) DEF_VISITOR(SelfExpr)
    DEF_VISITOR(InfixExpr) DEF_VISITOR(UnaryExpr) DEF_VISITOR(ArrayIndexExpr)
    DEF_VISITOR(DotCallExpr) DEF_VISITOR(CastExpr) DEF_VISITOR(MatchExpr) DEF_VISITOR(DotExpr)
    DEF_VISITOR(StringLiteralExpr) DEF_VISITOR(IntLiteralExpr) DEF_VISITOR(FloatLiteralExpr)
    DEF_VISITOR(BoolLiteralExpr) DEF_VISITOR(RecordInitExpr) DEF_VISITOR(SizeofExpr)
    DEF_VISITOR(NormExpr) DEF_VISITOR(ArrayInitExpr) DEF_VISITOR(ArrayRepeatExpr)
    // clang-format on

#undef DEF_VISITOR
};

#define DEFINE_VISIT                                                                               \
    void visit(ExprVisitor* v) override {                                                          \
        v->accept(this);                                                                           \
    }

//
// Expressions:
//

struct Expr {
    SourceLoc loc;

    Expr(SourceLoc loc) : loc(loc) {
    }
    virtual ~Expr() {
    }

    virtual void visit(ExprVisitor* v) = 0;
};

struct IfExpr : Expr {
    Ref<Expr> condition;
    Block body;
    Maybe<Block> elseBody;

    bool yieldsValue() const {
        return elseBody && body.expr && elseBody->expr;
    }

    IfExpr(Token ifToken, RefA<Expr>& condition, const Block& body, Maybe<Block> elseBody)
        : Expr(ifToken.loc()), condition(condition), body(body), elseBody(elseBody) {
    }
    DEFINE_VISIT
};

struct CallExpr : Expr {
    Token id;
    Maybe<Vec<Ref<TypeRef>>> templates;
    Vec<Ref<Expr>> args;

    CallExpr(Token id, Maybe<Vec<Ref<TypeRef>>> templates, const Vec<Ref<Expr>>& args)
        : Expr(id.loc()), id(id), templates(templates), args(args) {
    }
    DEFINE_VISIT
};

struct IdExpr : Expr {
    Token id;
    IdExpr(Token id) : Expr(id.loc()), id(id) {
    }
    DEFINE_VISIT
};

struct SelfExpr : Expr {
    Token self;
    SelfExpr(Token self) : Expr(self.loc()), self(self) {
    }
    DEFINE_VISIT
};

struct InfixExpr : Expr {
    Ref<Expr> lhs, rhs;
    Token op;

    InfixExpr(RefA<Expr> lhs, Token op, RefA<Expr> rhs)
        : Expr(lhs->loc), lhs(lhs), rhs(rhs), op(op) {
    }

    DEFINE_VISIT
};

struct UnaryExpr : Expr {
    Token op;
    Ref<Expr> e;

    UnaryExpr(Token op, RefA<Expr> e) : Expr(op.loc()), op(op), e(e) {
    }

    DEFINE_VISIT
};

struct ArrayIndexExpr : Expr {
    Ref<Expr> lhs, index;
    ArrayIndexExpr(RefA<Expr> lhs, RefA<Expr> index) : Expr(lhs->loc), lhs(lhs), index(index) {
    }
    DEFINE_VISIT
};

struct DotExpr : Expr {
    Ref<Expr> lhs;
    Token field;
    bool arrow;

    DotExpr(RefA<Expr> lhs, Token field, bool arrow)
        : Expr(lhs->loc), lhs(lhs), field(field), arrow(arrow) {
    }

    DEFINE_VISIT
};

struct DotCallExpr : Expr {
    Ref<Expr> lhs;
    Token field;
    Vec<Ref<Expr>> args;
    bool arrow;

    DotCallExpr(RefA<Expr> lhs, Token field, const Vec<Ref<Expr>>& args, bool arrow)
        : Expr(lhs->loc), lhs(lhs), field(field), args(args), arrow(arrow) {
    }

    DEFINE_VISIT
};

struct CastExpr : Expr {
    Ref<Expr> lhs;
    Ref<TypeRef> type;

    CastExpr(RefA<Expr> lhs, RefA<TypeRef> type) : Expr(lhs->loc), lhs(lhs), type(type) {
    }

    DEFINE_VISIT
};

struct Pattern {
    Token name;
    Maybe<Token> unwrapped;

    Pattern(Token name, Maybe<Token> unwrapped) : name(name), unwrapped(unwrapped) {
    }
};

struct MatchArm {
    Ref<Pattern> pat;
    Block block;
};

struct MatchExpr : Expr {
    Ref<Expr> match;
    Vec<MatchArm> arms;

    MatchExpr(Token matchToken, RefA<Expr> match, const Vec<MatchArm>& arms)
        : Expr(matchToken.loc()), match(match), arms(arms) {
    }

    DEFINE_VISIT
};

// Brace-initialized fields:
struct RecordInitExpr : Expr {
    Ref<TypeRef> type;
    Vec<Ref<Expr>> inits;

    RecordInitExpr(RefA<TypeRef> type, const Vec<Ref<Expr>>& inits)
        : Expr(type->tok.loc()), type(type), inits(inits) {
    }

    DEFINE_VISIT
};

struct SizeofExpr : Expr {
    Ref<TypeRef> type;
    SizeofExpr(SourceLoc loc, RefA<TypeRef> type) : Expr(loc), type(type) {
    }
    DEFINE_VISIT
};

struct NormExpr : Expr {
    Ref<Expr> of;
    NormExpr(SourceLoc loc, RefA<Expr> of) : Expr(loc), of(of) {
    }
    DEFINE_VISIT
};

// Literal exprs:

// "abc"
struct StringLiteralExpr : Expr {
    Str text;
    StringLiteralExpr(StrA text, SourceLoc loc) : Expr(loc), text(text) {
    }
    DEFINE_VISIT
};

// 123
enum IntLiteralType { ITT_CHAR, ITT_INT };
struct IntLiteralExpr : Expr {
    unsigned int val;
    IntLiteralType type;
    IntLiteralExpr(int val, SourceLoc loc, IntLiteralType type) : Expr(loc), val(val), type(type) {
    }
    DEFINE_VISIT
};

// 123.f
struct FloatLiteralExpr : Expr {
    double val;
    FloatLiteralExpr(double val, SourceLoc loc) : Expr(loc), val(val) {
    }
    DEFINE_VISIT
};

// true
struct BoolLiteralExpr : Expr {
    bool val;
    BoolLiteralExpr(bool val, SourceLoc loc) : Expr(loc), val(val) {
    }
    DEFINE_VISIT
};

// [ 1, 2, 3 ]
struct ArrayInitExpr : Expr {
    Vec<Ref<Expr>> inits;

    ArrayInitExpr(Token t, VecA<Ref<Expr>> inits) : Expr(t.loc()), inits(inits) {
    }

    DEFINE_VISIT
};

// [ 1; 5 ]
struct ArrayRepeatExpr : Expr {
    Ref<Expr> init, count;

    ArrayRepeatExpr(Token t, RefA<Expr> init, RefA<Expr> count)
        : Expr(t.loc()), init(init), count(count) {
    }

    DEFINE_VISIT
};

#undef DEFINE_VISIT

//
// Statements:
//

struct Stmt {
    SourceLoc loc;
    Stmt(const SourceLoc& loc) : loc(loc) {
    }

    virtual ~Stmt() {
    }
};

struct LetStmt : Stmt {
    Token nameToken;
    Ref<TypeRef> type;
    Ref<Expr> init;
    LetStmt(Token nameToken, Ref<TypeRef> type, RefA<Expr> init)
        : Stmt(nameToken.loc()), nameToken(nameToken), type(type), init(init) {
    }
};

struct WhileStmt : Stmt {
    Ref<Expr> condition;
    Block body;

    WhileStmt(SourceLoc loc, const Ref<Expr>& condition, const Block& body)
        : Stmt(loc), condition(condition), body(std::move(body)) {
    }
};

struct ReturnStmt : Stmt {
    Ref<Expr> val;
    ReturnStmt(SourceLoc loc, const Ref<Expr>& val) : Stmt(loc), val(val) {
    }
};

struct ExprStmt : Stmt {
    Ref<Expr> expr;
    ExprStmt(const Ref<Expr>& expr) : Stmt(expr->loc), expr(expr) {
    }
};

struct ForStmt : Stmt {
    Token bind;
    Ref<Expr> from;
    Block body;

    ForStmt(Token bind, RefA<Expr> from, const Block& block)
        : Stmt(bind.loc()), bind(bind), from(from), body(block) {
    }
};

struct BreakStmt : Stmt {
    BreakStmt(SourceLoc loopLoc) : Stmt(loopLoc) {
    }
};

struct ContinueStmt : Stmt {
    ContinueStmt(SourceLoc loopLoc) : Stmt(loopLoc) {
    }
};

// References to types:

// Bare identifier
struct SimpleTypeRef : TypeRef {
    Token id;
    SimpleTypeRef(Token id) : TypeRef(id), id(id) {
    }
};

// A<B, C, D>
struct TemplateInstanceRef : TypeRef {
    Token name;
    Vec<Ref<TypeRef>> args;
    TemplateInstanceRef(Token nameTok, const Vec<Ref<TypeRef>>& args)
        : TypeRef(nameTok), name(nameTok), args(args) {
    }
};

// int[3]
struct ArrayTypeRef : TypeRef {
    Ref<TypeRef> interior;
    size_t length;

    ArrayTypeRef(Token tok, RefA<TypeRef> interior, size_t length)
        : TypeRef(tok), interior(interior), length(length) {
    }
};

// A*
struct PointerTypeRef : TypeRef {
    Ref<TypeRef> interior;
    PointerTypeRef(Token tok, RefA<TypeRef> base) : TypeRef(tok), interior(base) {
    }
};

// \(int, int) -> bool
struct FunctionTypeRef : TypeRef {
    Ref<TypeRef> returnType;
    Vec<Ref<TypeRef>> argTypes;

    FunctionTypeRef(Token t, RefA<TypeRef> returnType, VecA<Ref<TypeRef>> argTypes)
        : TypeRef(t), returnType(returnType), argTypes(argTypes) {
    }
};

// Information about type definitions:

struct Field {
    Token nameToken;
    Ref<TypeRef> type;
};

struct TemplateArg {
    Str name;

    // These aren't actually types but we reuse it for simplicity:
    Vec<Ref<TypeRef>> constraints;

    TemplateArg(StrA name, VecA<Ref<TypeRef>> constraint) : name(name), constraints(constraint) {
    }
};

struct Argument {
    Str name;
    Ref<TypeRef> type;
    VFlags flags;

    Argument(const Str& name, RefA<TypeRef> type, VFlags flags)
        : name(name), type(type), flags(flags) {
    }
};
typedef Vec<Argument> ArgumentList;

struct FunctionSignature {
    Token nameToken;
    ArgumentList params;
    Ref<TypeRef> returnType;
    Vec<Ref<TemplateArg>> templates;

    FunctionSignature(Token name, const ArgumentList& params, RefA<TypeRef> returnType,
                      const Vec<Ref<TemplateArg>>& templates)
        : nameToken(name), params(params), returnType(returnType), templates(templates) {
    }

    bool isTemplated() const {
        return templates.size() > 0;
    }

    Str name() const {
        return nameToken.text();
    }
};

// Top-level:

struct FunctionDefinition {
    Ref<FunctionSignature> signature;
    Block body;

    FunctionDefinition(RefA<FunctionSignature> sig, const Block& body) : signature(sig), body(body) {
    }
};

struct TypeDefinition {
    Token name;
    Vec<Ref<TemplateArg>> templates;

    TypeDefinition(Token name, const Vec<Ref<TemplateArg>>& templates)
        : name(name), templates(templates) {
    }
    virtual ~TypeDefinition() {
    }
};

struct ConstructorInitField {
    Token name;
    Vec<Ref<Expr>> args;
};

struct ConstructorDefinition {
    Token selfToken;
    ArgumentList args;
    Vec<ConstructorInitField> inits;
    Block block;
};

struct DestructorDefinition {
    Token selfToken;
    Block block;
};

struct MethodSignature : FunctionSignature {
    bool isVirtual;

    MethodSignature(RefA<FunctionSignature> sig, bool isVirtual)
        : FunctionSignature(*sig), isVirtual(isVirtual) {
    }
};

struct MethodDefinition {
    Ref<MethodSignature> signature;
    Block body;

    MethodDefinition(RefA<MethodSignature> sig, const Block& body) : signature(sig), body(body) {
    }
};

struct RecordDefinition : TypeDefinition {
    Vec<Ref<TypeRef>> bases;
    Vec<ConstructorDefinition> constructors;
    Vec<Field> fields;
    Vec<MethodDefinition> methods;
    Maybe<DestructorDefinition> destructor;
    SourceLoc closingBrace;

    RecordDefinition(Token name, const Vec<Ref<TemplateArg>>& templates, VecA<Ref<TypeRef>> bases)
        : TypeDefinition(name, templates), bases(bases) {
    }
};

struct VariantDefinition : TypeDefinition {
    Vec<Field> fields;

    VariantDefinition(Token name, const Vec<Ref<TemplateArg>>& templates, const Vec<Field>& fields)
        : TypeDefinition(name, templates), fields(fields) {
    }
};

struct Import {
    Vec<Str> path;
};

// A compile-time interface for template constraints:
struct InterfaceDefinition {
    Token name;
    Vec<Ref<FunctionSignature>> methods;
    Set<Pair<Str, Ref<TypeRef>>> types;
};

// An implementation of a compile-time interface:
struct TraitImpl {
    Vec<Ref<TemplateArg>> templates;
    Ref<TypeRef> theType, theTrait;
    Vec<Ref<FunctionDefinition>> methods;
    Map<Str, Ref<TypeRef>> typeMap;
};

struct ModuleImplementation {
    Str path;
    Vec<Import> imports;
    Vec<Ref<FunctionSignature>> externs;
    Vec<Ref<FunctionDefinition>> functions;
    Vec<Ref<TypeDefinition>> types;

    Vec<InterfaceDefinition> interfaces;
    Vec<TraitImpl> interfaceImpls;
};

} // namespace Parsetree
