#include "common.h"

#include "ast.h"

LetVariableSymbol::LetVariableSymbol(RefA<Ast::LetStmt> orig)
    : VariableSymbol(orig->decl->init->type()), origin(orig) {}
