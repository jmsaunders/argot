#pragma once

#include "token.h"

const char* operatorToString(TokenType op);
Str getErrorLineText(const char* location, VecA<size_t> newlines, const char* buffer);

struct LexedFile {
    const char* filename;
    const char* buffer;
    Vec<Token> tokens;

    // Locations of newlines in the file:
    Vec<size_t> newlines;

    Str getErrorLineText(const char* location) const {
        return ::getErrorLineText(location, newlines, buffer);
    }
};

Ref<LexedFile> lex(StrA filename, const char* textToLex);
