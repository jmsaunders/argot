#include "common.h"

#include <fstream>
#include <mutex>

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

using namespace std;

int64_t _performanceFrequency;
static once_flag timer_init;

Maybe<Str> readFile(const Str& name) {
    ifstream t(name);
    if (!t.good())
        return none;

    return Str(istreambuf_iterator<char>(t), istreambuf_iterator<char>());
}

Pair<Str, Str> separateFilePath(StrA inputPath) {
    Str path, file = inputPath;

    auto cutoffIt = std::find_if(rbegin(inputPath), rend(inputPath),
                               [](char c) { return c == '\\' || c == '/'; });

    if (cutoffIt != inputPath.rend()) {
        auto cutoff = distance(cutoffIt, rend(inputPath));
        path = inputPath.substr(0, cutoff - 1);
        file = inputPath.substr(cutoff);
    }

    return make_pair(path, file);
}

bool isRelativePath(StrA path) {
    // Linux:
    if (path[0] == '/')
        return false;

    // Windows:
    if (path.size() > 1 && isalpha(path[0]) && path[1] == ':')
        return false;

    return true;
}

bool isPathSeparator(char c) {
    return c == '\\' || c == '/';
}

Str joinPath(StrA lhs, StrA rhs) {
    if (lhs.empty())
        return rhs;
    if (rhs.empty())
        return lhs;

    if (isPathSeparator(lhs.back()))
        return lhs + rhs;

    return lhs + _pathSeparator + rhs;
}

bool fileExists(StrA path) {
    auto f = fopen(path.c_str(), "r");
    if (f) {
        fclose(f);
        return true;
    }
    return false;
}

#ifdef _WIN32

Timer::Timer() {
    call_once(timer_init, []() {
        LARGE_INTEGER pf;
        QueryPerformanceFrequency(&pf);
        _performanceFrequency = pf.QuadPart;
    });

    reset();
}

void Timer::reset() {
    LARGE_INTEGER s;
    QueryPerformanceCounter(&s);
    _start = s.QuadPart;
}

double Timer::elapsedS() const {
    LARGE_INTEGER s;
    QueryPerformanceCounter(&s);
    return double(s.QuadPart - _start) / _performanceFrequency;
}

double Timer::elapsedMs() const {
    return elapsedS() * 1000;
}

Str workingDirectory() {
    // We allocate +1 bytes because GetCurrentDirectory writes the null term explicitly.
    // Just resize it away after:

    auto len = GetCurrentDirectoryA(0, nullptr);
    Str ret(len + 1, ' ');
    len = GetCurrentDirectoryA(len, ret.data());
    ret.resize(len);
    return ret;
}

Wstr workingDirectoryW() {
    // We allocate +1 bytes because GetCurrentDirectory writes the null term explicitly.
    // Just resize it away after:

    auto len = GetCurrentDirectoryW(0, nullptr);
    Wstr ret(len + 1, ' ');
    len = GetCurrentDirectoryW(len, ret.data());
    ret.resize(len);
    return ret;
}

void debugBreak() {
    DebugBreak();
}

#else

#error provide platform implementation

#endif
