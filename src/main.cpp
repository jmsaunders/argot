//
// Entry point:
//

#include "common.h"

#include "ast.h"
#include "cmdline.h"
#include "parsetree.h"
#include <sstream>

using namespace std;

cl::PositionalOpt inputFile;
cl::Opt<bool> verboseOpt("v");

Ref<Parsetree::ModuleImplementation> parse(StrA filename);

struct TcScope;

Pair<Ref<Ast::ModuleImplementation>, Ref<TcScope>>
typecheck(RefA<Parsetree::ModuleImplementation> mi, const Vec<Ref<TcScope>>& deps);

void codegen(const Vec<Ref<Ast::ModuleImplementation>>& modules);

//
// This is effectively the build system:
//

// @TOOD this could be made configurable with flags:
static const char* _defaultStandardLib = "C:\\dev\\argot";

struct Build {
    enum ModuleState {
        Parsed = 0,
        Typechecking,
        Typed,
    };

    struct ModuleInfo {
        Str path;
        Ref<Parsetree::ModuleImplementation> parseTree;
        Ref<Ast::ModuleImplementation> typedTree;
        Ref<TcScope> scope;
        Vec<Str> deps;
        ModuleState status = Parsed;
    };

    Map<Str, ModuleInfo> infos;
    Vec<Pair<Str, Str>> fileQueue;

    static Pair<Str, Str> catPath(const Vec<Str>& path, char separator = _pathSeparator) {
        stringstream ss;
        bool first = true;
        for (size_t i = 0; i < path.size() - 1; ++i) {
            if (!first)
                ss << separator;
            ss << path[i];
            first = false;
        }

        return make_pair(ss.str(), path.back());
    }

    // File resolution for an input file loaded by the compiler with 'open':
    static bool findPath(VecA<Str> pathInput, StrA curDir, Str& newFilePath, Str& newFilename) {
        Str inputPath;
        tie(inputPath, newFilename) = catPath(pathInput);
        newFilename += ".ar";

        // Try looking in the current directory:
        auto cwdPath = joinPath(curDir, inputPath);
        if (fileExists(joinPath(cwdPath, newFilename))) {
            newFilePath = cwdPath;
            return true;
        }

        // Try looking in the standard library location:
        auto stdPath = joinPath(_defaultStandardLib, inputPath);
        if (fileExists(joinPath(stdPath, newFilename))) {
            newFilePath = stdPath;
            return true;
        }

        return true;
    }

    void parseFile(StrA path, StrA name) {
        Str fullFilename = joinPath(path, name);
        auto& info = infos[fullFilename];
        info.path = fullFilename;

        if (info.parseTree)
            return;

        auto mod = parse(fullFilename);
        info.parseTree = mod;

        for (auto i : mod->imports) {
            assert(i.path.size() > 0);

            Str newFilePath, newFilename;
            if (!findPath(i.path, path, newFilePath, newFilename)) {
                fprintf(stderr, "cannot find opened file %s", i.path.back().c_str());
                exit(-1);
            }

            info.deps.push_back(joinPath(newFilePath, newFilename));
            fileQueue.push_back({ newFilePath, newFilename });
        }
    }

    void performTypecheck(StrA file) {
        auto& info = infos[file];
        if (!info.typedTree) {
            if (info.status == Typechecking) {
                fprintf(stderr, "cycle in module dependencies involving %s\n", file.c_str());
                exit(-1);
            }

            info.status = Typechecking;

            // resolve all its dependenies:
            Vec<Ref<TcScope>> deps;
            for (const auto& dep : info.deps) {
                performTypecheck(dep);
                deps.push_back(infos[dep].scope);
            }

            tie(info.typedTree, info.scope) = typecheck(info.parseTree, deps);

            info.status = Typed;
        }
    }

    void run() {
        if (!inputFile.value) {
            fprintf(stderr, "expected input file\n");
            exit(-1);
        }

        auto fullInputPath = *inputFile.value;
        if (isRelativePath(fullInputPath))
            fullInputPath = joinPath(workingDirectory(), *inputFile.value);

        auto separatedPath = separateFilePath(fullInputPath);
        fileQueue.push_back({ separatedPath.first, separatedPath.second });

        Timer parseTimer;

        // Process all the input files:
        while (!fileQueue.empty()) {
            auto f = fileQueue.back();
            fileQueue.pop_back();

            parseFile(f.first, f.second);
        }

        if (verboseOpt)
            printf("Lex/parse time: %fms\n", parseTimer.elapsedMs());

        Timer typecheckTimer;

        for (const auto& m : infos)
            performTypecheck(m.first);

        if (verboseOpt)
            printf("Typechecking time: %fms\n", typecheckTimer.elapsedMs());

        Vec<Ref<Ast::ModuleImplementation>> mods;
        for (const auto& [_, mod] : infos)
            mods.push_back(mod.typedTree);

        codegen(mods);
    }
};

int main(int argc, const char** argv) {
    Timer totalTimer;
    parseCommandLine(argc, argv);

    Build build;
    build.run();

    if (verboseOpt)
        printf("Total time: %fms\n", totalTimer.elapsedMs());
}
