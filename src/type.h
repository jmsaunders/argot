//
// type.h
// Fully resolved types, used in the type checker and forward.
//

#pragma once

#include "token.h"

struct Interface;

//
// Type information:
//

struct Type {
    Str name;

    Type(StrA name) : name(name) {
    }
    virtual ~Type() {
    }

    virtual bool equals(RefA<Type> rhs) const;

    bool isBool() const {
        return this == getBool().get();
    }
    bool isVoid() const {
        return this == getVoid().get();
    }

    Ref<Type> resolve();

    // Cast-up helpers:
    virtual Ref<Type> ref() = 0;
    virtual Ref<struct IntType> intType() {
        return nullptr;
    }
    virtual Ref<struct PointerType> pointerType() {
        return nullptr;
    }
    virtual Ref<struct RecordBaseType> recordType() {
        return nullptr;
    }
    virtual Ref<struct VariantBaseType> variantType() {
        return nullptr;
    }
    virtual Ref<struct GenericInstance> genericInstance() {
        return nullptr;
    }
    virtual Ref<struct TypeVariable> typeVariable() {
        return nullptr;
    }
    virtual Ref<struct TemplateArgType> templateVariable() {
        return nullptr;
    }

    // built-in types:
    static Ref<Type> getI8();
    static Ref<Type> getI16();
    static Ref<Type> getI32();
    static Ref<Type> getI64();
    static Ref<Type> getF32();
    static Ref<Type> getBool();
    static Ref<Type> getVoid();

    Str symbolName() const {
        return name;
    }

    // Some builtin trait information:
    virtual bool defaultConstructible() {
        return true;
    }
    virtual bool copyable();
    virtual bool nontrivialCopy() {
        return false;
    }

    // Traits this type implements:
    Vec<Ref<struct TraitInstanceInfo>> traits;
};

// Built-ins:

struct IntType : Type, std::enable_shared_from_this<IntType> {
    int width;
    static Ref<IntType> make(int width);

    Ref<Type> ref() override {
        return shared_from_this();
    }
    Ref<IntType> intType() override {
        return shared_from_this();
    }

  private:
    IntType(int width);
};

struct FloatType : Type, std::enable_shared_from_this<FloatType> {
    int width;
    static Ref<FloatType> make(int width);

    Ref<Type> ref() override {
        return shared_from_this();
    }

  private:
    FloatType(int width);
};

struct BoolType : Type, std::enable_shared_from_this<BoolType> {
    BoolType() : Type("bool") {
    }

    Ref<Type> ref() override {
        return shared_from_this();
    }
};

struct VoidType : Type, std::enable_shared_from_this<VoidType> {
    VoidType() : Type("void") {
    }

    Ref<Type> ref() override {
        return shared_from_this();
    }
};

//
// Replacable types are things in the domains of substitution maps.
// More specifically, they're template variables and internal trait types that get replaced
// eventually when we have type instance information.
//

struct ReplacableType : Type {
    ReplacableType(StrA name) : Type(name) {
    }
};

using SubstitutionMap = Map<Ref<ReplacableType>, Ref<Type>>;

//
// Generics:
//

struct Generic {
    virtual ~Generic() {
    }

    virtual Ref<Type> instantiate(const Vec<Ref<Type>>& vars);
    virtual Ref<Type> instantiate(const SubstitutionMap& vars) = 0;
    Ref<Type> trivialInstance();

    Token nameToken;
    Str name;
    Vec<Ref<TemplateArgType>> templates;

    // The outward-facing characteristics of this generic are now fully specified.
    // This is important because it means instances can be constructed.
    void markComplete() {
        complete = true;
    }
    bool isComplete() const {
        return complete;
    }

    bool validInstantiation(const SubstitutionMap& subs);

    Vec<Ref<TraitInstanceInfo>> traits;

  protected:
    Generic(Token name, const Vec<Ref<TemplateArgType>>& templates)
        : nameToken(name), name(name.text()), templates(templates) {
    }

    bool complete = false;
};

struct GenericInstance {
    virtual Ref<Generic> parent() = 0;
    SubstitutionMap arguments;

    GenericInstance(RefA<Generic> generic, const SubstitutionMap& arguments)
        : arguments(arguments) {
    }

    Ref<Type> refine(const SubstitutionMap& map);
};

struct PointerType : Type, std::enable_shared_from_this<PointerType> {
    Ref<Type> interior;
    PointerType(RefA<Type> interior) : Type(interior->name + "*"), interior(interior) {
    }

    static Ref<PointerType> of(Ref<Type> other) {
        return std::make_shared<PointerType>(other);
    }

    bool copyable() override {
        return true;
    }

    Ref<Type> ref() override {
        return shared_from_this();
    }
    Ref<PointerType> pointerType() override {
        return shared_from_this();
    }
};

struct TraitSelfType : Type, std::enable_shared_from_this<TraitSelfType> {
    Ref<Interface> trait;

    Ref<Type> ref() override {
        return shared_from_this();
    }
    TraitSelfType(RefA<Interface> trait);
};

// An argument to a template, including a possibly constraint on Generic(s):
struct TemplateArgType : ReplacableType, std::enable_shared_from_this<TemplateArgType> {
    Ref<Interface> constraint;

    TemplateArgType(StrA name, RefA<Interface> constraint = nullptr)
        : ReplacableType(name), constraint(constraint) {
    }

    bool equals(RefA<Type> rhs) const override {
        auto other = rhs->templateVariable();
        return other == shared_from_this();
    }

    //
    // For now all template arguments are implicitly copyable.
    // Eventually we can use the traits system to require copyability of these
    // arguments but for now assuming anything templated on requires copyability
    // is just fine.
    //
    // We always want to say we have a nontrivial copy because some instances may.
    //

    bool copyable() override {
        return true;
    }
    bool nontrivialCopy() override {
        return true;
    }

    Ref<Type> ref() override {
        return shared_from_this();
    }
    Ref<TemplateArgType> templateVariable() override {
        return shared_from_this();
    }
};

// A type variable used for type inference:
struct TypeVariable : Type, std::enable_shared_from_this<TypeVariable> {
    Ref<Type> bound;

    TypeVariable() : Type("TYPEVAR_" + std::to_string((uint64_t)this)) {
    }

    bool copyable() override {
        return true;
    }
    bool nontrivialCopy() override {
        return bound ? bound->nontrivialCopy() : true;
    }

    Ref<Type> ref() override {
        return shared_from_this();
    }
    Ref<TypeVariable> typeVariable() override {
        return shared_from_this();
    }
};

struct Argument {
    Str name;
    Ref<Type> type;
    VFlags flags;

    Argument(const Str& name, RefA<Type> type, VFlags flags)
        : name(name), type(type), flags(flags) {
    }
};
typedef Vec<Argument> ArgumentList;

struct VariadicInfo {
    Str name;
    Ref<Type> type;
    Ref<Interface> trait;
};

struct FunctionSignature {
    // @TODO
    // I'd like to remove this to make it easier to have builtin functions exposed. Right now we
    // hash on this to uniquely identify functions in the backend though.
    Token nameToken;
    Vec<Ref<TemplateArgType>> templates;

    // This is the list of non-variadic (i.e. required) arguments to the function:
    ArgumentList params;
    Ref<Type> returnType;

    // Non-null if we're a variadic function. The Trait describes the constraint for the arguments.
    Maybe<VariadicInfo> variadicConstraint;

    FunctionSignature(Token name, const ArgumentList& params, RefA<Type> returnType,
                      const Vec<Ref<TemplateArgType>>& templates)
        : nameToken(name), templates(templates), params(params), returnType(returnType) {
    }

    bool isTemplated() const {
        return templates.size() > 0;
    }

    Str name() const {
        return nameToken.text();
    }
};

//
// Record types:
//

struct RecordField {
    Str name;
    Ref<Type> type;
};

struct ConstructorDecl {
    Token selfToken;
    ArgumentList params;
};

struct DestructorDecl {
    Token squiggleSelf;
};

struct MethodSignature : FunctionSignature {
  public:
    MethodSignature(RefA<Type> originType, const FunctionSignature& sig)
        : FunctionSignature(sig), originType(originType) {
    }

    // The record this comes from:
    Ref<Type> originType;
    bool isVirtual = false;
};

// Common information shared between all record types:
struct RecordBase {
    Vec<Ref<RecordBaseType>> bases;
    Vec<ConstructorDecl> constructors;
    Vec<RecordField> fields;
    Vec<Ref<MethodSignature>> methods;

    // The above lists are only for the current record. The symbol table contains all of this
    // information in addition to the information from base classes.
    Map<Str, Ref<Symbol>> symbols;

    //
    // Meta-information about the record type:
    //

    // These indices are positions the constructors occupy in the constructor list:
    Maybe<size_t> copyConstructorIndex, defaultConstructorIndex;

    Maybe<DestructorDecl> explicitDestructorDecl;

    virtual ~RecordBase() {
    }
};

// The Generic for generic records:
struct RecordGeneric : Generic, RecordBase, std::enable_shared_from_this<RecordGeneric> {
    Ref<Type> instantiate(const SubstitutionMap& vars) override;

    RecordGeneric(Token name, const Vec<Ref<TemplateArgType>>& templates)
        : Generic(name, templates) {
    }
};

//
// This is the interface the type checker uses the interact with record types:
// RecordBase contains the information about the type.
//

struct RecordBaseType : Type, RecordBase {
    Token origin;
    RecordBaseType(StrA name, Token nameToken) : Type(name), origin(nameToken) {
    }

    bool defaultConstructible() override;
    bool copyable() override;
    bool nontrivialCopy() override;
};

// Non-templated record types:
struct RecordType : RecordBaseType, std::enable_shared_from_this<RecordType> {
    RecordType(Token name) : RecordBaseType(name.text(), name) {
    }

    Ref<Type> ref() override {
        return shared_from_this();
    }
    virtual Ref<RecordBaseType> recordType() override {
        return shared_from_this();
    }
};

// Template instantiations of record kinds:
struct RecordInstanceType : RecordBaseType,
                            GenericInstance,
                            std::enable_shared_from_this<RecordInstanceType> {
    RecordInstanceType(Str name, Token origin, RefA<RecordGeneric> gen,
                       const SubstitutionMap& templates)
        : RecordBaseType(name, origin), GenericInstance(gen, templates), _parent(gen) {
    }

    Ref<Type> ref() override {
        return shared_from_this();
    }
    Ref<Generic> parent() override {
        return _parent;
    }
    Ref<RecordGeneric> typedParent() {
        return _parent;
    }
    Ref<GenericInstance> genericInstance() override {
        return shared_from_this();
    }

    Ref<RecordBaseType> recordType() override;

  private:
    Ref<RecordGeneric> _parent;
    bool _filled = false;
};

//
// Variants:
//

struct VariantField {
    Token nameToken;
    Ref<Type> type;

    Str name() const {
        return nameToken.text();
    }
};

struct VariantBase {
    Vec<VariantField> fields;

    virtual ~VariantBase() {
    }
};

struct VariantGeneric : Generic, VariantBase, std::enable_shared_from_this<VariantGeneric> {
    VariantGeneric(Token name, const Vec<Ref<TemplateArgType>>& templates)
        : Generic(name, templates) {
    }

    Ref<Type> instantiate(const SubstitutionMap& vars) override;
};

struct VariantBaseType : VariantBase, Type {
    Token origin;

    VariantBaseType(StrA name, Token origin) : Type(name), origin(origin) {
    }

    bool copyable() override;
    bool nontrivialCopy() override;
    bool defaultConstructible() override {
        return false;
    }

    bool hasPayload() const {
        for (const auto& field : fields) {
            if (field.type)
                return true;
        }
        return false;
    }
};

// type Abc<T, U> = A(T) | B(U);
struct VariantType : VariantBaseType, std::enable_shared_from_this<VariantType> {
    VariantType(Token name) : VariantBaseType(name.text(), name) {
    }

    Ref<Type> ref() override {
        return shared_from_this();
    }
    Ref<VariantBaseType> variantType() override {
        return shared_from_this();
    }
};

// A generic variant type with substitution information on its kinds:
struct VariantTypeInstance : VariantBaseType,
                             GenericInstance,
                             std::enable_shared_from_this<VariantTypeInstance> {
    VariantTypeInstance(StrA name, Token origin, RefA<VariantGeneric> parent,
                        const SubstitutionMap& subs)
        : VariantBaseType(name, origin), GenericInstance(parent, subs), _parent(parent) {
    }

    Ref<Type> ref() override {
        return shared_from_this();
    }
    Ref<VariantBaseType> variantType() override;
    Ref<Generic> parent() override {
        return _parent;
    }
    Ref<GenericInstance> genericInstance() override {
        return shared_from_this();
    }

  private:
    Ref<VariantGeneric> _parent;
    bool _filled = false;
};

// Function (pointer) type:
struct FunctionType : Type, std::enable_shared_from_this<FunctionType> {
    Ref<Type> returnType;
    Vec<Ref<Type>> argTypes;

    FunctionType(RefA<Type> returnType, VecA<Ref<Type>> argTypes);

    Ref<Type> ref() override {
        return shared_from_this();
    }

    bool copyable() override {
        return true;
    }
    bool nontrivialCopy() override {
        return false;
    }
    bool defaultConstructible() override {
        return false;
    }
};

// Array type:
struct ArrayType : Type, std::enable_shared_from_this<ArrayType> {
    Ref<Type> baseType;
    size_t length;

    ArrayType(RefA<Type> baseType, size_t length);

    Ref<Type> ref() override {
        return shared_from_this();
    }

    bool copyable() override {
        return false;
    }
    bool nontrivialCopy() override {
        return false;
    }
    bool defaultConstructible() override {
        return false;
    }
};

Str generateTemplateListString(const Vec<Ref<Type>>& templates);
Str generateTemplateListString(const Vec<Ref<TemplateArgType>>& templates,
                               const SubstitutionMap& instances);

Ref<Type> replaceTypesFromMap(RefA<Type> from, const SubstitutionMap& map);
Ref<Interface> replaceTypesFromMap(RefA<Interface> from, const SubstitutionMap& map);
Ref<FunctionSignature> replaceTypesFromMap(RefA<FunctionSignature> fs, const SubstitutionMap& map);

// Returns a string on error:
Maybe<Str> canUnifyTypes(Ref<Type> lhs, Ref<Type> rhs);
