#include <stdio.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

void printnum(__int64 num) {
	printf("%lld\n", num);
    fflush(stdout);
}

void printfloat(float num) {
	printf("%f\n", num);
}